// FFT.h: interface for the CFFT class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FFT_H__5AA7D93E_DA0F_410C_9516_4A4FC51285B8__INCLUDED_)
#define AFX_FFT_H__5AA7D93E_DA0F_410C_9516_4A4FC51285B8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CFFT  
{
public:
	CFFT();
	virtual ~CFFT();

};

void cstb(int length,int inv,float *sin_tbl,float *cos_tbl);
void birv(float *a,int length,int ex,float *b);
void fft1core(float *a_rl,float *a_im,int length,int ex,float *sin_tbl,float *cos_tbl,float *buf);
int FFT1(float *a_rl,float *a_im,int ex,int inv);
void FrequencyFilter1(float *ar,float *ai,int length,int ftype,float d0,float rl=0.0,float rh=0.0);

int FFT2(float *a_rl,float *a_im,int inv,int xsize,int ysize);
void FrequencyFilter2(float *ar,float *ai,int xsize,int ysize,int ftype,float d0,float rl=0.0,float rh=0.0);

void ln(BYTE *image_in,float *image_out,int xsize,int ysize);
void ex(float *image_in,BYTE *image_out,int xsize,int ysize);

void OneDimensionFilter(float *filter,int length,float d0,int type);
void FrequencyFilter1(float *ar,float *ai,float *filter,int length,float d0);
int FFTFilterBigImage(BYTE *image_in,int xsize,int ysize,float d0,int filtertype,float mean);

#endif // !defined(AFX_FFT_H__5AA7D93E_DA0F_410C_9516_4A4FC51285B8__INCLUDED_)

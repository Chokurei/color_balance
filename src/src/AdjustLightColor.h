// AdjustLightColor.h: interface for the CAdjustLightColor class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ADJUSTLIGHTCOLOR_H__A6F8DA96_E496_4785_8E12_26BDE6B10A38__INCLUDED_)
#define AFX_ADJUSTLIGHTCOLOR_H__A6F8DA96_E496_4785_8E12_26BDE6B10A38__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ximage.h"
#include "BasicStructer.h"

class CAdjustLightColor  
{
public:
	CAdjustLightColor();
	virtual ~CAdjustLightColor();

protected:
	int	nFtype;			//滤波器类型
						//	0 理想高通滤波器
						//	1 理想低通滤波器
						//	2 巴特沃斯高通滤波器
						//	3 巴特沃斯低通滤波器
						//	4 高斯高通滤波器
						//	5 高斯低通滤波器
						//  6 同态滤波器
	float nD0,nRl,nRh;	//截止频率与同态滤波时的低频压缩因子与高频增强因子

	float *nAr,*nAi;	//2维离散傅立叶变换的实部与须部空间
	int nWsizex,nWsizey;//数据的宽度与高度

public:

//匀光
	//HomomorphicDodging() 进行影像的同态匀光处理
	//CxImage *pImg	待处理的影像,可以是24位彩色影像，也可以是8位灰度影像
	//float size	截止频率(size>0)
	//rl			低频压缩因子
	//rh			高频增强因子
	bool HomomorphicFilter(CxImage *pImg,float size,float rl,float rh);

	//DivideDodging() 进行影像匀光处理
	//处理过程为创建影像小的低频背景，然后原始影像除低频背景得高频信息
	//CxImage *pImg	待处理的影像,可以是24位彩色影像，也可以是8位灰度影像
	//float size	截止频率(size>0)
	bool DivideDodging(CxImage *pImg,float size=3.0);

	//SubDodging()  进行影像匀光处理
	//处理过程为创建影像小的低频背景，然后原始影像减去低频背景得高频信息
	//CxImage *pImg	待处理的影像,可以是24位彩色影像，也可以是8位灰度影像
	//float size	截止频率(size>0)
	bool SubDodging(CxImage *pImg,float size=3.0);

	//ImageFilter()	进行影像频率域滤波处理
	//CxImage *pImg	待处理的影像，可以是24位彩色影像，也可以是8位灰度影像
	//float size	截止频率
	//int nType		与 nFtype 变量同意
					//	0 理想高通滤波器
					//	1 理想低通滤波器
					//	2 巴特沃斯高通滤波器
					//	3 巴特沃斯低通滤波器
					//	4 高斯高通滤波器
					//	5 高斯低通滤波器
					//  6 同态滤波器
	//int nDimension 处理维度
	//				0 二维同时处理（需要内存大，速度慢，效果好）
	//				1 行列分开处理（需要内存小，速度快，效果差）
	bool ImageFilter(CxImage *pImg,float size,int nType=0,int nDimension=0);

	//SimpleTemplate() 简单模板匀光处理
	//CxImage *pImg 待处理影像
	//int iSize		简单模板窗口尺寸
	//RGBQUAD *pBackGrd	不参与统计的背景颜色列表
	//int nBackGrd		背景颜色列表数量
	void SimpleTemplate(CxImage *pImg,int iSize=3,RGBQUAD *pBackGrd=NULL,int nBackGrd=0);

	//BBISubDodging()  进行BBI影像匀光处理
	//处理过程为创建影像小的低频背景，然后原始影像减去低频背景得高频信息
	//CString strBBI	待处理的影像路径,可以是24位彩色影像，也可以是8位灰度影像
	//CString strRes	结果影像路径
	//float size	截止频率(size>0)
	bool BBISubDodging(CString strBBI,CString strRes,float size=3.0);

	//BBIDivideDodging() 进行BBI影像匀光处理
	//处理过程为创建影像小的低频背景，然后原始影像除低频背景得高频信息
	//CString strBBI	待处理的影像路径,可以是24位彩色影像，也可以是8位灰度影像
	//CString strRes	结果影像路径
	//float size	截止频率(size>0)
	bool BBIDivideDodging(CString strBBI,CString strRes,float size=3.0);

// 	//BBISimpleTemplate() BBI影像简单模板匀光处理
// 	//CString strBBI	待处理的影像路径,可以是24位彩色影像，也可以是8位灰度影像
// 	//CString strRes	结果影像路径
// 	//int iSize		简单模板窗口尺寸
// 	//RGBQUAD *pBackGrd	不参与统计的背景颜色列表
// 	//int nBackGrd		背景颜色列表数量
// 	void BBISimpleTemplate(CString strBBI,CString strRes,int iSize=3,RGBQUAD *pBackGrd=NULL,int nBackGrd=0);

//匀色
	//AdjustColor() 进行影像匀色处理
	//CxImage *pImg	待处理的影像，可以是24位彩色影像，也可以是8位灰度影像
	//int stdbiBit	影像颜色分辨率（8或24） 8指8位灰度影像，24指24位彩色影像
	//float fVars	方差 fVars[0],fVars[1],fVars[2]指24位彩色影像的RGB波段方差,fVars[3]指8位灰度影像的方差
	//float fMeans	均值 fMeans[0],fMeans[1],fMeans[2]指24位彩色影像的RGB波段均值,fMeans[3]指8位灰度影像的均值
	//RGBQUAD *pBackGrd	不参与统计的背景颜色列表
	//int nBackGrd		背景颜色列表数量
	bool AdjustColor(CxImage *pImg,int stdbiBit,float fVars[4],float fMeans[4],RGBQUAD *pBackGrd=NULL,int nBackGrd=0);

	//BlockAdjustColor()影像分块匀色
	//int nWidBlock		影像行分块数
	//int nHeiBlock		影像列分块数
	//int nOverlapeWid	影像块结合区域的重叠度（相对于小块宽高）
	//其他参数与AdjustColor()相同
	void BlockAdjustColor(CxImage *pImg,int nWidBlock,int nHeiBlock,int nOverlapeWid,int stdbiBit,float fvars[4],float fmeans[4],RGBQUAD *pBackGrd=NULL,int nBackGrd=0);

	//GetColorStatistic()	统计影像颜色分辨率，均值与方差等信息，参数意义与AdjustColor()相同
	void GetColorStatistic(CxImage *pImg,int &bpp,float fvars[4],float fmeans[4],RGBQUAD *pBackGrd=NULL,int nBackGrd=0);

	//WallisTransform()	给定标准的均值与方差和当前均值与方差，进行Wallis处理
	//CxImage *pImg,int stdbiBit 待处理影像以及颜色分辨率
	//float fVarRef[4],float fMeanRef[4] 标准均值与方差
	//float fVarCur[4],float fMeanCur[4] 当前均值与方差
	bool WallisTransform(CxImage *pImg,int stdbiBit,float fVarRef[4],float fMeanRef[4],float fVarCur[4],float fMeanCur[4],RGBQUAD *pBackGrd=NULL,int nBackGrd=0);

	//AdjustBBIColor()	给定标准的均值与方差，进行BBI格式文件的色调处理
	//CString strBBI    BBI文件路径
	//int stdbiBit      标准影像颜色分辨率
	//float fVarRef[4],float fMeanRef[4] 标准均值与方差
	//RGBQUAD *pBackGrd	不参与统计的背景颜色列表
	//int nBackGrd		背景颜色列表数量
	//BOOL bReWrite		是否重写BBI文件，FALSE 只创建颜色表
	bool AdjustBBIColor(CString strBBI,int stdbiBit,float fVarRef[4],float fMeanRef[4],RGBQUAD *pBackGrd=NULL,int nBackGrd=0,BOOL bReWrite=FALSE);

	//GetBBIStatistic()	统计BBI影像颜色分辨率，均值与方差等信息，参数意义与AdjustColor()相同
	bool GetBBIStatistic(CString strBBI,int &bpp,float fvars[4],float fmeans[4],RGBQUAD *pBackGrd=NULL,int nBackGrd=0);

	//AdjustBBIRGB()	调整BBI文件的RGB值
	//CString strBBI    BBI文件路径
	//int biBit         影像颜色分辨率，24 彩色，8 灰度
	//int rgbs[4]       R波段的调整rgbs[0],G波段的调整rgbs[1],B波段的调整rgbs[2],如果是灰度影像，灰度波段的调整rgbs[3]
	//RGBQUAD *pBackGrd	不参与调整的背景颜色列表
	//int nBackGrd		背景颜色列表数量
	bool AdjustBBIRGB(CString strBBI,int biBit,int rgbs[4],RGBQUAD *pBackGrd=NULL,int nBackGrd=0);

	//GetBBIStatistic()	统计大影像颜色分辨率，均值与方差等信息，参数意义与AdjustColor()相同
	bool GetBigFileStatistic(CString strBigFileDir,int &bpp,float fvars[4],float fmeans[4],RGBQUAD *pBackGrd=NULL,int nBackGrd=0);

	//AdjustBigFileColor()	给定标准的均值与方差，进行大文件的色调处理
	//CString strBigFileDir	大文件路径
	//int stdbiBit          标准影像颜色分辨率
	//float fVarRef[4],float fMeanRef[4] 标准均值与方差
	//RGBQUAD *pBackGrd	    不参与统计的背景颜色列表
	//int nBackGrd		    背景颜色列表数量
	bool AdjustBigFileColor(CString strBigFileDir,int stdbiBit,float fVarRef[4],float fMeanRef[4],RGBQUAD *pBackGrd=NULL,int nBackGrd=0);

	//GetBBILapStatistic() 统计两幅BBI图像重叠区域的均值与方差，并返回重叠区域的有效像素数
	//CString strBBIDir1,float fVars1[4],float fMeans1[4] 第一幅BBI图像的路径与均值，方差统计结果
	//CString strBBIDir2,float fVars2[4],float fMeans2[4] 第二幅BBI图像的路径与均值，方差统计结果
	//RGBQUAD *pBackGrd,int nBackGrd	不参与统计的背景颜色列表与列表数量
	LONGLONG GetBBILapStatistic(CString strBBIDir1,float fVars1[4],float fMeans1[4],CString strBBIDir2,float fVars2[4],float fMeans2[4],RGBQUAD *pBackGrd=NULL,int nBackGrd=0);

	//GetLapStatistic() 统计两幅正射影像重叠区域的均值与方差，并返回重叠区域的有效像素数
	//CString strOrthoDir1,float fVars1[4],float fMeans1[4] 第一幅正射影像的路径与均值，方差统计结果
	//CString strOrthoDir2,float fVars2[4],float fMeans2[4] 第二幅正射影像的路径与均值，方差统计结果
	//RGBQUAD *pBackGrd,int nBackGrd	不参与统计的背景颜色列表与列表数量
	long GetLapStatistic(CString strOrthoDir1,float fVars1[4],float fMeans1[4],CString strOrthoDir2,float fVars2[4],float fMeans2[4],RGBQUAD *pBackGrd=NULL,int nBackGrd=0);


//测区影像最小二乘平差匀色
	//StatisticOrtho()		根据重叠区域统计正射影像上方差与相关系数均较大的区域的均值与方差，返回重叠区域的有效像元数
	//CString strOrtho1		第一张正射影像路径名
	//float *pVars1,float *pMeans1 影像1在分块后重叠区域的有效像元的均值与方差，内存空间长度为BytesPPixel*nRowBlk*nColBlk*sizeof(float)
	//CString strOrtho2		第二张重叠的正射影像路径名
	//float *pVars2,float *pMeans2 影像2在分块后重叠区域的有效像元的均值与方差，内存空间长度为BytesPPixel*nRowBlk*nColBlk*sizeof(float)
	//long *pUsefulSize		分块后重叠区域的有效像元数，内存长度为nRowBlk*nColBlk*sizeof(long)
	//int nRowBlk=3,int nColBlk=3  重叠区域的行列分块数
	//int nWinSize			相关窗口大小
	//int nCorrCut			相关度阈值，小于该值，不予统计
	//RGBQUAD *pBackGrd,int nBackGrd	不参与统计的背景颜色列表与列表数量
	BOOL CorrStatisticOrtho(CString strOrtho1,float *pVars1,float *pMeans1,
							CString strOrtho2,float *pVars2,float *pMeans2,
							long *pUsefulSize,int nRowBlk=3,int nColBlk=3,int nWinSize=9,int nCorrCut=80,RGBQUAD *pBackGrd=NULL,int nBackGrd=0);

	

//ADS40零级影像处理
	//ADS40RadiateEliminate() ADS40零级影像辐射校正，16位或8位输入，8位输出
	//CString strDir          ADS40零级影像路径
	//BOOL bManMade	          统计时是否去除人造地物（TRUE 去除人造地物，FALSE 不去除人造地物）
	//BOOL bStainMeans	      固定均值辐射校正  （TRUE 固定均值校正，FALSE 不固定均值校正）
	//CString strSaveDir      辐射校正后的结果保存路径
	bool ADS40RadiateEliminate(CString strDir,BOOL bManMade,BOOL bStainMeans,CString strSaveDir);

	//ADS40StandEliminate() 利用标准影像进行ADS40零级影像的辐射校正，16位或8位输入，8位输出
	//CString strDir        ADS40零级影像路径
	//CString strStdDir     缩小的影像路径（标准影像）
	//CString strSaveDir    辐射校正后的结果保存路径
	bool ADS40StandEliminate(CString strDir,CString strStdDir,CString strSaveDir);

	//CombainRGB() ADS40零级影像彩色融合，16位或8位输入，24位输出
	//CString strR Red波段的影像路径
	//int oXR,oYR  Red波段配准的起始像素坐标(0 - photo width,photo height)
	//float fR     Red波段配准的相对影像因子(0.0-1.0)
	//............................
	//CString strSaveDir RGB融合后的结果保存路径
	//BOOL bGrnControl   是否利用绿波段进行辐射控制
	bool CombainRGB(CString strR,CString strG,CString strB,CString strSaveDir,
					int oXR=0,int oYR=0,float fR=1.0,
					int oXG=0,int oYG=0,float fG=1.0,
					int oXB=0,int oYB=0,float fB=1.0,
					BOOL bGrnControl=FALSE);

	//TextureRecognition() 纹理识别,将LPB值在(LPB1 <= LPB <= LPB2)之间的且LC值在(LC1 <= LC <= LC2)之间的纹理区域用FillColor颜色填充
	//CString strDir       原始影像路径
	//CString strSaveDir   结果影像路径
	//int LPB1,int LPB2    LPB值范围(0-255)，有：LPB1<=LPB2
	//int LC1,int LC2      LC值范围 (0-255)，有：LC1<=LC2
	//int nIgnore          忽略小于该值的目标区域 
	//int nRadius		   窗口半径
	//int nPixels		   圆形邻域像素个数
	bool TextureRecognition(CString strDir,CString strSaveDir,int LBP1,int LBP2,int LC1,int LC2,int nIgnore=5,int nRadius=3,int nPixels=8);

//16bit影像处理
	//DivideDodging16Bit()	进行16bit影像匀光处理
	//处理过程为创建影像小的低频背景，然后原始影像除低频背景得高频信息
	//CString strDir 原始影像路径
	//CString strResDir 处理结果保存路径
	//float size	截止频率(size>0)
	bool DivideDodging16Bit(CString strDir,CString strResDir,float size=3.0,BOOL bCompare=FALSE);

	//SubDodging16Bit()	进行16bit影像匀光处理
	//处理过程为创建影像小的低频背景，然后原始影像减低频背景得高频信息
	//CString strDir 原始影像路径
	//CString strResDir 处理结果保存路径
	//float size	截止频率(size>0)
	bool SubDodging16Bit(CString strDir,CString strResDir,float size=3.0,BOOL bCompare=FALSE);

	//SimpleTemplate16Bit() 16bit影像简单模板匀光处理
	//CString strDir 原始影像路径
	//CString strResDir 处理结果保存路径
	//其它参数同 SimpleTemplate()
	bool SimpleTemplate16Bit(CString strDir,CString strResDir,int iSize=3,BOOL bCompare=FALSE);

	//AdjustColor16Bit() 16bit影像匀色处理
	//CString strDir 原始影像路径
	//CString strResDir 处理结果保存路径
	//其它参数同 AdjustColor()
	bool AdjustColor16Bit(CString strDir,CString strResDir,int stdbiBit,float fStdVars[4],float fStdMeans[4],BOOL bCompare=FALSE);

	//GetColorStatistic16Bit()	统计影像颜色分辨率，均值与方差等信息，参数意义与AdjustColor()相同
	bool GetColorStatistic16Bit(CString strDir,int &bpp,float fvars[4],float fmeans[4]);

	//GetColorStatistic16Bit() 统计影像均值与方差信息
	//WORD *pData 灰度数据
	//int wid,int hei 灰度块宽高
	//float &fvars,float &fmeans 统计结果
	void GetColorStatistic16Bit(WORD *pData,int wid,int hei,float &fvars,float &fmeans);

	//WallisTransform16Bit() 对16bit数据，给定标准的均值与方差和当前均值与方差，进行Wallis处理
	//WORD *pData 灰度数据
	//int wid,int hei 灰度块宽高
	//float fVarRef,float fMeanRef 准均值与方差
	//float fVarCur,float fMeanCur 当前均值与方差
	void WallisTransform16Bit(WORD *pData,int wid,int hei,float fVarRef,float fMeanRef,float fVarCur,float fMeanCur);

	long Histogram16Bit(WORD *pData,int wid,int hei,long *pHisto);
	void ChannelByTemplate16Bit(WORD *pData,int wid,int hei,long pHisto[65536],int iSize=3);
	bool DivideDodging16BitGray(WORD *pData,int wid,int hei,float size);
	bool SubDodging16BitGray(WORD *pData,int wid,int hei,float size);

private:
	//一维灰度数据均值与方差等信息的统计
	float GetMeans(float *pData,int size);
	void GetMeansVars2(BYTE *pGray,int gSize,float &means2,float &vars2,float mean,float var);
	void GetMeansVars(BYTE *pGray,int gSize,float &means,float &vars);

	//二维灰度数据均值与方差等信息的统计
	void HistoStatis16Bit(long nUseSize, long *pHisto, float &fVars,float &fMeans,WORD &LightMins,WORD &LightMaxs);
	void CalculateLut16Bit(WORD lut[65536],float fVarRef,float fMeanRef,float fVarCur,float fMeanCur);
	long Histogram(CxImage *pImg,long* pRed,long* pGreen,long* pBlue,long* pGray,RGBQUAD *pBackGrd=NULL,int nBackGrd=0);
	void HistoStatis(long nUseSize, long *pRed, long *pGre, long *pBlu, long *pGra, float fVars[4],float fMeans[4],BYTE LightMins[4],BYTE LightMaxs[4]);
	void CalculateLut(int iBit,BYTE lutR[256],BYTE lutG[256],BYTE lutB[256],BYTE lutA[256],float fVarRef[4],float fMeanRef[4],float fVarCur[4],float fMeanCur[4]);
	void WallisGlobal(CxImage *pImg,BYTE lut[256],RGBQUAD *pBackGrd=NULL,int nBackGrd=0);
	void WallisGlobal(CxImage *pImg,BYTE lutR[256],BYTE lutG[256],BYTE lutB[256],RGBQUAD *pBackGrd=NULL,int nBackGrd=0);

	//分块匀光
	void CombainSmallPhoto(CxImage *pBigImg,CxImage *pSmall,CRect rect,int overlape,int verhor);
	BOOL BeBackground(BYTE *lpBit, int iBit, RGBQUAD* pBackGrd,int nBackGrd);
	void BlockAdjust(CxImage *pImg,int nWidBlock,int nHeiBlock,int nOverlapeWid,int stdbiBit,float fvars[4],float fmeans[4],RGBQUAD *pBackGrd=NULL,int nBackGrd=0);
	void LocalBlockAdjust(CxImage *pImg,int nWidBlock,int nOverlapeWid,float fvars[4],float fmeans[4],int stdbiBit,RGBQUAD *pBackGrd=NULL,int nBackGrd=0);

	//简单模板匀光
	double GetMainElement(int n, int k, double *pdA, int *ik, int *jk);
	BOOL GauceFunction(int n, double *pdA, double *pdb, double *pdResult, double delta=1e-10);
	void LuminanceTemplateFit(int *pTemplate,int iWid,int iHei,int mingray=0,int maxgray=255);
	void ChannelByTemplate(CxImage *pImg,long pHisto[256],int iSize=3,RGBQUAD *pBackGrd=NULL,int nBackGrd=0);

	//Homomorphic() 同态滤波
	//	image_in:	输入图像数据指针,输出图像数据指针
	//	xsize:	数据宽度（2的次方）
	//	ysize：	数据高度（2的次方）
	//	return value: -1 failer; 0 successful
	int Homomorphic(BYTE *image);
	void HomomorphicGray(CxImage *img);
	void HomomorphicColor(CxImage *img);

	//FFTFilterImage() 图像的二维FFT，滤波处理，逆FFT
	//image_in:	输入图像数据指针,输出图像数据指针
	//xsize:	数据宽度（2的次方）
	//ysize：	数据高度（2的次方）
	//return value: -1 failer; 0 successful
	int FFTFilterImage16Bit(WORD *image);
	int FFTFilterImage(BYTE *image,float mean);
	void FilterGray(CxImage *img,float mean);
	void FilterColor(CxImage *img,float nMeans[3]);

	void FilterBigGray(CxImage *img,float d0,int filtertype,float mean);
	void FilterBigColor(CxImage *img,float d0,int filtertype,float nMeans[3]);

	//CreatMemSpace() 构建频率域的滤波器,分配频率域的变换空间，参数正确，返回TRUE
	//int xsize,int ysize 滤波器尺寸
	//int ftype	滤波器类型，见int	nFtype注释
	//float d0	截止频率
	//float rl,float rh 同态滤波时的低频压缩因子与高频增强因子
	BOOL CreatMemSpace(int xsize,int ysize,int ftype,float d0,float rl=0,float rh=0);

	//CalCircleSet() 计算采样环的坐标
	//int p          圆形邻域像素个数，p<31
	//int r          环半径
	//double *dxy    环上采样点坐标
	void CalCircleSet(int p,int r,double *dxy);

	//Resample16Bit() 16Bit数据双线性差值采样
	//WORD *pData,int wid,int hei 原始数据与宽高
	//WORD *pDis,int w,int h	  采样后的结果与宽高
	void Resample16Bit(WORD *pData,int wid,int hei,WORD *pDis,int w,int h);

	//BilinearInt() 双线性插值运算
	//BYTE *gray    灰度矩阵数据
	//int w,int h   灰度矩阵宽高
	//double *xy    像素坐标值
	inline BYTE BilinearInt(BYTE *gray,int w,int h,double xy[2]);

	//GetLBPLC()  纹理分类基本函数,计算所给纹理矩阵的lbp与lc值
	//BYTE *gray  按行优先存储的灰度矩阵
	//int p       圆形邻域像素个数，p<31
	//BYTE gcent  中心像元的灰度值
	//int &lbp    LBP值计算结果
	//int &lc     LC 值计算结果
	void GetLBPLC(BYTE *gray,int p,BYTE *gcent,int &lbp,int &lc);

	//在两个影像 *img1与 *img2的两个（分别以int x1、y1与 x2、y2中心；height与wigth）影像块的相关系数
	//*img1,row1,col1, *img2,row2,col2,－－－－－－两个影像与行、列数
	//x1,y1,x2,y2－－－－－－－－－－－－－－－－－影像块中心点的坐标
	//height,wigth－－－－－－－－－－－－－－－－－影像块的高宽；
	//*maxw0－－－－－－－－－－－－－－－－－－－－返回的相关系数
	void CorrCoefficient(BYTE *img1,int row1,int col1,BYTE *img2,int row2,int col2,int x1,int y1,int x2,int y2,int height,int width,BYTE &cr);

	BOOL ReadTFWFILE(CString strTifDir,double &left,double &top,double &gsd);
	long LapStatistic(BYTE *pBuf1,BYTE *pBuf2,int nPBB,int wid,int hei,float fMean1[4],float fVars1[4],float fMean2[4],float fVars2[4],RGBQUAD *pBackGrd=NULL,int nBackGrd=0);	
	void BlkStatistic(BYTE *pBuf,int nPBB,int wid,int hei,float fMean[4],float fVars[4],RGBQUAD *pBackGrd=NULL,int nBackGrd=0);
};

#endif // !defined(AFX_ADJUSTLIGHTCOLOR_H__A6F8DA96_E496_4785_8E12_26BDE6B10A38__INCLUDED_)

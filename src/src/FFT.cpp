// FFT.cpp: implementation of the CFFT class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "FFT.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define FFTPI 3.141592653589793f
#define FFTE  2.718281828459045f
#define SQRT2 1.414213562373095f

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFFT::CFFT()
{

}

CFFT::~CFFT()
{

}


/*-------1维fft--------------------
	a_rl:	实部
	a_im:	虚部
	ex:		数据个数＝2的ex次方
	inv:	1:DFT -1:逆DFT
----------------------------------*/
int FFT1(float *a_rl,float *a_im,int ex,int inv)
{
	int i=0,length=0;
	float *sin_tbl=NULL;
	float *cos_tbl=NULL;
	float *buf=NULL;

	length=1<<ex;
	sin_tbl=(float *)malloc(length*sizeof(float));
	cos_tbl=(float *)malloc(length*sizeof(float));
	buf=(float *)malloc(length*sizeof(float));

	if ((sin_tbl==NULL)||(cos_tbl==NULL)||(buf==NULL)) return -1;
	cstb(length,inv,sin_tbl,cos_tbl);
	fft1core(a_rl,a_im,length,ex,sin_tbl,cos_tbl,buf);

	free(sin_tbl);
	free(cos_tbl);
	free(buf);

	return 0;
}

/*----------1维fft主计算部分-----------------
	a_rl:	实部
	a_im:	虚部
	ex:		数据个数＝2的ex次方
	sin_tbl:sin数据配列
	cos_tbl:cos数据配列
-------------------------------------------*/
void fft1core(float *a_rl,float *a_im,int length,int ex,float *sin_tbl,float *cos_tbl,float *buf)
{
	int i,j,k,w,j1,j2;
	int numb,lenb,timb;
	float xr,xi,yr,yi,nrml;
 
	for(i=1;i<length;i+=2)
	{
		a_rl[i]=-a_rl[i];
		a_im[i]=-a_im[i];
	}

	numb=1;
	lenb=length;
	for(i=0;i<ex;i++)
	{
		lenb/=2;
		timb=0;
		for(j=0;j<numb;j++)
		{
			w=0;
			for(k=0;k<lenb;k++)
			{
				j1=timb+k;
				j2=j1+lenb;
				xr=a_rl[j1];
				xi=a_im[j1];
				yr=a_rl[j2];
				yi=a_im[j2];
				a_rl[j1]=xr+yr;	
				a_im[j1]=xi+yi;

				xr-=yr;
				xi-=yi;

				a_rl[j2]=xr*cos_tbl[w]-xi*sin_tbl[w];
				a_im[j2]=xr*sin_tbl[w]+xi*cos_tbl[w];

				w+=numb;
			}
			timb+=lenb*2;
		}
		numb*=2;
	}
	birv(a_rl,length,ex,buf);
	birv(a_im,length,ex,buf);

	for(i=1;i<length;i+=2)
	{
		a_rl[i]=-a_rl[i];
		a_im[i]=-a_im[i];
	}

	nrml=(float)(1.0/sqrt((float)length));
	for(i=0;i<length;i++)
	{
		a_rl[i]*=nrml;
		a_im[i]*=nrml;
	}
}

/*-------计算sin，cos数据---------
	length:	数据个数
	inv：	1：DFT，-1：逆DFT
	sin_tbl:sin数据配列
	cos_tbl:cos数据配列
----------------------------------*/
void cstb(int length,int inv,float *sin_tbl,float *cos_tbl)
{
	int i;
	float xx,arg;
	float *lpSin=sin_tbl,*lpCos=cos_tbl;

	xx=(float)(((-FFTPI)*2)/(float)length);
	if (inv<0) xx=-xx;
	for(i=0;i<length;i++)
	{
		arg=(float)i*xx;
		*lpSin=(float)sin(arg);
		*lpCos=(float)cos(arg);

		lpSin++;
		lpCos++;
	}
}

/*-------排列数据--------------------
	a:		数据配列
	length:	数据个数
	ex:		数据个数＝2的ex次方
	b:		工作用配列
----------------------------------*/
void birv(float *a,int length,int ex,float *b)
{
	int i,ii,k,bit;

	for(i=0;i<length;i++)
	{
		for(k=0,ii=i,bit=0;;bit<<=1,ii>>=1)
		{
			bit=(ii&1)|bit;
			if(++k==ex) break;
		}
		b[i]=a[bit];
	}
	memcpy(a,b,length*sizeof(float));
}

/*-------2维fft--------------------
	a_rl:	实部
	a_im:	虚部
	inv:	1:DFT -1:逆DFT
	xsize:	数据宽度（2的次方）
	ysize：	数据高度（2的次方）
----------------------------------*/
int FFT2(float *a_rl,float *a_im,int inv,int xsize,int ysize)
{
	float *hsin_tbl=NULL;
	float *hcos_tbl=NULL;
	float *vsin_tbl=NULL;
	float *vcos_tbl=NULL;
	float *buf_x=NULL;
	float *buf_y=NULL;
	float *buf_rl=NULL;
	float *buf_im=NULL;
	float *lpA=NULL,*lpB=NULL;

	int i,j;

	hsin_tbl=(float *)malloc(xsize*sizeof(float));
	hcos_tbl=(float *)malloc(xsize*sizeof(float));
	memset(hsin_tbl,0,xsize*sizeof(float));
	memset(hcos_tbl,0,xsize*sizeof(float));

	vsin_tbl=(float *)malloc(ysize*sizeof(float));
	vcos_tbl=(float *)malloc(ysize*sizeof(float));
	memset(vsin_tbl,0,ysize*sizeof(float));
	memset(vcos_tbl,0,ysize*sizeof(float));

	buf_x=(float *)malloc(xsize*sizeof(float));
	buf_y=(float *)malloc(ysize*sizeof(float));
	memset(buf_x,0,xsize*sizeof(float));
	memset(buf_y,0,ysize*sizeof(float));

	buf_rl=(float *)malloc(ysize*sizeof(float));
	buf_im=(float *)malloc(ysize*sizeof(float));
	memset(buf_rl,0,ysize*sizeof(float));
	memset(buf_im,0,ysize*sizeof(float));

	if(	hsin_tbl==NULL||hcos_tbl==NULL||
		vsin_tbl==NULL||vcos_tbl==NULL||
		buf_x==NULL||buf_y==NULL||
		buf_rl==NULL||buf_im==NULL)
		return -1;

	cstb(xsize,inv,hsin_tbl,hcos_tbl);
	cstb(ysize,inv,vsin_tbl,vcos_tbl);

	int x_exp=0;
	while((1<<x_exp)<xsize) x_exp++;

	for(i=0;i<ysize;i++)
	{
		fft1core(&(*(a_rl+i*xsize)),&(*(a_im+i*xsize)),xsize,x_exp,hsin_tbl,hcos_tbl,buf_x);
	}

	int y_exp=0;
	while((1<<y_exp)<ysize) y_exp++;

	for(i=0;i<xsize;i++)
	{
		lpA=a_rl+i;
		lpB=a_im+i;
		for (j=0;j<ysize;j++)
		{
			buf_rl[j]=*lpA;
			buf_im[j]=*lpB;

			lpA+=xsize;
			lpB+=xsize;
 		}

		fft1core(buf_rl,buf_im,ysize,y_exp,vsin_tbl,vcos_tbl,buf_y);

		lpA=a_rl+i;
		lpB=a_im+i;
		for (j=0;j<ysize;j++)
		{
			*lpA=buf_rl[j];
			*lpB=buf_im[j];

			lpA+=xsize;
			lpB+=xsize;
 		}
	}

	free(hsin_tbl);
	free(hcos_tbl);
	free(vsin_tbl);
	free(vcos_tbl);
	free(buf_x);
	free(buf_y);
	free(buf_rl);
	free(buf_im);
	lpA=NULL,lpB=NULL;

	return 0;
}

/*------------1维频率域滤波操作--------------
	float *ar,float *ai 傅立叶实部与虚部
	int ftype		处理类型:
					0 理想高通滤波器
					1 理想低通滤波器
					2 巴特沃斯高通滤波器
					3 巴特沃斯低通滤波器
					4 高斯高通滤波器
					5 高斯低通滤波器
					6 同态滤波器
	float d0	截止频率
	float rl,float rh 低频压缩因子与高频增强因子
-------------------------------------*/
void FrequencyFilter1(float *ar,float *ai,int length,int ftype,float d0,float rl,float rh)
{
	float *lpAr=ar;
	float *lpAi=ai;

	float *radius2=new float[length];
	memset(radius2,0,length*sizeof(float));
	int lhalf=length/2;

	float fdis=0;
	for(int i=0;i<length;i++)
	{
		if (i<lhalf) fdis=lhalf-i-1;
		else fdis=i-lhalf;
		radius2[i]=fdis*fdis;
	}

	float dis2=0;
	float d02=d0*d0;
	float filter=0.0;
	for (i=0;i<length;i++)
	{
		dis2=radius2[i];

		switch(ftype)
		{
		case 6:
			filter=(rh-rl)*(1.0-(float)pow(FFTE, -dis2/d02))+rl;
			break;
		case 5:
			filter=(float)pow(FFTE, -dis2/d02);
			break;
		case 4:
			filter=1.0-(float)pow(FFTE, -dis2/d02);
			break;
		case 3:
			filter=(float)(1.0/(1.0+dis2/d02));
			break;
		case 2:
			filter=(float)(1.0/(1.0+d02/(dis2+0.0000001)));
			break;
		case 1:
			if (dis2<=d02) filter=1.0;
			else filter=0.0;
			break;
		case 0:
			if (dis2>d02) filter=1.0;
			else filter=0.0;
			break;
		default:
			break;
		}

		(*lpAr)*=filter;
		(*lpAi)*=filter;

		lpAr++;
		lpAi++;
	}
	delete radius2;	
}

/*------------2维频率域滤波操作--------------
	float *ar,float *ai 傅立叶实部与虚部
	int ftype		处理类型:
					0 理想高通滤波器
					1 理想低通滤波器
					2 巴特沃斯高通滤波器
					3 巴特沃斯低通滤波器
					4 高斯高通滤波器
					5 高斯低通滤波器
					6 同态滤波器
	float d0	截止频率
	float rl,float rh 低频压缩因子与高频增强因子
-------------------------------------*/
void FrequencyFilter2(float *ar,float *ai,int xsize,int ysize,int ftype,float d0,float rl,float rh)
{
	float *lpAr=ar;
	float *lpAi=ai;

	float *radiusx2=new float[xsize];
	memset(radiusx2,0,xsize*sizeof(float));
	float *radiusy2=new float[ysize];
	memset(radiusy2,0,ysize*sizeof(float));
	int xhalf=xsize/2,yhalf=ysize/2;

	float fdis=0;
	for(int i=0;i<xsize;i++)
	{
		if (i<xhalf) fdis=xhalf-i-1;
		else fdis=i-xhalf;
		radiusx2[i]=fdis*fdis;
	}

	for(int j=0;j<ysize;j++)
	{
		if (j<yhalf) fdis=yhalf-j-1;
		else fdis=j-yhalf;
		radiusy2[j]=fdis*fdis;
	}

	float dis2=0;
	float d02=d0*d0;
	float filter=0.0;
	for (j=0;j<ysize;j++)
	{
		for (i=0;i<xsize;i++) 
		{
			dis2=radiusx2[i]+radiusy2[j];

			switch(ftype)
			{
			case 6:
				filter=(rh-rl)*(1.0-(float)pow(FFTE, -dis2/d02))+rl;
				break;
			case 5:
				filter=(float)pow(FFTE, -dis2/d02);
				break;
			case 4:
				filter=1.0-(float)pow(FFTE, -dis2/d02);
				break;
			case 3:
				filter=(float)(1.0/(1.0+dis2/d02));
				break;
			case 2:
				filter=(float)(1.0/(1.0+d02/dis2));
				break;
			case 1:
				if (dis2<=d02) filter=1.0;
				else filter=0.0;
				break;
			case 0:
				if (dis2>d02) filter=1.0;
				else filter=0.0;
				break;
			default:
				break;
			}

			(*lpAr)*=filter;
			(*lpAi)*=filter;

			lpAr++;
			lpAi++;
		}
	}
	delete radiusx2;
	delete radiusy2;
}

/*------------对数化操作--------------
	image_in:	输入
	image_out:	输出
	xsize:	数据宽度
	ysize:	数据高度
-------------------------------------*/
void ln(BYTE *image_in,float *image_out,int xsize,int ysize)
{
	long xysize=xsize*ysize;

	BYTE *pi=image_in;
	float *po=image_out;

	for (long i=0;i<xysize;i++)
	{
		*po=log(*pi+1.0);

		pi++;
		po++;
	}
}

/*------------指数化操作--------------
	image_in:	输入
	image_out:	输出
	xsize:	数据宽度
	ysize:	数据高度
-------------------------------------*/
void ex(float *image_in,BYTE *image_out,int xsize,int ysize)
{
	long xysize=xsize*ysize;
	float *pi=image_in;

	float mingray=255,maxgray=0;
	for (long i=0;i<xysize;i++)
	{
		*pi=exp(*pi)-1.0;

		if (*pi>maxgray) maxgray=*pi;
		if (*pi<mingray) mingray=*pi;

		pi++;
	}
	double ratio=255.0/(maxgray-mingray);

	pi=image_in;
	BYTE *po=image_out;
	float data=0;
	for (i=0;i<xysize;i++)
	{
		*po= (*pi-mingray)*ratio;

		pi++;
		po++;
	}
}

/*------------1维频率域滤波模版-----------------
	filter:	滤波器模版
	length:	数据宽度,为偶数
	d0:		截止频率
	int nType		滤波器类型:
					0 理想高通滤波器
					1 理想低通滤波器
					2 巴特沃斯高通滤波器
					3 巴特沃斯低通滤波器
					4 高斯高通滤波器
					5 高斯低通滤波器
-------------------------------------------------*/
void OneDimensionFilter(float *filter,int length,float d0,int type)
{
	float radius=0.0,d02=d0*d0;
	int halflength=length/2;

	memset(filter,0,length*sizeof(float));
	for(int i=0;i<length;i++)
	{
		if (i<halflength) radius=halflength-i-1;
		else radius=i-halflength;

		if(type==0) 
		{
			if (radius>=d0) *(filter+i)=1.0;
		}
		else if(type==1)
		{
			if (radius<d0) *(filter+i)=1.0;			
		}
		else if (type==2)
			*(filter+i)=(float)(1.0/(1.0+d02/(radius*radius+0.0000001)));
		else if(type==3)
			*(filter+i)=(float)(1.0/(1.0+(radius*radius)/d02));
		else if (type==4)
			*(filter+i)=1.0-(float)(pow(FFTE, -radius*radius/d02))/*/2*/;
		else if (type==5)
			*(filter+i)=(float)(pow(FFTE, -radius*radius/d02));
	}
}

/*------------1维频率域滤波--------
	ar:	频率域实部
	ai:	频率域虚部
	filter: 滤波器
	length:	数据宽度
	d0:		截止频率
-------------------------------------*/
void FrequencyFilter1(float *ar,float *ai,float *filter,int length,float d0)
{
	float *lpAr=ar;
	float *lpAi=ai;
	float data;

	for(int i=0;i<length;i++)
	{
		data  =(float)(*(filter+i));

		(*lpAr)*=data;
		(*lpAi)*=data;

		lpAr++;
		lpAi++;
	}
}

/*-------大图像的二维FFT，频率域滤波处理，逆FFT--------------------
	与FFTFilterImage()不同的是，它逐行与逐列滤波处理，节省内存空间
	image_in:	输入图像数据指针,输出图像数据指针
	xsize:	数据宽度（2的次方）
	ysize:	数据高度（2的次方）
	d0:		截止频率
	filtertype：滤波器类型	0 高斯高通滤波器；1 高斯低通滤波器；2 巴特沃司高通滤波器；3 巴特沃司低通滤波器
	return value: -1 failer; 0 successful
-----------------------------------------------------------------*/
int FFTFilterBigImage(BYTE *image_in,int xsize,int ysize,float d0,int filtertype,float mean)
{
	if (d0<=0.0) return -1;

	BYTE *lpIn=NULL,*lpOut=NULL;
	float *lpAr=NULL;
	float data=0;

	//step1,逐行处理图象
	int r_ex=0;
	while((1<<r_ex)<xsize) r_ex++;

	float *r_rl=new float[xsize];
	float *r_im=new float[xsize];
	float *r_sin1=new float[xsize];
	float *r_cos1=new float[xsize];
	float *r_sin2=new float[xsize];
	float *r_cos2=new float[xsize];
	float *r_buf=new float[xsize];

	memset(r_rl,0,xsize*sizeof(float));
	memset(r_im,0,xsize*sizeof(float));
	memset(r_sin1,0,xsize*sizeof(float));
	memset(r_cos1,0,xsize*sizeof(float));
	memset(r_sin2,0,xsize*sizeof(float));
	memset(r_cos2,0,xsize*sizeof(float));
	memset(r_buf,0,xsize*sizeof(float));

	cstb(xsize,1,r_sin1,r_cos1);
	cstb(xsize,-1,r_sin2,r_cos2);

	float *filterx=new float[xsize];
	OneDimensionFilter(filterx,xsize,d0,filtertype);

	for (int r=0;r<ysize;r++)
	{
		lpIn=image_in+r*xsize;
		lpAr=r_rl;
		for (int i=0;i<xsize;i++)
		{
			*lpAr=(float)(*lpIn)-mean;

			lpIn++;
			lpAr++;			
		}

		fft1core(r_rl,r_im,xsize,r_ex,r_sin1,r_cos1,r_buf);
 		FrequencyFilter1(r_rl,r_im,filterx,xsize,d0);
		fft1core(r_rl,r_im,xsize,r_ex,r_sin2,r_cos2,r_buf);

		lpOut=image_in+r*xsize;
		lpAr=r_rl;
		for (i=0;i<xsize;i++)
		{
			*lpOut=max(0,min(mean+*lpAr,255));

			lpAr++;
			lpOut++;
		}
	}

	delete r_rl;
	delete r_im;
	delete r_sin1;
	delete r_cos1;
	delete r_sin2;
	delete r_cos2;
	delete r_buf;
	delete filterx;
	//step1 end

	//step2,交换行列位置
	BYTE *image_tmp1=new BYTE[xsize*ysize];
	memcpy(image_tmp1,image_in,xsize*ysize*sizeof(BYTE));

	lpOut=image_tmp1;
	for (int j=0;j<ysize;j++)
	{
		lpIn=image_in+j;	
		for (int i=0;i<xsize;i++)
		{
			*lpIn=*lpOut;
			lpIn+=ysize;
			lpOut++;
		}
	}
	delete image_tmp1;
	//step2 end

	//step3,逐行处理图象	
	int c_ex=0;
	while((1<<c_ex)<ysize) c_ex++;

	float *c_rl=new float[ysize];
	float *c_im=new float[ysize];
	float *c_sin1=new float[ysize];
	float *c_cos1=new float[ysize];
	float *c_sin2=new float[ysize];
	float *c_cos2=new float[ysize];
	float *c_buf=new float[ysize];

	memset(c_rl,0,ysize*sizeof(float));
	memset(c_im,0,ysize*sizeof(float));
	memset(c_sin1,0,ysize*sizeof(float));
	memset(c_cos1,0,ysize*sizeof(float));
	memset(c_sin2,0,ysize*sizeof(float));
	memset(c_cos2,0,ysize*sizeof(float));
	memset(c_buf,0,ysize*sizeof(float));

	cstb(ysize,1,c_sin1,c_cos1);
	cstb(ysize,-1,c_sin2,c_cos2);

	float *filtery=new float[ysize];
	OneDimensionFilter(filtery,ysize,d0,filtertype);

	for (int c=0;c<xsize;c++)
	{
		lpIn=image_in+c*ysize;
		lpAr=c_rl;
		for (int i=0;i<ysize;i++)
		{
			*lpAr=(float)(*lpIn)-mean;

			lpIn++;
			lpAr++;			
		}

		fft1core(c_rl,c_im,ysize,c_ex,c_sin1,c_cos1,c_buf);
 		FrequencyFilter1(c_rl,c_im,filtery,ysize,d0);
		fft1core(c_rl,c_im,ysize,c_ex,c_sin2,c_cos2,c_buf);

		lpOut=image_in+c*ysize;
		lpAr=c_rl;
		for (i=0;i<ysize;i++)
		{
			*lpOut=max(0,min(mean+*lpAr,255));
			lpAr++;
			lpOut++;
		}
	}

	delete c_rl;
	delete c_im;
	delete c_sin1;
	delete c_cos1;
	delete c_sin2;
	delete c_cos2;
	delete c_buf;
	delete filtery;
	//step3 end

	//step4,恢复行列位置
	BYTE *image_tmp2=new BYTE[xsize*ysize];
	memcpy(image_tmp2,image_in,xsize*ysize*sizeof(BYTE));

	lpIn=image_in;
	for (j=0;j<ysize;j++)
	{
		lpOut=image_tmp2+j;
		for (int i=0;i<xsize;i++)
		{
			*lpIn=*lpOut;
			lpIn++;
			lpOut+=ysize;
		}
	}
	delete image_tmp2;
	//step4 end

	return 0;
}
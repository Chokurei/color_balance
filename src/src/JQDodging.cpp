// JQDodging.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "JQDodging.h"
#include "AdjustLightColor.h"
#include "SpBBImage.h"
//#include "GCTsk.h"
#include "GCUsr.h"
//#include "JQKey.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// The one and only application object

CWinApp theApp;
CGCTsk tsk;
TSKMSG tskMsg;			// 并行任务返回的消息结构体

using namespace std;

int randEx()
{
	LARGE_INTEGER seed;
	QueryPerformanceFrequency(&seed);
	QueryPerformanceCounter(&seed);
	srand(seed.QuadPart);
	
	return rand()%1000000+1;
}

void SendTaskMsg( TSKMSG_TYPE nType, int lPar=0 )
{
	TSKMSG tskMsg; memset( &tskMsg,0,sizeof(tskMsg) ); 
	tskMsg.wParam = nType;
	tskMsg.lParam = lPar;
	tsk.SendTskMsg( tskMsg ); 
}

bool CreateFlagFile(CString strFlag)
{
	FILE *fp=NULL;
	fp = fopen(strFlag, "w"); if (fp==NULL) { printf("Create Flag File fails: %s\n", strFlag); return false; }
	
	fprintf(fp, "任务状态正常.\n");
	
	fclose(fp);
	return true;
}

bool IMG2BBI(CString strTif,CString strBBI)
{
	CSpVZImage imgTif;
	if (!imgTif.Open(strTif,CSpVZImage::modeRead))
	{
		cout << "TIF2BBI: Open Tif File Field" << endl;
		return false;
	}

	CSpBBImage2 imgBBI;
	if (!imgBBI.Open(strBBI,CSpBBImage::modeCreate))
	{
		cout << "TIF2BBI: Create BBI File Field" << endl;
		imgTif.Close();	
		return false;
	}

	int nTables=0;
	const RGBQUAD *pTableTif=imgTif.GetColorTable(nTables);
	int nRows=imgTif.GetRows();
	int nCols=imgTif.GetCols();
	int nBPPs=imgTif.GetPixelBytes();

	int nBlkSize=1024;
	imgBBI.SetRows(nRows);
	imgBBI.SetCols(nCols);
	imgBBI.SetPixelBytes(nBPPs);
	imgBBI.SetBlockSize(nBlkSize);

	if (nTables!=0)
	{
		RGBQUAD *pTableBBI=new RGBQUAD[nTables];
		memcpy(pTableBBI,pTableTif,nTables*sizeof(RGBQUAD));
		imgBBI.SetColorTable(pTableBBI,nTables);
		delete pTableBBI;
	}
	else
	{
		RGBQUAD rgbq[256];
		for (int p=0;p<256;p++) rgbq[p].rgbBlue=rgbq[p].rgbGreen=rgbq[p].rgbRed=rgbq[p].rgbReserved=p;
		imgBBI.SetColorTable(rgbq,256);
	}

	int blkRows=(nRows+nBlkSize-1)/nBlkSize;
	BYTE *pBuf=new BYTE[nBlkSize*nCols*nBPPs];
	memset(pBuf,250,nBlkSize*nCols*nBPPs*sizeof(BYTE));
	for (int r=0;r<blkRows;r++)
	{
		memset(pBuf,0,nBlkSize*nCols*nBPPs*sizeof(BYTE));

		imgTif.Read(pBuf,nBPPs,r*nBlkSize,0,nBlkSize,nCols);
		imgBBI.Write(pBuf,nBPPs,r*nBlkSize,0,nBlkSize,nCols);
	}
	delete pBuf;

	imgBBI.Close();	
	imgTif.Close();	

	return true;
}

bool BBI2IMG(CString strBBI,CString strTif)
{
	CSpBBImage imgBBI;
	if (!imgBBI.Open(strBBI,CSpBBImage::modeRead))
	{
		cout << "BBI2Tif:  Open BBI File Field" << endl;
		return false;
	}

	CStDPGImage imgTif;
	if (!imgTif.Open(strTif,CStDPGImage::modeCreate))
	{
		cout << "BBI2Tif: Create BBI File Field" << endl;
		imgBBI.Close();
		return false;
	}

	int nTables=256;
	const RGBQUAD *pTableBBI=imgBBI.GetColorTable();
	int nBlkSize=imgBBI.GetBlockSize();
	int nRows=imgBBI.GetRows();
	int nCols=imgBBI.GetCols();
	int nBPPs=imgBBI.GetPixelBytes();

	imgTif.SetRows(nRows);
	imgTif.SetCols(nCols);
	imgTif.SetPixelBytes(nBPPs);

	RGBQUAD *pTableTif=new RGBQUAD[nTables];
	memcpy(pTableTif,pTableBBI,nTables*sizeof(RGBQUAD));
	imgTif.SetColorTable(pTableTif,nTables);
	delete pTableTif;
	
	int blkRows=(nRows+nBlkSize-1)/nBlkSize;
	BYTE *pBuf=new BYTE[nBlkSize*nCols*nBPPs];
	
	for (int r=0;r<blkRows;r++)
	{
		memset(pBuf,0,nBlkSize*nCols*nBPPs*sizeof(BYTE));
		imgBBI.Read(pBuf,nBPPs,r*nBlkSize,0,nBlkSize,nCols);
		imgTif.WriteEx(pBuf,r*nBlkSize,0,nBlkSize,nCols);
	}
	delete pBuf;

	imgTif.Close();	
	imgBBI.Close();	
	
	return true;
}

int Dodging(CString str)
{
 	CString strMsg="";

	int tasktype;
	CString strTmp;
	CString strImg;		//原始影像路径
	CString strSave;	//匀光结果路径
	CString strStd;		//标准模版路径
	int winsize, dodgtype;
	
	CString strFlag=str + ".log";

	FILE* fpPal;
	if ((fpPal=fopen(str, "r"))==NULL)
	{
		SendTaskMsg(TSKDODGE_ERROR);
		return 0;
	}

	fscanf( fpPal, "%d", &tasktype );
	fscanf( fpPal, "%s", strTmp );
	strImg.Format("%s", strTmp);

	fscanf( fpPal, "%s", strTmp );
	strSave.Format("%s", strTmp);

	fscanf( fpPal, "%s", strTmp );
	strStd.Format("%s", strTmp);

	fscanf( fpPal, "%d", &winsize );
	fscanf( fpPal, "%d", &dodgtype );

	fclose(fpPal);

 	strMsg.Format("WinSize:%d",winsize);
 	cout << (LPCTSTR)strMsg << endl;

 	strMsg.Format("DogType:%d",dodgtype);
 	cout << (LPCTSTR)strMsg << endl;

	if (tasktype<1||tasktype>3)
	{
		strMsg.Format("TaskType Error!");
		cout << (LPCTSTR)strMsg << endl;
		SendTaskMsg(TSKDODGE_ERROR);
		return 0;
	}

	if (winsize<=0)
	{
		strMsg.Format("WinSize Error!");
		cout << (LPCTSTR)strMsg << endl;
		SendTaskMsg(TSKDODGE_ERROR);
		return 0;
	}

	if (dodgtype<0||dodgtype>2)
	{
		strMsg.Format("WinSize Error!");
		cout << (LPCTSTR)strMsg << endl;
		SendTaskMsg(TSKDODGE_ERROR);
		return 0;
	}
 	// 预处理完毕

/*//copy DIM files beg!
	CFileFind finder;
	CString dimfile = strImg + ".DIM";
	char atDir[512], domDir[512], *pS=NULL; strcpy(atDir, strImg); strcpy(domDir, strSave);
	
	pS = strrchr(atDir, '\\'); if (!pS) pS = strrchr( atDir,'/' ); if (pS) *pS = 0;
	pS = strrchr(domDir, '\\'); if (!pS) pS = strrchr( domDir,'/' ); if (pS) *pS = 0;
	
	if (finder.FindFile(dimfile))
	{
		// 打开DIM文件
		FILE *fDIM = fopen(dimfile, "r");
		if(!fDIM) { fprintf( fDIM, "Can't open DIM file!\n" ); return FALSE; }
		
		CopyFile( dimfile, strSave+".DIM", FALSE);
		
		char fileSrc[512], fileDes[512], strRead[256];
		fscanf( fDIM, "SENSOR_NUM=%s\n", strRead );
		fscanf( fDIM, "SENSOR_ID=%s\n", strRead );
		if (strcmp(strRead, "JqADS40.ADS40Line.1")==0)
		{
			fscanf( fDIM, "CAMERA_FILE=%s\n", strRead );
			sprintf( fileSrc, "%s\\%s", atDir, strRead ); sprintf( fileDes, "%s\\%s", domDir, strRead );
			CopyFile( fileSrc, fileDes, FALSE );
			
			fscanf( fDIM, "SUPPORT_FILE=%s\n", strRead );
			sprintf( fileSrc, "%s\\%s", atDir, strRead ); sprintf( fileDes, "%s\\%s", domDir, strRead );
			CopyFile( fileSrc, fileDes, FALSE );
			
			fscanf( fDIM, "POS_FILE=%s\n", strRead );
			sprintf( fileSrc, "%s\\%s", atDir, strRead ); sprintf( fileDes, "%s\\%s", domDir, strRead );
			CopyFile( fileSrc, fileDes, FALSE );
			
			fscanf( fDIM, "AOP_FILE=%s\n", strRead );
			sprintf( fileSrc, "%s\\%s", atDir, strRead ); sprintf( fileDes, "%s\\%s", domDir, strRead );
			CopyFile( fileSrc, fileDes, FALSE );
		}
		fclose(fDIM);

		strMsg = "Copy Dim file OK!";
		cout << (LPCTSTR)strMsg << endl;
	}
	else
	{
		strMsg = "Can't Copy Dim file!";
		cout << (LPCTSTR)strMsg << endl;
		SendTaskMsg(TSKDODGE_ERROR);
		return 0;
	}
	SendTaskMsg(TSKDODGE_STEP, 5);

//copy DIM files end!*/
	CAdjustLightColor adjcal;

	int nBpp=0;
	float fVars[4],fMeans[4];
	RGBQUAD back[2];
	back[0].rgbBlue=back[0].rgbGreen=back[0].rgbRed=back[0].rgbReserved=0;
	back[1].rgbBlue=back[1].rgbGreen=back[1].rgbRed=back[1].rgbReserved=255;
	if (tasktype==1||tasktype==3)
	{
		CxImage imgStd;
		if (!imgStd.Load(strStd)) 
		{
			strMsg = "Can't Open Color Standard Image!";
			cout << (LPCTSTR)strMsg << endl;
			SendTaskMsg(TSKDODGE_ERROR);
			return 0;
		}
		adjcal.GetColorStatistic(&imgStd,nBpp,fVars,fMeans,back,2);	
		imgStd.Destroy();

		strMsg = "Color Standard Statistic OK!";
		cout << (LPCTSTR)strMsg << endl;
	}	
	// 匀色模版标准统计完毕
	SendTaskMsg(TSKDODGE_STEP, 15);

	CString strExt=strImg.Right(3);
	strExt.MakeLower();
	if (strExt=="bbi")
	{		
		if (tasktype==1||tasktype==3)
		{
			CString strCtb=strImg+".ctb";
			DeleteFile(strCtb);
			if (!adjcal.AdjustBBIColor(strImg,nBpp,fVars,fMeans,back,2))
			{
				strMsg = "Can't AdjustColor!";
				cout << (LPCTSTR)strMsg << endl;
				SendTaskMsg(TSKDODGE_ERROR);
				return 0;
			}
			
			if (tasktype==1)
			{
				CSpBBImage2 bbi;
				if (!bbi.Open(strImg))
				{
					strMsg = "Can't Copy File!";
					cout << (LPCTSTR)strMsg << endl;
					SendTaskMsg(TSKDODGE_ERROR);
					return 0;
				}
				if (!bbi.SaveAs(strSave))
				{
					strMsg = "Can't SaveAs File!";
					cout << (LPCTSTR)strMsg << endl;
					SendTaskMsg(TSKDODGE_ERROR);
					return 0;					
				}
				bbi.Close();
			}
			strMsg = "AdjustColor OK!";
			cout << (LPCTSTR)strMsg << endl;
		}
		SendTaskMsg(TSKDODGE_STEP, 50);
		
		if (tasktype==2||tasktype==3)
		{
			strMsg = "Begining Dodging!";
			cout << (LPCTSTR)strMsg << endl;
			if (!adjcal.BBISubDodging(strImg,strSave,winsize))
			{
				strMsg = "Can't Dodging!";
				cout << (LPCTSTR)strMsg << endl;
				SendTaskMsg(TSKDODGE_ERROR);
				return 0;
			}
			strMsg = "Dodging OK!";
			cout << (LPCTSTR)strMsg << endl;
		}
	
		SendTaskMsg(TSKDODGE_STEP, 100);
		for (int i=0; i!=5; i++) { SendTaskMsg( TSKDODGE_OVER ); Sleep(16); } 
		Sleep(512); // 稍微等一下，保证消息已经发出，在结束退出
		CreateFlagFile(strFlag);
		
		return 1;
	}

	CxImage img;
	if (img.Load(strImg))
	{
		if (tasktype==1||tasktype==3)
		{
			if (!adjcal.AdjustColor(&img,nBpp,fVars,fMeans,back,2))
			{
				strMsg = "Can't AdjustColor!";
				cout << (LPCTSTR)strMsg << endl;
				SendTaskMsg(TSKDODGE_ERROR);
				return 0;
			}
			
			strMsg = "AdjustColor OK!";
			cout << (LPCTSTR)strMsg << endl;
		}
		// 匀色完毕
		SendTaskMsg(TSKDODGE_STEP, 40);
		
		if (tasktype==2||tasktype==3) 
		{
			if (dodgtype==0) adjcal.SimpleTemplate(&img,winsize);
			else if(dodgtype==1) adjcal.SubDodging(&img,winsize);
			else if(dodgtype==2) adjcal.DivideDodging(&img,winsize);
			else
			{
				strMsg = "Can't Dodging!";
				cout << (LPCTSTR)strMsg << endl;
				SendTaskMsg(TSKDODGE_ERROR);
				return 0;
			}
			
			strMsg = "Dodging OK!";
			cout << (LPCTSTR)strMsg << endl;
		}
		// 匀光完毕
		SendTaskMsg(TSKDODGE_STEP, 80);
		
		int ntype=0;
		str=strSave;
		str=str.Right(3);
		if (str=="tif"||str=="TIF") ntype=CXIMAGE_FORMAT_TIF;
		else if (str=="bmp"||str=="BMP") ntype=CXIMAGE_FORMAT_BMP;
		else if (str=="jpg"||str=="JPG") ntype=CXIMAGE_FORMAT_JPG;
		else 
		{
			strMsg = "Not TIF/BMP/JPG File Format!";
			cout << (LPCTSTR)strMsg << endl;
		}
		img.Save(strSave,ntype);
		img.Destroy();
		
		strMsg = "Save OK!";
		cout << (LPCTSTR)strMsg << endl;

		SendTaskMsg(TSKDODGE_STEP, 100);
		for (int i=0; i!=5; i++) { SendTaskMsg( TSKDODGE_OVER ); Sleep(16); } 
		Sleep(512); // 稍微等一下，保证消息已经发出，在结束退出
		CreateFlagFile(strFlag);
		
		return 1;
	}

	strMsg = "Can't Open Image!";
	cout << (LPCTSTR)strMsg << endl;

	strMsg = "Convert to BBI!";
	cout << (LPCTSTR)strMsg << endl;

	CString strBBITmp1=strSave.Left(strSave.ReverseFind('.'))+"_tmp1.bbi";
	CString strBBITmp2=strSave.Left(strSave.ReverseFind('.'))+"_tmp2.bbi";
	IMG2BBI(strImg,strBBITmp1);

	SendTaskMsg(TSKDODGE_STEP, 30);
	
	if (tasktype==1||tasktype==3)
	{
		CString strCtb=strBBITmp1+".ctb";
		DeleteFile(strCtb);
		if (!adjcal.AdjustBBIColor(strBBITmp1,nBpp,fVars,fMeans,back,2))
		{
			strMsg = "Can't AdjustColor!";
			cout << (LPCTSTR)strMsg << endl;
			SendTaskMsg(TSKDODGE_ERROR);
			return 0;
		}
		
		if (tasktype==1)
		{
			CSpBBImage2 bbi;
			if (!bbi.Open(strBBITmp1))
			{
				strMsg = "Can't Copy File!";
				cout << (LPCTSTR)strMsg << endl;
				SendTaskMsg(TSKDODGE_ERROR);
				return 0;
			}
			if (!bbi.SaveAs(strBBITmp2))
			{
				strMsg = "Can't SaveAs File!";
				cout << (LPCTSTR)strMsg << endl;
				SendTaskMsg(TSKDODGE_ERROR);
				return 0;					
			}
			bbi.Close();
		}
		strMsg = "AdjustColor OK!";
		cout << (LPCTSTR)strMsg << endl;
	}
	SendTaskMsg(TSKDODGE_STEP, 50);
	
	if (tasktype==2||tasktype==3)
	{
		strMsg = "Begining Dodging!";
		cout << (LPCTSTR)strMsg << endl;
		if (!adjcal.BBISubDodging(strBBITmp1,strBBITmp2,winsize))
		{
			strMsg = "Can't Dodging!";
			cout << (LPCTSTR)strMsg << endl;
			SendTaskMsg(TSKDODGE_ERROR);
			return 0;
		}
		strMsg = "Dodging OK!";
		cout << (LPCTSTR)strMsg << endl;
	}
	SendTaskMsg(TSKDODGE_STEP, 80);

	BBI2IMG(strBBITmp2,strSave);
	DeleteFile(strBBITmp1);
	DeleteFile(strBBITmp1+".ctb");
	DeleteFile(strBBITmp2);

	SendTaskMsg(TSKDODGE_STEP, 100);
	for (int i=0; i!=5; i++) { SendTaskMsg( TSKDODGE_OVER ); Sleep(16); } 
	Sleep(512); // 稍微等一下，保证消息已经发出，在结束退出
	CreateFlagFile(strFlag);
	
	return 1;
}

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{

	int nRetCode = 0;

	// initialize MFC and print and error on failure
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: change error code to suit your needs
		cerr << _T("Fatal Error: MFC initialization failed") << endl;
		nRetCode = 1;
	}
	else
	{
		// TODO: code your application's behavior here.
		CString strHello;
		strHello.LoadString(IDS_HELLO);
		cout << (LPCTSTR)strHello << endl;
	}

	CString str=::GetCommandLine();

	int marksp=str.Find(' ');
	if (marksp==-1) 
	{
		SendTaskMsg(TSKDODGE_ERROR);
		return 0;
	}
	str=str.Right(str.GetLength()-marksp-1);
	cout << (LPCTSTR)str << endl;

	SendTaskMsg(TSKDODGE_START); Sleep(16); SendTaskMsg(TSKDODGE_START); Sleep(16); SendTaskMsg(TSKDODGE_START);

	if (str.GetLength()) 
		return Dodging(str);

	return nRetCode;
}

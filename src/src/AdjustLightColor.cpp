// AdjustLightColor.cpp: implementation of the CAdjustLightColor class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AdjustLightColor.h"

#include "fft.h"
#include "SpVZImage.h"
#include "SpBBImage.h"
#include "StDPGImage.h"

using namespace std;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#ifndef PI
#define PI 3.141592653589793f
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAdjustLightColor::CAdjustLightColor()
{
	nAr=NULL;
	nAi=NULL;
	nWsizex=nWsizey=0;

	nFtype=-1;
	nD0=nRl=nRh=0.0;
}

CAdjustLightColor::~CAdjustLightColor()
{
	if (nAr!=NULL) delete nAr;
	if (nAi!=NULL) delete nAi;

	nAr=NULL;
	nAi=NULL;

	nWsizex=nWsizey=0;
}

void CAdjustLightColor::GetColorStatistic(CxImage *pImg, int &bpp, float fvars[], float fmeans[],RGBQUAD* pBackGrd,int nBackGrd)
{
	bpp=pImg->GetBpp();
	long pHisto[256],pHistor[256],pHistog[256],pHistob[256],nUsefulSize;
	BYTE lMin[4],lMax[4];

	if(bpp==8)
	{
		nUsefulSize=Histogram(pImg,NULL,NULL,NULL,pHisto,pBackGrd,nBackGrd);
		HistoStatis(nUsefulSize,NULL,NULL,NULL,pHisto,fvars,fmeans,lMin,lMax);
	}
	else if(bpp==24)
	{
		nUsefulSize=Histogram(pImg,pHistor,pHistog,pHistob,NULL,pBackGrd,nBackGrd);
		HistoStatis(nUsefulSize,pHistor,pHistog,pHistob,NULL,fvars,fmeans,lMin,lMax);
	}
}

bool CAdjustLightColor::GetColorStatistic16Bit(CString strDir,int &bpp,float fvars[4],float fmeans[4])
{
	CStDPGImage DPGimg;
	if (!DPGimg.Open(strDir)) return false;

	int smpbytes=DPGimg.GetSampleBytes();
	int bytepp=DPGimg.GetPixelSamples();
	if(smpbytes!=2||(bytepp!=1&&bytepp!=3))
	{
		DPGimg.Close();
		return false;
	}
	bpp=bytepp*smpbytes*8;

	int hei=DPGimg.GetRows();
	int wid=DPGimg.GetCols();
	CStDPGImage::SAMP_STOR smStor=DPGimg.GetSampleStorage();

	if (bytepp==1)
	{
		WORD *pData=new WORD[wid*hei];
		memset(pData,0,wid*hei*sizeof(WORD));

		DPGimg.ReadEx((BYTE *)pData,0,0,hei,wid,smStor);
		GetColorStatistic16Bit(pData,wid,hei,fvars[3],fmeans[3]);

		delete pData;
	}
	else
	{
		WORD *pDataR=new WORD[wid*hei];
		memset(pDataR,0,wid*hei*sizeof(WORD));

		WORD *pDataG=new WORD[wid*hei];
		memset(pDataG,0,wid*hei*sizeof(WORD));

		WORD *pDataB=new WORD[wid*hei];
		memset(pDataB,0,wid*hei*sizeof(WORD));

		int i=0,j=0;
		WORD *pRow=new WORD[wid*3];
		WORD *lp,*lpR=pDataR,*lpG=pDataG,*lpB=pDataB;	

		for (i=0;i<hei;i++)
		{
			memset(pRow,0,wid*sizeof(WORD));
			DPGimg.ReadEx((BYTE *)pRow,i,smStor);	
			lp=pRow;

			for (j=0;j<wid;j++)
			{
				*lpR=*lp; lpR++; lp++;
				*lpG=*lp; lpG++; lp++;
				*lpB=*lp; lpB++; lp++;
			}
		}

		GetColorStatistic16Bit(pDataR,wid,hei,fvars[0],fmeans[0]);
		GetColorStatistic16Bit(pDataG,wid,hei,fvars[1],fmeans[1]);
		GetColorStatistic16Bit(pDataB,wid,hei,fvars[2],fmeans[2]);

		delete pDataR;
		delete pDataG;
		delete pDataB;
		delete pRow;
	}

	DPGimg.Close();
	return true;	
}

void CAdjustLightColor::GetColorStatistic16Bit(WORD *pData,int wid,int hei,float &fvars,float &fmeans)
{
	WORD lMin,lMax;
	long pHisto[65536]={0};
	long nUsefulSize=Histogram16Bit(pData,wid,hei,pHisto);
	HistoStatis16Bit(nUsefulSize,pHisto,fvars,fmeans,lMin,lMax);
}

long CAdjustLightColor::Histogram(CxImage *pImg, long *pRed, long *pGreen, long *pBlue, long *pGray,RGBQUAD *pBackGrd,int nBackGrd)
{
	int iBit=pImg->GetBpp();
	if(iBit!=8 && iBit!=24) return -1;

	long nHei=pImg->GetHeight();
	long nWid=pImg->GetWidth();
	long nScanWid=pImg->GetEffWidth();
	BYTE* lpBits=pImg->GetBits();
	BYTE* lpPos;
	long nHeight=0;

	//统计直方图
	if(pRed!=NULL)		memset(pRed,0x00,sizeof(long)*256);
	if(pGreen!=NULL)	memset(pGreen,0x00,sizeof(long)*256);
	if(pBlue!=NULL)		memset(pBlue,0x00,sizeof(long)*256);
	if(pGray!=NULL)		memset(pGray,0x00,sizeof(long)*256);

	int i,j,r,g,b,gray;
	long nSize=nHei*nWid;

	for(i=0;i<nHei;i++)
	{
		for(j=0;j<nWid;j++)
		{
			lpPos=lpBits+i*nScanWid+j*iBit/8;
			if(nBackGrd>0 && pBackGrd!=NULL)
			{
				if(BeBackground(lpPos,iBit,pBackGrd,nBackGrd)) 
				{
					nSize--;
					continue;
				}
			}

			if(iBit==8)
			{
				gray=*(lpPos);
				pGray[gray]++;
			}
			else
			{
				b=*(lpPos);lpPos++;
				g=*(lpPos);lpPos++;
				r=*(lpPos);

				pBlue[b]++;
				pGreen[g]++;
				pRed[r]++;
				if(pGray)
				{
					gray=int(0.3*r+0.59*g+0.11*b+0.5);
					pGray[gray]++;
				}
			}
		}
	}
	return nSize;
}

void CAdjustLightColor::HistoStatis(long nUseSize, long *pRed, long *pGre, long *pBlu, long *pGra, float fVars[], float fMeans[],BYTE LightMins[],BYTE LightMaxs[])
{
	if(nUseSize<=0) return;
	//计算标准偏差和平均亮度
	for(int j=0;j<4;j++)
	{
		fVars[j]=0.0f;
		fMeans[j]=0.0f;
	}

	for(j=0;j<256;j++)
	{
		if(pRed!=NULL)
			fMeans[0]+=float(pRed[j])/nUseSize*j;
		if(pGre!=NULL)
			fMeans[1]+=float(pGre[j])/nUseSize*j;
		if(pBlu!=NULL)
			fMeans[2]+=float(pBlu[j])/nUseSize*j;
		if(pGra!=NULL)
			fMeans[3]+=float(pGra[j])/nUseSize*j;
	}

	for(j=0;j<256;j++)
	{
		if(pRed!=NULL)
			fVars[0]+=(j-fMeans[0])*(j-fMeans[0])*pRed[j]/nUseSize;
		if(pGre!=NULL)
			fVars[1]+=(j-fMeans[1])*(j-fMeans[1])*pGre[j]/nUseSize;
		if(pBlu!=NULL)
			fVars[2]+=(j-fMeans[2])*(j-fMeans[2])*pBlu[j]/nUseSize;
		if(pGra!=NULL)
			fVars[3]+=(j-fMeans[3])*(j-fMeans[3])*pGra[j]/nUseSize;
	}

	for(j=0;j<4;j++)
	{
		fVars[j]=float(sqrt(fVars[j]));
	}
	//统计最小值和最大值
	//gray
	if(pGra!=NULL)
	{
		for(j=0;j<256;j++)
		{
			if(pGra[j]==0) continue;
			else break;
		}
		LightMins[0]=j;
		for(j=255;j>=0;j--)
		{
			if(pGra[j]==0) continue;
			else break;
		}
		LightMaxs[0]=j;
	}

	//red
	if(pRed!=NULL)
	{
		for(j=0;j<256;j++)
		{
			if(pRed[j]==0) continue;
			else break;
		}
		LightMins[1]=j;
		for(j=255;j>=0;j--)
		{
			if(pRed[j]==0) continue;
			else break;
		}
		LightMaxs[1]=j;
	}

	//green
	if(pGre!=NULL)
	{
		for(j=0;j<256;j++)
		{
			if(pGre[j]==0) continue;
			else break;
		}
		LightMins[2]=j;
		for(j=255;j>=0;j--)
		{
			if(pGre[j]==0) continue;
			else break;
		}
		LightMaxs[2]=j;
	}

	//blue
	if(pBlu!=NULL)
	{
		for(j=0;j<256;j++)
		{
			if(pBlu[j]==0) continue;
			else break;
		}
		LightMins[3]=j;
		for(j=255;j>=0;j--)
		{
			if(pBlu[j]==0) continue;
			else break;
		}
		LightMaxs[3]=j;
	}
}

void CAdjustLightColor::HistoStatis16Bit(long nUseSize, long *pHisto, float &fVars,float &fMeans,WORD &LightMins,WORD &LightMaxs)
{
	if(nUseSize<=0||pHisto==NULL) return;

	fVars=fMeans=0.0f;

	for(int j=0;j<65536;j++) 
		fMeans+=float(pHisto[j])/nUseSize*j;
	for(j=0;j<65536;j++) 
		fVars+=(j-fMeans)*(j-fMeans)*pHisto[j]/nUseSize;

	fVars=float(sqrt(fVars));

	for(j=0;j<65536;j++)
	{
		if(pHisto[j]==0) continue;
		else break;
	}
	LightMins=j;

	for(j=65536;j>=0;j--)
	{
		if(pHisto[j]==0) continue;
		else break;
	}
	LightMaxs=j;	
}

BOOL CAdjustLightColor::BeBackground(BYTE *lpBit, int iBit, RGBQUAD *pBackGrd, int nBackGrd)
{
	RGBQUAD rgb;
	if(iBit==8)
	{
		rgb.rgbBlue=rgb.rgbGreen=rgb.rgbRed=*lpBit;
	}
	else
	{
		rgb.rgbBlue=*lpBit;
		rgb.rgbGreen=*(lpBit+1);
		rgb.rgbRed=*(lpBit+2);
	}
	
	for(int i=0;i<nBackGrd;i++)
	{
		if(rgb.rgbBlue==pBackGrd[i].rgbBlue &&
			rgb.rgbGreen==pBackGrd[i].rgbGreen &&
			rgb.rgbRed==pBackGrd[i].rgbRed)
			return TRUE;
	}
	return FALSE;
}

//void CAdjustLightColor::WallisGlobal(CxImage *pImg,float fVarRef,float fMeanRef,float fVarCur,float fMeanCur,RGBQUAD *pBackGrd,int nBackGrd)
//{
//	int iBit=pImg->GetBpp();
//	if(iBit!=8) return;
//
//	int i,j,iv;
//	int nHei=pImg->GetHeight();
//	int nWid=pImg->GetWidth();
//	int nScanwid=pImg->GetEffWidth();
//	BYTE* lpBits=pImg->GetBits();
//	BYTE* lpOrg;
//	for(i=0;i<nHei;i++)
//	{
//		for(j=0;j<nWid;j++)
//		{
//			lpOrg=lpBits+(nHei-1-i)*nScanwid+j;
//			if(BeBackground(lpOrg,iBit,pBackGrd,nBackGrd)) 	continue;
//
//			iv=*lpOrg;
//			iv=int(fMeanRef+fVarRef/fVarCur*(iv-fMeanCur)+0.5);
//			if(iv>255) iv=255;
//			if(iv<0) iv=0;
//			*lpOrg=iv;
//		}
//	}
//
//	lpBits=NULL;
//	lpOrg=NULL;
//}

void CAdjustLightColor::WallisGlobal(CxImage *pImg,BYTE lut[256],RGBQUAD *pBackGrd,int nBackGrd)
{
	int iBit=pImg->GetBpp();
	if(iBit!=8) return;

	int i,j;
	int nHei=pImg->GetHeight();
	int nWid=pImg->GetWidth();
	int nScanwid=pImg->GetEffWidth();
	BYTE* lpBits=pImg->GetBits();
	BYTE* lpOrg;
	for(i=0;i<nHei;i++)
	{
		for(j=0;j<nWid;j++)
		{
			lpOrg=lpBits+i*nScanwid+j;
			if(BeBackground(lpOrg,iBit,pBackGrd,nBackGrd)) 	continue;

			*lpOrg=lut[*lpOrg];
		}
	}

	lpBits=NULL;
	lpOrg=NULL;
}

void CAdjustLightColor::WallisGlobal(CxImage *pImg,BYTE lutR[256],BYTE lutG[256],BYTE lutB[256],RGBQUAD *pBackGrd,int nBackGrd)
{
	int iBit=pImg->GetBpp();
	if(iBit!=24) return;

	int i,j;
	int nHei=pImg->GetHeight();
	int nWid=pImg->GetWidth();
	int nScanwid=pImg->GetEffWidth();
	BYTE* lpBits=pImg->GetBits();
	BYTE* lpOrg;
	for(i=0;i<nHei;i++)
	{
		for(j=0;j<nWid;j++)
		{
			lpOrg=lpBits+i*nScanwid+j*3;
			if(BeBackground(lpOrg,iBit,pBackGrd,nBackGrd)) 	continue;

			*lpOrg=lutB[*lpOrg];

			lpOrg++;
			*lpOrg=lutG[*lpOrg];

			lpOrg++;
			*lpOrg=lutR[*lpOrg];
		}
	}

	lpBits=NULL;
	lpOrg=NULL;	
}

void CAdjustLightColor::CalculateLut(int iBit,BYTE lutR[256],BYTE lutG[256],BYTE lutB[256],BYTE lutA[256],float fVarRef[4],float fMeanRef[4],float fVarCur[4],float fMeanCur[4])
{
	int val=0;
	for (int i=0;i<256;i++)
	{
		if(iBit==8)
		{
			val=int(fMeanRef[3]+fVarRef[3]/fVarCur[3]*(i-fMeanCur[3])+0.5);
			val=min(max(0,val),255);
			lutA[i]=val;
		}
		else if(iBit==24)
		{
			val=int(fMeanRef[0]+fVarRef[0]/fVarCur[0]*(i-fMeanCur[0])+0.5);
			lutR[i]=min(max(0,val),255);

			val=int(fMeanRef[1]+fVarRef[1]/fVarCur[1]*(i-fMeanCur[1])+0.5);
			lutG[i]=min(max(0,val),255);

			val=int(fMeanRef[2]+fVarRef[2]/fVarCur[2]*(i-fMeanCur[2])+0.5);
			lutB[i]=min(max(0,val),255);
 		}
	}
}

void CAdjustLightColor::CalculateLut16Bit(WORD lut[65536],float fVarRef,float fMeanRef,float fVarCur,float fMeanCur)
{
	int val=0;
	for (int i=0;i<65536;i++)
	{
		val=int(fMeanRef+fVarRef/fVarCur*(i-fMeanCur)+0.5);
		lut[i]=min(max(0,val),65535);
	}
	lut[0]=0;
}

bool CAdjustLightColor::WallisTransform(CxImage *pImg,int stdbiBit,float fVarRef[4],float fMeanRef[4],float fVarCur[4],float fMeanCur[4],RGBQUAD *pBackGrd,int nBackGrd)
{
	int ibit=pImg->GetBpp();
	if (stdbiBit!=ibit) 	return false;

	BYTE lutR[256]={0};
	BYTE lutG[256]={0};
	BYTE lutB[256]={0};
	BYTE lutA[256]={0};
	CalculateLut(ibit,lutR,lutG,lutB,lutA,fVarRef,fMeanRef,fVarCur,fMeanCur);

	if(ibit==8)
	{
		WallisGlobal(pImg,lutA,pBackGrd,nBackGrd);
		return true;
	}
	if(ibit==24)
	{
		WallisGlobal(pImg,lutR,lutG,lutB,pBackGrd,nBackGrd);
		return true;
	}

	return false;
}

void CAdjustLightColor::WallisTransform16Bit(WORD *pData,int wid,int hei,float fVarRef,float fMeanRef,float fVarCur,float fMeanCur)
{
	WORD lut[65536]={0};
	CalculateLut16Bit(lut,fVarRef,fMeanRef,fVarCur,fMeanCur);

	int lsize=wid*hei;
	WORD *lp=pData;
	for (int i=0;i<lsize;i++)
	{
		*lp=lut[*lp];
		lp++;
	}
}

bool CAdjustLightColor::AdjustColor(CxImage *pImg,int stdbiBit,float fVars[4],float fMeans[4],RGBQUAD *pBackGrd,int nBackGrd)
{
	int ibit=pImg->GetBpp();
	if (stdbiBit!=ibit) return false;

	float fVarsCur[4]={0.0};
	float fMeansCur[4]={0.0};
	GetColorStatistic(pImg,ibit,fVarsCur,fMeansCur,pBackGrd,nBackGrd);

	return WallisTransform(pImg,stdbiBit,fVars,fMeans,fVarsCur,fMeansCur,pBackGrd,nBackGrd);
}

bool CAdjustLightColor::AdjustColor16Bit(CString strDir,CString strResDir,int stdbiBit,float fStdVars[4],float fStdMeans[4],BOOL bCompare)
{
	CStDPGImage DPGimg;
	if (!DPGimg.Open(strDir)) return false;

	int smpbytes=DPGimg.GetSampleBytes();
	int bytepp=DPGimg.GetPixelSamples();
	if(DPGimg.GetSampleBytes()!=2||(bytepp!=1&&bytepp!=3)||(stdbiBit!=bytepp*smpbytes*8))
	{
		DPGimg.Close();
		return false;
	}

	CStDPGImage DPGimgRes;
	if (!DPGimgRes.Open(strResDir,CSpVZImage::modeCreate)) 
	{
		DPGimg.Close();
		return false;
	}

	int hei=DPGimg.GetRows();
	int wid=DPGimg.GetCols();
	CStDPGImage::SAMP_STOR smStor=DPGimg.GetSampleStorage();

	DPGimgRes.SetRows(hei);
	DPGimgRes.SetCols(wid);
	DPGimgRes.SetSampleBytes(2);
	DPGimgRes.SetPixelSamples(bytepp);
	DPGimgRes.SetSampleStorage(smStor);
	if (bCompare) DPGimgRes.SetCompress(CStDPGImage::CT_JPG);

	float fvars,fmeans;
	if (bytepp==1)
	{
		WORD *pData=new WORD[wid*hei];
		memset(pData,0,wid*hei*sizeof(WORD));

		DPGimg.ReadEx((BYTE *)pData,0,0,hei,wid,smStor);
		GetColorStatistic16Bit(pData,wid,hei,fvars,fmeans);
		WallisTransform16Bit(pData,wid,hei,fStdVars[3],fStdMeans[3],fvars,fmeans);
		DPGimgRes.WriteEx((BYTE *)pData,0,0,hei,wid,smStor);

		delete pData;
	}
	else 
	{
		WORD *pDataR=new WORD[wid*hei];
		memset(pDataR,0,wid*hei*sizeof(WORD));

		WORD *pDataG=new WORD[wid*hei];
		memset(pDataG,0,wid*hei*sizeof(WORD));

		WORD *pDataB=new WORD[wid*hei];
		memset(pDataB,0,wid*hei*sizeof(WORD));

		int i=0,j=0;
		WORD *pRow=new WORD[wid*3];
		WORD *lp,*lpR=pDataR,*lpG=pDataG,*lpB=pDataB;	

		for (i=0;i<hei;i++)
		{
			memset(pRow,0,wid*sizeof(WORD));
			DPGimg.ReadEx((BYTE *)pRow,i,smStor);	
			lp=pRow;

			for (j=0;j<wid;j++)
			{
				*lpR=*lp; lpR++; lp++;
				*lpG=*lp; lpG++; lp++;
				*lpB=*lp; lpB++; lp++;
			}
		}

		GetColorStatistic16Bit(pDataR,wid,hei,fvars,fmeans);
		WallisTransform16Bit(pDataR,wid,hei,fStdVars[0],fStdMeans[0],fvars,fmeans);

		GetColorStatistic16Bit(pDataG,wid,hei,fvars,fmeans);
		WallisTransform16Bit(pDataG,wid,hei,fStdVars[1],fStdMeans[1],fvars,fmeans);

		GetColorStatistic16Bit(pDataB,wid,hei,fvars,fmeans);
		WallisTransform16Bit(pDataB,wid,hei,fStdVars[2],fStdMeans[2],fvars,fmeans);

		lpR=pDataR;
		lpG=pDataG;
		lpB=pDataB;	
		
		for (i=0;i<hei;i++)
		{
			lp=pRow;
			for (j=0;j<wid;j++)
			{
				*lp=*lpR; lpR++; lp++;
				*lp=*lpG; lpG++; lp++;
				*lp=*lpB; lpB++; lp++;
			}
			DPGimgRes.WriteEx((BYTE *)pRow,i,smStor);
		}

		delete pDataR;
		delete pDataG;
		delete pDataB;
		delete pRow;
	}

	DPGimg.Close();
	DPGimgRes.Close();
 	return true;
}

void CAdjustLightColor::BlockAdjustColor(CxImage *pImg, int nWidBlock, int nHeiBlock, int nOverlapeWid,int stdbiBit, float fvars[], float fmeans[],RGBQUAD *pBackGrd,int nBackGrd)
{
	int imgwidth=pImg->GetWidth();
	int imgheight=pImg->GetHeight();

	int reswid=imgwidth%nWidBlock;
	int reshei=imgheight%nHeiBlock;

	if (reswid||reshei)
		pImg->Resample2(imgwidth-reswid+nWidBlock,imgheight-reshei+nHeiBlock);

	BlockAdjust(pImg, nWidBlock, nHeiBlock, nOverlapeWid,stdbiBit, fvars, fmeans,pBackGrd,nBackGrd);

	if (reswid||reshei)
		pImg->Resample2(imgwidth,imgheight);
}

void CAdjustLightColor::BlockAdjust(CxImage *pImg, int nWidBlock, int nHeiBlock, int nOverlapeWid,int stdbiBit, float fvars[], float fmeans[],RGBQUAD *pBackGrd,int nBackGrd)
{
	int imgwidth=pImg->GetWidth();
	int imgheight=pImg->GetHeight();

	if(imgwidth%nWidBlock) return;
	if(imgheight%nHeiBlock) return;

	int blockhei=imgheight/nHeiBlock;

	CxImage tempimg(*pImg);
	BYTE *pBits=tempimg.GetBits();
	long size=imgheight*tempimg.GetEffWidth();
	memset(pBits,0,size*sizeof(BYTE));

	CRect currect(0,0,0,0);
	CxImage smallimg;
	for(int j=0;j<nHeiBlock;j++)
	{
		currect.left=0;
		currect.right=imgwidth;

		currect.top=j*blockhei-nOverlapeWid;
		if(currect.top<0) currect.top=0;

		currect.bottom=(j+1)*blockhei+nOverlapeWid;
		if(currect.bottom>imgheight) currect.bottom=imgheight;

		pImg->Crop(currect,&smallimg);

		LocalBlockAdjust(&smallimg,nWidBlock,nOverlapeWid,fvars,fmeans,stdbiBit,pBackGrd,nBackGrd);
		CombainSmallPhoto(&tempimg,&smallimg,currect,nOverlapeWid,2);

		smallimg.Destroy();
	}

	*pImg=tempimg;
	tempimg.Destroy();
}

void CAdjustLightColor::LocalBlockAdjust(CxImage *pImg, int nWidBlock, int nOverlapeWid, float fvars[], float fmeans[],int stdbiBit,RGBQUAD *pBackGrd,int nBackGrd)
{
	int imgwidth=pImg->GetWidth();
	int imgheight=pImg->GetHeight();

	if(imgwidth%nWidBlock) return;

	int blockwid=imgwidth/nWidBlock;

	CxImage tempimg(*pImg);
	BYTE *pBits=tempimg.GetBits();
	long size=imgheight*tempimg.GetEffWidth();
	memset(pBits,0,size*sizeof(BYTE));

	CRect currect(0,0,0,0);
	CxImage smallimg;
	for(int i=0;i<nWidBlock;i++)
	{
		currect.left=i*blockwid-nOverlapeWid;
		if(currect.left<0) currect.left=0;

		currect.right=(i+1)*blockwid+nOverlapeWid;
		if(currect.right>imgwidth) currect.right=imgwidth;

		currect.top=0;
		currect.bottom=imgheight;

		pImg->Crop(currect,&smallimg);
		AdjustColor(&smallimg,stdbiBit,fvars,fmeans,pBackGrd,nBackGrd);
		CombainSmallPhoto(&tempimg,&smallimg,currect,nOverlapeWid,1);

		smallimg.Destroy();
	}
	*pImg=tempimg;
	tempimg.Destroy();
}

void CAdjustLightColor::CombainSmallPhoto(CxImage *pBigImg, CxImage *pSmall, CRect rect, int overlape,int verhor)
{
	int bigwidth=pBigImg->GetWidth();
	int bigheight=pBigImg->GetHeight();

	int bigoffsetx=rect.left+2*overlape;
	if(rect.left==0) bigoffsetx=0;

	int bigoffsety=rect.top+2*overlape;
	if(rect.top==0) bigoffsety=0;

	int smallwidth=pSmall->GetWidth();
	int smallheight=pSmall->GetHeight();

	int smalloffsetx=2*overlape;
	if(rect.left==0) smalloffsetx=0; 

	int smalloffsety=2*overlape;
	if(rect.top==0) smalloffsety=0;

	int width=smallwidth-smalloffsetx-2*overlape;
	if(rect.right==bigwidth) width=smallwidth-smalloffsetx;
	int height=smallheight-smalloffsety-2*overlape;
	if(rect.bottom==bigheight) height=smallheight-smalloffsety;

	RGBQUAD color;
	for(int i=0;i<width;i++)
	{
		for(int j=0;j<height;j++)
		{
			color=pSmall->GetPixelColor(smalloffsetx+i,smallheight-(smalloffsety+j)-1,false);
			pBigImg->SetPixelColor(bigoffsetx+i,bigheight-(bigoffsety+j)-1,color);
		}
	}

	double *val=new double[2*overlape];
	for(i=0;i<overlape;i++) val[i]=(i+1)*(1.0/(2*overlape));
	for(i=overlape;i<2*overlape;i++) val[i]=i*(1.0/(2*overlape));

	RGBQUAD colors;
	RGBQUAD colorb;
	double tempgray=0;
	if(verhor==1) //左右边界羽化
	{
		if(rect.left!=0)
		{
			for(int j=0;j<smallheight;j++)
			{
				for(int i=0;i<2*overlape;i++)
				{
					colors=pSmall->GetPixelColor(i,smallheight-(smalloffsety+j)-1,false);
					colorb=pBigImg->GetPixelColor(rect.left+i,bigheight-(bigoffsety+j)-1,false);
					
					colorb.rgbBlue=tempgray=(double)colors.rgbBlue*val[i]+(double)colorb.rgbBlue+0.5;
					if(tempgray>255)  colorb.rgbBlue=255;

					colorb.rgbGreen=tempgray=(double)colors.rgbGreen*val[i]+(double)colorb.rgbGreen+0.5;
					if(tempgray>255)  colorb.rgbGreen=255;

					colorb.rgbRed=tempgray=(double)colors.rgbRed*val[i]+(double)colorb.rgbRed+0.5;
					if(tempgray>255)  colorb.rgbRed=255;

					pBigImg->SetPixelColor(rect.left+i,bigheight-(bigoffsety+j)-1,colorb);
				}
			}
		}
		if(rect.right!=bigwidth)
		{
			for(int j=0;j<smallheight;j++)
			{
				for(int i=0;i<2*overlape;i++)
				{
					colors=pSmall->GetPixelColor(smallwidth-i-1,smallheight-(smalloffsety+j)-1,false);
					colorb=pBigImg->GetPixelColor(rect.right-i-1,bigheight-(bigoffsety+j)-1,false);
					
					colorb.rgbBlue=tempgray=(double)colors.rgbBlue*val[i]+(double)colorb.rgbBlue+0.5;
					if(tempgray>255)  colorb.rgbBlue=255;

					colorb.rgbGreen=tempgray=(double)colors.rgbGreen*val[i]+(double)colorb.rgbGreen+0.5;
					if(tempgray>255)  colorb.rgbGreen=255;

					colorb.rgbRed=tempgray=(double)colors.rgbRed*val[i]+(double)colorb.rgbRed+0.5;
					if(tempgray>255)  colorb.rgbRed=255;

					pBigImg->SetPixelColor(rect.right-i-1,bigheight-(bigoffsety+j)-1,colorb);
				}
			}
		}
	}

	if(verhor==2) //上下边界羽化
	{
		if(rect.top!=0)
		{
			for(int i=0;i<smallwidth;i++)
			{
				for(int j=0;j<2*overlape;j++)
				{
					colors=pSmall->GetPixelColor(i,smallheight-j-1,false);
					colorb=pBigImg->GetPixelColor(rect.left+i,bigheight-(rect.top+j)-1,false);

					colorb.rgbBlue=tempgray=(double)colors.rgbBlue*val[j]+(double)colorb.rgbBlue+0.5;
					if(tempgray>255)  colorb.rgbBlue=255;

					colorb.rgbGreen=tempgray=(double)colors.rgbGreen*val[j]+(double)colorb.rgbGreen+0.5;
					if(tempgray>255)  colorb.rgbGreen=255;

					colorb.rgbRed=tempgray=(double)colors.rgbRed*val[j]+(double)colorb.rgbRed+0.5;
					if(tempgray>255)  colorb.rgbRed=255;

					pBigImg->SetPixelColor(rect.left+i,bigheight-(rect.top+j)-1,colorb);
				}
			}
		}
		if(rect.bottom!=bigheight)
		{
			for(int i=0;i<smallwidth;i++)
			{
				for(int j=0;j<2*overlape;j++)
				{
					colors=pSmall->GetPixelColor(i,j,false);
					colorb=pBigImg->GetPixelColor(rect.left+i,bigheight-(rect.bottom-j),false);

					colorb.rgbBlue=tempgray=(double)colors.rgbBlue*val[j]+(double)colorb.rgbBlue+0.5;
					if(tempgray>255)  colorb.rgbBlue=255;

					colorb.rgbGreen=tempgray=(double)colors.rgbGreen*val[j]+(double)colorb.rgbGreen+0.5;
					if(tempgray>255)  colorb.rgbGreen=255;

					colorb.rgbRed=tempgray=(double)colors.rgbRed*val[j]+(double)colorb.rgbRed+0.5;
					if(tempgray>255)  colorb.rgbRed=255;

					pBigImg->SetPixelColor(rect.left+i,bigheight-(rect.bottom-j),colorb);
				}
			}
		}
	}
	delete val;
}

bool CAdjustLightColor::SimpleTemplate16Bit(CString strDir,CString strResDir,int iSize,BOOL bCompare)
{
	CStDPGImage DPGimg;
	if (!DPGimg.Open(strDir)) return false;

	int bytepp=DPGimg.GetPixelSamples();
	if(DPGimg.GetSampleBytes()!=2||(bytepp!=1&&bytepp!=3))
	{
		DPGimg.Close();
		return false;
	}

	CStDPGImage DPGimgRes;
	if (!DPGimgRes.Open(strResDir,CSpVZImage::modeCreate)) 
	{
		DPGimg.Close();
		return false;
	}

	int hei=DPGimg.GetRows();
	int wid=DPGimg.GetCols();
	CStDPGImage::SAMP_STOR smStor=DPGimg.GetSampleStorage();

	DPGimgRes.SetRows(hei);
	DPGimgRes.SetCols(wid);
	DPGimgRes.SetSampleBytes(2);
	DPGimgRes.SetPixelSamples(bytepp);
	DPGimgRes.SetSampleStorage(smStor);
	if (bCompare) DPGimgRes.SetCompress(CStDPGImage::CT_JPG);
	
	if (bytepp==1)
	{
		WORD *pData=new WORD[wid*hei];
		memset(pData,0,wid*hei*sizeof(WORD));
		long pHisto[65536]={0};

		DPGimg.ReadEx((BYTE *)pData,0,0,hei,wid,smStor);
		Histogram16Bit(pData,wid,hei,pHisto);
		ChannelByTemplate16Bit(pData,wid,hei,pHisto,iSize);
		DPGimgRes.WriteEx((BYTE *)pData,0,0,hei,wid,smStor);

		delete pData;
	}
	else 
	{
		WORD *pDataR=new WORD[wid*hei];
		memset(pDataR,0,wid*hei*sizeof(WORD));
		long pHisto[65536]={0};

		WORD *pDataG=new WORD[wid*hei];
		memset(pDataG,0,wid*hei*sizeof(WORD));

		WORD *pDataB=new WORD[wid*hei];
		memset(pDataB,0,wid*hei*sizeof(WORD));

		int i=0,j=0;
		WORD *pRow=new WORD[wid*3];
		WORD *lp,*lpR=pDataR,*lpG=pDataG,*lpB=pDataB;	

		for (i=0;i<hei;i++)
		{
			memset(pRow,0,wid*3*sizeof(WORD));
			DPGimg.ReadEx((BYTE *)pRow,i,smStor);	
			lp=pRow;

			for (j=0;j<wid;j++)
			{
				*lpR=*lp; lpR++; lp++;
				*lpG=*lp; lpG++; lp++;
				*lpB=*lp; lpB++; lp++;
			}
		}

		Histogram16Bit(pDataR,wid,hei,pHisto);
		ChannelByTemplate16Bit(pDataR,wid,hei,pHisto,iSize);

		Histogram16Bit(pDataG,wid,hei,pHisto);
		ChannelByTemplate16Bit(pDataG,wid,hei,pHisto,iSize);

		Histogram16Bit(pDataB,wid,hei,pHisto);
		ChannelByTemplate16Bit(pDataB,wid,hei,pHisto,iSize);

		lpR=pDataR;
		lpG=pDataG;
		lpB=pDataB;	
		
		for (i=0;i<hei;i++)
		{
			lp=pRow;
			for (j=0;j<wid;j++)
			{
				*lp=*lpR; lpR++; lp++;
				*lp=*lpG; lpG++; lp++;
				*lp=*lpB; lpB++; lp++;
			}
			DPGimgRes.WriteEx((BYTE *)pRow,i,smStor);
		}

		delete pDataR;
		delete pDataG;
		delete pDataB;
		delete pRow;
	}

	DPGimg.Close();
	DPGimgRes.Close();
	return true;
}

void CAdjustLightColor::ChannelByTemplate16Bit(WORD *pData,int wid,int hei,long pHisto[65536],int iSize)
{
	int i,j,l;
	float fMean=0.0f;
	for(i=0;i<65536;i++) fMean+=float(pHisto[i])/hei/wid*i;

	WORD *lpPos=NULL;
	WORD *lpData=pData;
	int *pTemplate=new int[(hei/iSize+1)*(wid/iSize+1)];
	memset(pTemplate,0,sizeof(int)*(hei/iSize+1)*(wid/iSize+1));

	int sum=0,num=0;
	int blkh=hei/iSize;
	int blkw=wid/iSize;
	int iBeginx,iEndx,iBeginy,iEndy,iMin,m,n;	

	for(i=0;i<=blkh;i++)
	{
		iBeginy=i*iSize;
		if(iBeginy>=hei) continue;
		iEndy=min((i+1)*iSize,hei);
		
		for(j=0;j<=blkw;j++)
		{
			iBeginx=j*iSize;
			if(iBeginx>=wid) continue;
			iEndx=min((j+1)*iSize,wid);

			iMin=*(lpData+(hei-1-iBeginy)*wid+iBeginx);
			sum=0;num=0;
			for(m=iBeginx;m<iEndx;m++)
			{
				for(n=iBeginy;n<iEndy;n++)
				{
					lpPos=lpData+(hei-1-n)*wid+m;
					sum+=*lpPos;
					num++;
				}
			}
			iMin=int(float(sum)/num+0.5);
			pTemplate[(blkw+1)*(blkh-i)+j]=iMin;
		}
	}

	LuminanceTemplateFit(pTemplate,blkw+1,blkh+1,0,65535);

	for(i=0;i<hei;i++)
	{
		for(j=0;j<wid;j++)
		{
			lpPos=lpData+(hei-1-i)*wid+j;
			if(*lpPos==0) continue;
			l=*lpPos;
			iMin=pTemplate[(blkw+1)*(blkh-i/iSize)+j/iSize];
			*lpPos=max(0,min(65535,int(l-iMin+fMean+0.5)));
		}
	}
	delete pTemplate;	
}

long CAdjustLightColor::Histogram16Bit(WORD *pData,int wid,int hei,long *pHisto)
{
	int i,j,gray;
	long nSize=hei*wid;
	WORD *lpData=pData;
	WORD *lpPos=NULL;
	if(pHisto!=NULL) memset(pHisto,0x00,sizeof(long)*65536);

	for(i=0;i<hei;i++)
	{
		for(j=0;j<wid;j++)
		{
			lpPos=lpData+i*wid+j;
			gray=*(lpPos);
			if (gray==0) 
			{
				nSize--;
				continue;
			}
			pHisto[gray]++;
		}
	}
	return nSize;
}

void CAdjustLightColor::SimpleTemplate(CxImage *pImg,int iSize,RGBQUAD *pBackGrd,int nBackGrd)
{
	long pHisto[256],pHistor[256],pHistog[256],pHistob[256],nUsefulSize;
	int bpp=pImg->GetBpp();

	if(bpp==8) 
	{
		nUsefulSize=Histogram(pImg,NULL,NULL,NULL,pHisto,pBackGrd,nBackGrd);
		ChannelByTemplate(pImg,pHisto,iSize,pBackGrd,nBackGrd);
	}
	else if(bpp==24) 
	{
		nUsefulSize=Histogram(pImg,pHistor,pHistog,pHistob,NULL,pBackGrd,nBackGrd);

		CxImage ImgR;
		CxImage ImgG;
		CxImage ImgB;

		pImg->SplitRGB(&ImgR,&ImgG,&ImgB);
		ChannelByTemplate(&ImgR, pHistor,iSize,pBackGrd,nBackGrd);
		ChannelByTemplate(&ImgG, pHistog,iSize,pBackGrd,nBackGrd);
		ChannelByTemplate(&ImgB, pHistob,iSize,pBackGrd,nBackGrd);
		pImg->Combine(&ImgR,&ImgG,&ImgB,NULL);

		ImgR.Destroy();
		ImgG.Destroy();
		ImgB.Destroy();
	}
}

void CAdjustLightColor::ChannelByTemplate(CxImage *pImg,long pHisto[256],int iSize,RGBQUAD *pBackGrd,int nBackGrd)
{
	int j,l;
	int nHei=pImg->GetHeight();
	int nWid=pImg->GetWidth();

	int iMean=128;
	float fMean=0.0f;
	for(int i=0;i<256;i++) fMean+=float(pHisto[i])/nHei/nWid*i;

	int iBit=pImg->GetBpp();
	int nScanWid=pImg->GetEffWidth();
	int iBeginx,iEndx,iBeginy,iEndy,iMin,m,n;
	BYTE* lpBits=pImg->GetBits();
	BYTE* lpPos;
	int *pTemplate=new int[(nHei/iSize+1)*(nWid/iSize+1)];
	memset(pTemplate,0,sizeof(int)*(nHei/iSize+1)*(nWid/iSize+1));
	
	int sum=0,num=0;
	int blkh=nHei/iSize;
	int blkw=nWid/iSize;
	for(i=0;i<=blkh;i++)
	{
		iBeginy=i*iSize;
		if(iBeginy>=nHei) continue;
		iEndy=min((i+1)*iSize,nHei);
		
		for(j=0;j<=blkw;j++)
		{
			iBeginx=j*iSize;
			if(iBeginx>=nWid) continue;
			iEndx=min((j+1)*iSize,nWid);

			iMin=*(lpBits+(nHei-1-iBeginy)*nScanWid+iBeginx);
			sum=0;num=0;
			for(m=iBeginx;m<iEndx;m++)
			{
				for(n=iBeginy;n<iEndy;n++)
				{
					lpPos=lpBits+(nHei-1-n)*nScanWid+m;
					sum+=*lpPos;
					num++;
				}
			}
			iMin=int(float(sum)/num+0.5);
			pTemplate[(blkw+1)*(blkh-i)+j]=iMin;
		}
	}

	LuminanceTemplateFit(pTemplate,blkw+1,blkh+1);

	for(i=0;i<nHei;i++)
	{
		for(j=0;j<nWid;j++)
		{
			lpPos=lpBits+(nHei-1-i)*nScanWid+j*(iBit/8);
			if(BeBackground(lpPos,iBit,pBackGrd,nBackGrd)) continue;
			l=*lpPos;
			iMin=pTemplate[(blkw+1)*(blkh-i/iSize)+j/iSize];
			*lpPos=max(0,min(255,int(l-iMin+fMean+0.5)));
		}
	}
	delete pTemplate;
}

void CAdjustLightColor::LuminanceTemplateFit(int *pTemplate, int iWid, int iHei,int mingray,int maxgray)
{
	double dA[6][6]={0.0};
	double db[6]={0.0};
	double pdResult[6]={0.0};

	float fx,fy,fz;
	double pow2x,pow2y,pow3x,pow3y;

	int i,j;
	int iN=0;
	for (i=0;i< iWid;i++)
	{		
		for(j=0; j<iHei; j++)
		{
			if(pTemplate[j*iWid+i]==0) continue;
			fx=float(i);
			fy=float(j);
			fz=float(pTemplate[j*iWid+i]);
			pow2x=pow(fx,2.0);
			pow2y=pow(fy,2.0);
			pow3x=pow(fx,3.0);
			pow3y=pow(fy,3.0);
			dA[0][1]+=fx;
			dA[0][2]+=fy;
			dA[0][3]+=fx*fy;
			dA[0][4]+=pow2x;
			dA[0][5]+=pow2y;
			dA[1][3]+=pow2x*fy;
			dA[1][4]+=pow3x;
			dA[1][5]+=pow2y*fx;
			dA[2][5]+=pow3y;
			dA[3][3]+=pow2x*pow2y;
			dA[3][4]+=pow3x*fy;
			dA[3][5]+=pow3y*fx;
			dA[4][4]+=pow(fx,4.0);
			dA[5][5]+=pow(fy,4.0);
			db[0]+=fz;
			db[1]+=fx*fz;
			db[2]+=fy*fz;
			db[3]+=fx*fy*fz;
			db[4]+=pow2x*fz;
			db[5]+=pow2y*fz;

			iN++;
		}
	}
	dA[0][0]=iN;
	dA[1][0]=dA[0][1];dA[1][1]=dA[0][4];dA[1][2]=dA[0][3];
	dA[2][0]=dA[0][2];dA[2][1]=dA[1][2];dA[2][2]=dA[0][5];dA[2][3]=dA[1][5];dA[2][4]=dA[1][3];
	dA[3][0]=dA[0][3];dA[3][1]=dA[1][3];dA[3][2]=dA[2][3];
	dA[4][0]=dA[0][4];dA[4][1]=dA[1][4];dA[4][2]=dA[2][4];dA[4][3]=dA[3][4];dA[4][5]=dA[3][3];
	dA[5][0]=dA[0][5];dA[5][1]=dA[1][5];dA[5][2]=dA[2][5];dA[5][3]=dA[3][5];dA[5][4]=dA[4][5];

	double pdA[36]={0.0};
	double pdb[6]={0.0};
	for(i=0;i<6;i++)
	{
		for(j=0;j<6;j++)
		{
			pdA[i*6+j]=dA[i][j];
		}
		pdb[i]=db[i];
	}
	if(!GauceFunction(6,pdA,pdb,pdResult)) return;

	int iV;
	BYTE *pV=new BYTE[iHei*iWid];
	for (i=0;i< iWid;i++)
	{
		for(j=0; j<iHei; j++)
		{
			fx=float(i);
			fy=float(j);
			fz=float(pdResult[0]+pdResult[1]*fx+pdResult[2]*fy+pdResult[3]*fx*fy+pdResult[4]*pow(fx,2.0)+pdResult[5]*pow(fy,2.0));
			pTemplate[j*iWid+i]=int(fz+0.5);

			iV=max(mingray,min(fz,maxgray));
			pV[j*iWid+i]=iV;
		}
	}
	delete pV;
}

BOOL CAdjustLightColor::GauceFunction(int n, double *pdA, double *pdb, double *pdResult, double delta)
{
	int i,j,ik,jk,k=0;
	int *iZ=new int[n];
	int iChange;
	double dChange;
	double dMain;

	for(i=0;i<n;i++)
		iZ[i]=i;
	for(k=0;k<n-1;k++)
	{
		dMain=GetMainElement(n,k,pdA,&ik,&jk);
		if(ik!=k)
		{
			for(i=0;i<n;i++)
			{
				dChange=pdA[ik*n+i];
				pdA[ik*n+i]=pdA[k*n+i];
				pdA[k*n+i]=dChange;
			}
			dChange=pdb[ik];
			pdb[ik]=pdb[k];
			pdb[k]=dChange;
		}
		if(jk!=k)
		{
			for(i=0;i<n;i++)
			{
				dChange=pdA[i*n+jk];
				pdA[i*n+jk]=pdA[i*n+k];
				pdA[i*n+k]=dChange;
			}

			iChange=iZ[jk];
			iZ[jk]=iZ[k];
			iZ[k]=iChange;
		}

		double dm;
		for(i=k+1;i<n;i++)
		{
			dm=pdA[n*i+k]/pdA[n*k+k];
			for(j=0;j<n;j++) pdA[n*i+j]=pdA[n*i+j]-dm*pdA[k*n+j];
			pdb[i]=pdb[i]-dm*pdb[k];
		}
	}

	double temp=0.0;
	if(fabs(pdA[n*n-1])<=delta) return false;
	
	pdResult[n-1]=pdb[n-1]/pdA[n*n-1];
	for(i=n-2;i>=0;i--)
	{
		temp=0.0;
		for(j=i+1;j<n;j++) temp+=pdA[i*n+j]*pdResult[j];
		pdResult[i]=(pdb[i]-temp)/pdA[i*n+i];
	}

	for(i=0;i<n;i++) pdb[i]=pdResult[i];
	for(i=0;i<n;i++) pdResult[iZ[i]]=pdb[i];
	delete iZ;

	return true;
}

double CAdjustLightColor::GetMainElement(int n, int k, double *pdA, int *ik, int *jk)
{
	double dMain=0.0;
	int i,j;
	for(i=k;i<n;i++)
		for(j=k;j<n;j++)
		{
			if(fabs(pdA[i*n+j])>dMain)
			{
				dMain=pdA[i*n+j];
				*ik=i;
				*jk=j;
			}
		}
		
		return dMain;
}

bool CAdjustLightColor::ImageFilter(CxImage *pImg,float size,int nType,int nDimension)
{
	if(!pImg) return false;
	int wid=pImg->GetWidth();
	int hei=pImg->GetHeight();

	long i=0;
	while((1<<i)<wid) i++;
	long w=1<<i;

	i=0;
	while((1<<i)<hei) i++;
	long h=1<<i;

	int iBpp=0;
	float fvars[4]={0.0};
	float fmeans[4]={0.0};
	GetColorStatistic(pImg,iBpp,fvars,fmeans);
	RGBQUAD back;
	if (iBpp==24) 
	{
		back.rgbRed=fmeans[0];
		back.rgbGreen=fmeans[1];
		back.rgbBlue=fmeans[2];
		back.rgbReserved=0;	
	}
	if (iBpp==8) 
	{
		back.rgbRed=back.rgbGreen=back.rgbBlue=fmeans[3];
		back.rgbReserved=0;	
	}
	if (w!=wid||h!=hei) 
	{
		pImg->Expand(0,0,w-wid,h-hei,back);
	}

	if(nDimension==0) 
	{
		if (!CreatMemSpace(w,h,nType,size)) 
		{
			AfxMessageBox("Alloc Memory Fail!");
			return false;
		}
		if(iBpp==24)
			FilterColor(pImg,fmeans);
		else if(iBpp==8)
			FilterGray(pImg,fmeans[3]);
		else return false;
	}
	else if (nDimension==1)
	{
		if(iBpp==24)
			FilterBigColor(pImg,size,nType,fmeans);
		else if(iBpp==8)
			FilterBigGray(pImg,size,nType,fmeans[3]);
		else return false;
	}

	if (w!=wid||h!=hei) 
		pImg->Crop(0,0,wid,hei);

	return true;
}

void CAdjustLightColor::FilterGray(CxImage *img,float mean)
{
	BYTE *lpIn=img->GetBits();

	FFTFilterImage(lpIn,mean);
}

void CAdjustLightColor::FilterColor(CxImage *img,float nMeans[3])
{
	CxImage ImgR;
	CxImage ImgG;
	CxImage ImgB;

	img->SplitRGB(&ImgR,&ImgG,&ImgB);
	img->Destroy();

	FilterGray(&ImgR,nMeans[0]);
	FilterGray(&ImgG,nMeans[1]);
	FilterGray(&ImgB,nMeans[2]);

	img->Combine(&ImgR,&ImgG,&ImgB,NULL);

	ImgR.Destroy();
	ImgG.Destroy();
	ImgB.Destroy();
}

void CAdjustLightColor::FilterBigGray(CxImage *img,float d0,int filtertype,float mean)
{
	int w=img->GetWidth();
	int h=img->GetHeight();
	BYTE *lpIn=img->GetBits();

	long psize=img->GetEffWidth()*img->GetHeight();
	for (long i=0;i<psize;i++)
	{
		if( *lpIn==0 || *lpIn==255 ) *lpIn = max(0,min(255,mean));
		lpIn++;
	}

	lpIn=img->GetBits();
	FFTFilterBigImage(lpIn,w,h,d0,filtertype,mean);
}

void CAdjustLightColor::FilterBigColor(CxImage *img,float d0,int filtertype,float nMeans[3])
{
	CxImage ImgR;
	CxImage ImgG;
	CxImage ImgB;

	img->SplitRGB(&ImgR,&ImgG,&ImgB);
	img->Destroy();

	FilterBigGray(&ImgR,d0,filtertype,nMeans[0]);
	FilterBigGray(&ImgG,d0,filtertype,nMeans[1]);
	FilterBigGray(&ImgB,d0,filtertype,nMeans[2]);

	img->Combine(&ImgR,&ImgG,&ImgB,NULL);

	ImgR.Destroy();
	ImgG.Destroy();
	ImgB.Destroy();	
}

bool CAdjustLightColor::SubDodging(CxImage *pImg,float size)
{
	if(!pImg) return false;

	int iBpp=pImg->GetBpp();
	int wid=pImg->GetWidth();
	int hei=pImg->GetHeight();

	long i=0;
	while((1<<i)<wid) i++;
	long w=1<<i;

	i=0;
	while((1<<i)<hei) i++;
	long h=1<<i;

	if(w>4096) w=4096;
	if(h>4096) h=4096;

	CxImage SubImg;
	if (w!=wid||h!=hei) 
	{
		if (!pImg->Resample(w,h,0,&SubImg)) return FALSE;		
	}
	else SubImg=*pImg;

	int bpp=0;
	float fVars[4]={0.0};
	float fMeans[4]={0.0};
	GetColorStatistic(&SubImg,bpp,fVars,fMeans);
	RGBQUAD back;
	if (bpp==24) 
	{
		back.rgbRed=fMeans[0];
		back.rgbGreen=fMeans[1];
		back.rgbBlue=fMeans[2];
		back.rgbReserved=0;	
	}
	if (bpp==8) 
	{
		back.rgbRed=back.rgbGreen=back.rgbBlue=fMeans[3];
		back.rgbReserved=0;	
	}

	int iBPP=SubImg.GetBpp();
	if (iBPP==24) FilterBigColor(&SubImg,size,5,fMeans);
	else if (iBPP==8) FilterBigGray(&SubImg,size,5,fMeans[3]);
	else return FALSE;

	if (w!=wid||h!=hei) 
	{
		if (!SubImg.Resample(wid,hei,0)) return FALSE;
	}

	int r=0,c=0;
	BYTE *pSub=NULL,*pSrc=NULL;
	if (iBpp==8) 
	{
		for (r=0;r<hei;r++)
		{
			pSub=SubImg.GetBits(r);
			pSrc=pImg->GetBits(r);

			for (c=0;c<wid;c++)
			{
				*pSrc=max(0,min(255,(*pSrc)+fMeans[3]-(*pSub)));

				pSub++;
				pSrc++;
			}
		}
	}
	else
	{
		for (r=0;r<hei;r++)
		{
			pSub=SubImg.GetBits(r);
			pSrc=pImg->GetBits(r);

			for (c=0;c<wid;c++)
			{
				*pSrc=max(0,min(255,(*pSrc)+fMeans[2]-(*pSub)));
				pSub++;
				pSrc++;

				*pSrc=max(0,min(255,(*pSrc)+fMeans[1]-(*pSub)));
				pSub++;
				pSrc++;

				*pSrc=max(0,min(255,(*pSrc)+fMeans[0]-(*pSub)));
				pSub++;
				pSrc++;
			}
		}
	}
	SubImg.Destroy();

	return true;
}

bool CAdjustLightColor::DivideDodging(CxImage *pImg,float size)
{
	if(!pImg) return false;

	int wid=pImg->GetWidth();
	int hei=pImg->GetHeight();

	long i=0;
	while((1<<i)<wid) i++;
	long w=1<<i;

	i=0;
	while((1<<i)<hei) i++;
	long h=1<<i;

	if(w>4096) w=4096;
	if(h>4096) h=4096;

	CxImage SubImg;
	if (w!=wid||h!=hei) 
	{
		if (!pImg->Resample(w,h,0,&SubImg)) return FALSE;		
	}
	else SubImg=*pImg;

	int bpp=0;
	float fVars[4]={0.0};
	float fMeans[4]={0.0};
	GetColorStatistic(&SubImg,bpp,fVars,fMeans);
	RGBQUAD back;
	if (bpp==24) 
	{
		back.rgbRed=fMeans[0];
		back.rgbGreen=fMeans[1];
		back.rgbBlue=fMeans[2];
		back.rgbReserved=0;	
	}
	if (bpp==8) 
	{
		back.rgbRed=back.rgbGreen=back.rgbBlue=fMeans[3];
		back.rgbReserved=0;	
	}

	int iBPP=SubImg.GetBpp();
	if (iBPP==24) FilterBigColor(&SubImg,size,5,fMeans);
	else if (iBPP==8) FilterBigGray(&SubImg,size,5,fMeans[3]);
	else return FALSE;

	if (w!=wid||h!=hei) 
	{
		if (!SubImg.Resample(wid,hei,0)) return FALSE;
	}

	int r=0,c=0;
	BYTE *pSub=NULL,*pSrc=NULL;
	if (iBPP==8) 
	{
		for (r=0;r<hei;r++)
		{
			pSub=SubImg.GetBits(r);
			pSrc=pImg->GetBits(r);

			for (c=0;c<wid;c++)
			{
				*pSrc=max(0,min(255,*pSrc*fMeans[3]/(*pSub)));

				pSub++;
				pSrc++;
			}
		}
	}
	else
	{
		for (r=0;r<hei;r++)
		{
			pSub=SubImg.GetBits(r);
			pSrc=pImg->GetBits(r);

			for (c=0;c<wid;c++)
			{
				*pSrc=max(0,min(255,*pSrc*fMeans[2]/(*pSub)));
				pSub++;
				pSrc++;

				*pSrc=max(0,min(255,*pSrc*fMeans[1]/(*pSub)));
				pSub++;
				pSrc++;

				*pSrc=max(0,min(255,*pSrc*fMeans[0]/(*pSub)));
				pSub++;
				pSrc++;
			}
		}
	}
	SubImg.Destroy();

	return true;
}

bool CAdjustLightColor::DivideDodging16BitGray(WORD *pData,int wid,int hei,float size)
{
	long i=0;
	while((1<<i)<wid) i++;
	long w=1<<i;

	i=0;
	while((1<<i)<hei) i++;
	long h=1<<i;

	if(w>4096) w=4096;
	if(h>4096) h=4096;
	if(!CreatMemSpace(w,h,5,size))
	{
		AfxMessageBox("Alloc Memory Fail!");
		return false;
	}

	WORD *pSub=new WORD[w*h];
	memcpy(pSub,pData,w*h*sizeof(WORD));
	if (w!=wid||h!=hei) Resample16Bit(pData,wid,hei,pSub,w,h);
	FFTFilterImage16Bit(pSub);

	float fVars=0.0,fMeans=0.0;
	GetColorStatistic16Bit(pSub,w,h,fVars,fMeans);

	WORD *pSubRes=new WORD[wid*hei];
	memset(pSubRes,0,wid*hei*sizeof(WORD));
	if (w!=wid||h!=hei) Resample16Bit(pSub,w,h,pSubRes,wid,hei);
	else memcpy(pSubRes,pSub,wid*hei*sizeof(WORD));

	WORD *pS=pSubRes,*pD=pData;
	long lsize=wid*hei;
	for (i=0;i<lsize;i++)
	{
		*pD=max(0,min(65535,*pD*fMeans/(*pS)));
		pD++;	pS++;			
	}

	delete pSub;
	delete pSubRes;

	return true;
}

bool CAdjustLightColor::SubDodging16BitGray(WORD *pData,int wid,int hei,float size)
{
	long i=0;
	while((1<<i)<wid) i++;
	long w=1<<i;

	i=0;
	while((1<<i)<hei) i++;
	long h=1<<i;

	if(w>4096) w=4096;
	if(h>4096) h=4096;
	if(!CreatMemSpace(w,h,5,size))
	{
		AfxMessageBox("Alloc Memory Fail!");
		return false;
	}

	WORD *pSub=new WORD[w*h];
	memcpy(pSub,pData,w*h*sizeof(WORD));
	if (w!=wid||h!=hei) Resample16Bit(pData,wid,hei,pSub,w,h);
	FFTFilterImage16Bit(pSub);

	float fVars=0.0,fMeans=0.0;
	GetColorStatistic16Bit(pSub,w,h,fVars,fMeans);

	WORD *pSubRes=new WORD[wid*hei];
	memset(pSubRes,0,wid*hei*sizeof(WORD));
	if (w!=wid||h!=hei) Resample16Bit(pSub,w,h,pSubRes,wid,hei);
	else memcpy(pSubRes,pSub,wid*hei*sizeof(WORD));

	WORD *pS=pSubRes,*pD=pData;
	long lsize=wid*hei;
	for (i=0;i<lsize;i++)
	{
		*pD=max(0,min(65535,*pD+fMeans-*pS));
		pD++;	pS++;			
	}

	delete pSub;
	delete pSubRes;

	return true;
}

bool CAdjustLightColor::DivideDodging16Bit(CString strDir,CString strResDir,float size,BOOL bCompare)
{
	CStDPGImage DPGimg;
	if (!DPGimg.Open(strDir)) return false;

	int bytepp=DPGimg.GetPixelSamples();
	if(DPGimg.GetSampleBytes()!=2||(bytepp!=1&&bytepp!=3))
	{
		DPGimg.Close();
		return false;
	}

	CStDPGImage DPGimgRes;
	if (!DPGimgRes.Open(strResDir,CSpVZImage::modeCreate)) 
	{
		DPGimg.Close();
		return false;
	}

	int wid=DPGimg.GetCols();
	int hei=DPGimg.GetRows();
	CStDPGImage::SAMP_STOR smStor=DPGimg.GetSampleStorage();

	DPGimgRes.SetRows(hei);
	DPGimgRes.SetCols(wid);
	DPGimgRes.SetSampleBytes(2);
	DPGimgRes.SetPixelSamples(bytepp);
	DPGimgRes.SetSampleStorage(smStor);
	if (bCompare) DPGimgRes.SetCompress(CStDPGImage::CT_JPG);

	if (bytepp==1)
	{
		WORD *pData=new WORD[wid*hei];
		memset(pData,0,wid*hei*sizeof(WORD));
		DPGimg.ReadEx((BYTE *)pData,0,0,hei,wid,smStor);

		DivideDodging16BitGray(pData,wid,hei,size);
		DPGimgRes.WriteEx((BYTE *)pData,0,0,hei,wid,smStor);

		delete pData;
	}
	else 
	{
		WORD *pDataR=new WORD[wid*hei];
		memset(pDataR,0,wid*hei*sizeof(WORD));

		WORD *pDataG=new WORD[wid*hei];
		memset(pDataG,0,wid*hei*sizeof(WORD));

		WORD *pDataB=new WORD[wid*hei];
		memset(pDataB,0,wid*hei*sizeof(WORD));

		int i=0,j=0;
		WORD *pRow=new WORD[wid*3];
		WORD *lp,*lpR=pDataR,*lpG=pDataG,*lpB=pDataB;	

		for (i=0;i<hei;i++)
		{
			memset(pRow,0,wid*sizeof(WORD));
			DPGimg.ReadEx((BYTE *)pRow,i,smStor);	
			lp=pRow;

			for (j=0;j<wid;j++)
			{
				*lpR=*lp; lpR++; lp++;
				*lpG=*lp; lpG++; lp++;
				*lpB=*lp; lpB++; lp++;
			}
		}

		DivideDodging16BitGray(pDataR,wid,hei,size);
		DivideDodging16BitGray(pDataG,wid,hei,size);
		DivideDodging16BitGray(pDataB,wid,hei,size);

		lpR=pDataR;
		lpG=pDataG;
		lpB=pDataB;

		for (i=0;i<hei;i++)
		{
			lp=pRow;
			for (j=0;j<wid;j++)
			{
				*lp=*lpR; lpR++; lp++;
				*lp=*lpG; lpG++; lp++;
				*lp=*lpB; lpB++; lp++;
			}
			DPGimgRes.WriteEx((BYTE *)pRow,i,smStor);
		}

		delete pDataR;
		delete pDataG;
		delete pDataB;
		delete pRow;
	}

	DPGimg.Close();
	DPGimgRes.Close();
	return true;
}

bool CAdjustLightColor::SubDodging16Bit(CString strDir,CString strResDir,float size,BOOL bCompare)
{
	CStDPGImage DPGimg;
	if (!DPGimg.Open(strDir)) return false;

	int bytepp=DPGimg.GetPixelSamples();
	if(DPGimg.GetSampleBytes()!=2||(bytepp!=1&&bytepp!=3))
	{
		DPGimg.Close();
		return false;
	}

	CStDPGImage DPGimgRes;
	if (!DPGimgRes.Open(strResDir,CSpVZImage::modeCreate)) 
	{
		DPGimg.Close();
		return false;
	}

	int wid=DPGimg.GetCols();
	int hei=DPGimg.GetRows();
	CStDPGImage::SAMP_STOR smStor=DPGimg.GetSampleStorage();

	DPGimgRes.SetRows(hei);
	DPGimgRes.SetCols(wid);
	DPGimgRes.SetSampleBytes(2);
	DPGimgRes.SetPixelSamples(bytepp);
	DPGimgRes.SetSampleStorage(smStor);
	if (bCompare) DPGimgRes.SetCompress(CStDPGImage::CT_JPG);

	if (bytepp==1)
	{
		WORD *pData=new WORD[wid*hei];
		memset(pData,0,wid*hei*sizeof(WORD));
		DPGimg.ReadEx((BYTE *)pData,0,0,hei,wid,smStor);

		SubDodging16BitGray(pData,wid,hei,size);
		DPGimgRes.WriteEx((BYTE *)pData,0,0,hei,wid,smStor);

		delete pData;
	}
	else 
	{
		WORD *pDataR=new WORD[wid*hei];
		memset(pDataR,0,wid*hei*sizeof(WORD));

		WORD *pDataG=new WORD[wid*hei];
		memset(pDataG,0,wid*hei*sizeof(WORD));

		WORD *pDataB=new WORD[wid*hei];
		memset(pDataB,0,wid*hei*sizeof(WORD));

		int i=0,j=0;
		WORD *pRow=new WORD[wid*3];
		WORD *lp,*lpR=pDataR,*lpG=pDataG,*lpB=pDataB;	

		for (i=0;i<hei;i++)
		{
			memset(pRow,0,wid*sizeof(WORD));
			DPGimg.ReadEx((BYTE *)pRow,i,smStor);	
			lp=pRow;

			for (j=0;j<wid;j++)
			{
				*lpR=*lp; lpR++; lp++;
				*lpG=*lp; lpG++; lp++;
				*lpB=*lp; lpB++; lp++;
			}
		}

		SubDodging16BitGray(pDataR,wid,hei,size);
		SubDodging16BitGray(pDataG,wid,hei,size);
		SubDodging16BitGray(pDataB,wid,hei,size);

		lpR=pDataR;
		lpG=pDataG;
		lpB=pDataB;

		for (i=0;i<hei;i++)
		{
			lp=pRow;
			for (j=0;j<wid;j++)
			{
				*lp=*lpR; lpR++; lp++;
				*lp=*lpG; lpG++; lp++;
				*lp=*lpB; lpB++; lp++;
			}
			DPGimgRes.WriteEx((BYTE *)pRow,i,smStor);
		}

		delete pDataR;
		delete pDataG;
		delete pDataB;
		delete pRow;
	}

	DPGimg.Close();
	DPGimgRes.Close();
	return true;
}

void CAdjustLightColor::Resample16Bit(WORD *pData,int wid,int hei,WORD *pDis,int w,int h)
{
	double ratw=double(wid-1)/double(w-1);
	double rath=double(hei-1)/double(h-1);

	int x=0,y=0;
	double dx=0.0,dy=0.0;
	WORD *lpdata=NULL,*lpDis=pDis;

	for (int i=0;i<h;i++)
	{
		y=dy=i*rath;
		dy-=y;

		for (int j=0;j<w;j++)
		{
			x=dx=j*ratw;
			dx-=x;
			lpdata=pData+y*wid+x;

			if (y>=(hei-1)||x>=(wid-1)) 
			{
				*lpDis=*lpdata;
				lpDis++;
				continue;
			}

			*lpDis=*lpdata*(1-dx)*(1-dy)+0.5;
			*lpDis+=*(lpdata+1)*dx*(1-dy)+0.5;
			lpdata+=wid;
			*lpDis+=*lpdata*(1-dx)*dy+0.5;
			*lpDis+=*(lpdata+1)*dx*dy+0.5;

			lpDis++;
		}
	}
}

void CAdjustLightColor::HomomorphicGray(CxImage *img)
{
	int w=img->GetEffWidth();
	int h=img->GetHeight();
	BYTE *lpIn=img->GetBits();

	Homomorphic(lpIn);
}

void CAdjustLightColor::HomomorphicColor(CxImage *img)
{
	CxImage ImgR;
	CxImage ImgG;
	CxImage ImgB;

	img->SplitRGB(&ImgR,&ImgG,&ImgB);
	img->Destroy();

	HomomorphicGray(&ImgR);
	HomomorphicGray(&ImgG);
	HomomorphicGray(&ImgB);

	img->Combine(&ImgR,&ImgG,&ImgB,NULL);

	ImgR.Destroy();
	ImgG.Destroy();
	ImgB.Destroy();
}

bool CAdjustLightColor::HomomorphicFilter(CxImage *pImg,float size,float rl,float rh)
{
	if(!pImg) return false;
	int iBpp=pImg->GetBpp();
	int wid=pImg->GetWidth();
	int hei=pImg->GetHeight();

	long i=0;
	while((1<<i)<wid) i++;
	long w=1<<i;

	i=0;
	while((1<<i)<hei) i++;
	long h=1<<i;

	if (w!=wid||h!=hei) 
	{
		RGBQUAD back;
		memset(&back,127,sizeof(RGBQUAD));
		pImg->Expand(0,0,w-wid,h-hei,back);
	}

	if (!CreatMemSpace(w,h,6,size,rl,rh)) 
	{
		AfxMessageBox("Alloc Memory Fail!");
		return false;
	}

	if(iBpp==24)
		HomomorphicColor(pImg);
	else if(iBpp==8)
		HomomorphicGray(pImg);
	else return false;

	if (w!=wid||h!=hei) 
		pImg->Crop(0,0,wid,hei);

	return true;
}

BOOL CAdjustLightColor::CreatMemSpace(int xsize,int ysize,int ftype,float d0,float rl,float rh)
{
	if (nWsizex==xsize&&nWsizey==ysize&&
		nFtype==ftype&&nD0==d0&&nRl==rl&&nRh==rh&&
		nAr!=NULL&&nAi!=NULL) return true;

	if (xsize<=0||ysize<=0) return false;

	if (nWsizex!=xsize||nWsizey!=ysize||nAr==NULL)
	{
		if (nAr!=NULL) delete nAr;//VirtualFree(nAr,0,MEM_DECOMMIT);
		nAr=NULL;
		nAr=new float[xsize*ysize];//(float *)VirtualAlloc(NULL,xsize*ysize*sizeof(float),MEM_COMMIT,PAGE_READWRITE);
		if (nAr==NULL) return false;
	}

	if (nWsizex!=xsize||nWsizey!=ysize||nAr==NULL)
	{
		if (nAi!=NULL) delete nAi;//VirtualFree(nAi,0,MEM_DECOMMIT); 
		nAi=NULL;
		nAi=new float[xsize*ysize];//(float *)VirtualAlloc(NULL,xsize*ysize*sizeof(float),MEM_COMMIT,PAGE_READWRITE);
		if (nAi==NULL) return false;
	}

	nWsizex=xsize;
	nWsizey=ysize;
	nFtype=ftype;
	nD0=d0;
	nRl=rl;
	nRh=rh;

	return true;
}

int CAdjustLightColor::FFTFilterImage(BYTE *image,float mean)
{
	if (nAr==NULL||nAi==NULL)	return -1;

	long i,xysize=nWsizex*nWsizey;

	BYTE *lpIn,*lpOut;
	float *lpAr;

	lpIn=image;
	lpAr=nAr;
	for(i=0;i<xysize;i++)
	{
		*lpAr=(float)(*lpIn)-mean;

		lpIn++;
		lpAr++;
	}

	if (FFT2(nAr,nAi,1,nWsizex,nWsizey)==-1) return -1;

	FrequencyFilter2(nAr,nAi,nWsizex,nWsizey,nFtype,nD0,nRl,nRh);

 	if (FFT2(nAr,nAi,-1,nWsizex,nWsizey)==-1) return -1;

	lpAr=nAr;
	lpOut=image;
	for(i=0;i<xysize;i++)
	{
		*lpOut=max(0,min(mean+*lpAr,255));

		lpAr++;
		lpOut++;
	}

	return 0;
}

int CAdjustLightColor::FFTFilterImage16Bit(WORD *image)
{
	if (nAr==NULL||nAi==NULL)	return -1;

	long i,xysize=nWsizex*nWsizey;

	WORD *lpIn,*lpOut;
	float *lpAr;

	lpIn=image;
	lpAr=nAr;
	for(i=0;i<xysize;i++)
	{
		*lpAr=(float)(*lpIn);

		lpIn++;
		lpAr++;
	}

	if (FFT2(nAr,nAi,1,nWsizex,nWsizey)==-1) return -1;

	FrequencyFilter2(nAr,nAi,nWsizex,nWsizey,nFtype,nD0,nRl,nRh);

 	if (FFT2(nAr,nAi,-1,nWsizex,nWsizey)==-1) return -1;

	lpAr=nAr;
	lpOut=image;
	for(i=0;i<xysize;i++)
	{
		*lpOut=max(0,min(*lpAr,65535));

		lpAr++;
		lpOut++;
	}

	return 0;
}

int CAdjustLightColor::Homomorphic(BYTE *image)
{
	if (nAr==NULL||nAi==NULL)	return -1;

	long xysize=nWsizex*nWsizey;

	memset(nAr,0,xysize*sizeof(float));
	memset(nAi,0,xysize*sizeof(float));

	ln(image,nAr,nWsizex,nWsizey);

	if (FFT2(nAr,nAi,1,nWsizex,nWsizey)==-1) return -1;

	FrequencyFilter2(nAr,nAi,nWsizex,nWsizey,nFtype,nD0,nRl,nRh);

	if (FFT2(nAr,nAi,-1,nWsizex,nWsizey)==-1) return -1;

	ex(nAr,image,nWsizex,nWsizey);

	return 0;
}

bool CAdjustLightColor::ADS40RadiateEliminate(CString strDir,BOOL bManMade,BOOL bStainMeans,CString strSaveDir)
{
	CSpVZImage imgOral;	//影像文件变量

	if (!imgOral.Open(strDir)) 
	{
		AfxMessageBox("Open ADS40 File Fail!");
		return FALSE;
	}

	if (imgOral.GetPixelBytes()!=1) 
	{
		AfxMessageBox("Can't Process Colour Image!");
		return FALSE;
	}

	int nSize=imgOral.GetRows(); //影像行数

	float *pMeans=new float[nSize];//统计出的影像的每行的均值
	memset(pMeans,0,nSize*sizeof(float));

	float *pVars=new float[nSize];//统计出的影像的每行的方差
	memset(pVars,0,nSize*sizeof(float));

	float *pGaussMeans=new float[nSize];//对均值数据进行gauss处理后的结果
	memset(pGaussMeans,0,nSize*sizeof(float));

	float *pGaussVars=new float[nSize];//对方差数据进行gauss处理后的结果
	memset(pGaussVars,0,nSize*sizeof(float));

	float *pMeans2=new float[nSize];//根据均值与方差重新统计的均值
	memset(pMeans2,0,nSize*sizeof(float));

	float *pVars2=new float[nSize];//根据均值与方差重新统计的方差
	memset(pVars2,0,nSize*sizeof(float));

	float *pResMeans=new float[nSize];//处理后统计的均值结果
	memset(pResMeans,0,nSize*sizeof(float));

	float *pResVars=new float[nSize];//处理后统计的方差结果
	memset(pResVars,0,nSize*sizeof(float));

	int width =imgOral.GetCols();
	BYTE *pBuf=new BYTE[width];
	memset(pBuf,0,width*sizeof(BYTE));

//Static
	for (int i=0;i<nSize;i++)
	{
		memset(pBuf,0,width*sizeof(BYTE));
		imgOral.Read(pBuf,i);

		GetMeansVars (pBuf,width,pMeans[i],pVars[i]);
		if(bManMade) GetMeansVars2(pBuf,width,pMeans2[i],pVars2[i],pMeans[i],pVars[i]);
	}

	if (!bManMade) 
	{
		memcpy(pMeans2,pMeans,nSize*sizeof(float));
		memcpy(pVars2,pVars,nSize*sizeof(float));
	}

	float gmean=GetMeans(pMeans2,nSize);
	float gvars=GetMeans(pVars2,nSize);

//Low pass filter
	long e=0;
	while((1<<e)<nSize) e++;
	long w=1<<e;
	int fsize=nSize/8000;
	if (fsize<10) fsize=10;

	float *ai=new float[w];
	float *ar=new float[w];

	memset(ai,0,w*sizeof(float));
	memset(ar,0,w*sizeof(float));
	memcpy(ar,pMeans2,nSize*sizeof(float));
	for (i=nSize;i<w;i++) ar[i]=gmean;

	FFT1(ar,ai,e,1);
	FrequencyFilter1(ar,ai,w,5,fsize);
	FFT1(ar,ai,e,-1);
	memcpy(pGaussMeans,ar,nSize*sizeof(float));

	memset(ai,0,w*sizeof(float));
	memset(ar,0,w*sizeof(float));
	memcpy(ar,pVars2,nSize*sizeof(float));
	for (i=nSize;i<w;i++) ar[i]=gvars;

	FFT1(ar,ai,e,1);
	FrequencyFilter1(ar,ai,w,5,fsize);
	FFT1(ar,ai,e,-1);
	memcpy(pGaussVars,ar,nSize*sizeof(float));

	delete ar;
	delete ai;

	CSpVZImage imgRes;
	if (!imgRes.Open(strSaveDir,CSpVZImage::modeCreate)) 
	{
		AfxMessageBox("Creat File Fail!");
		return FALSE;
	}
	imgRes.SetCols(width);
	imgRes.SetRows(nSize);
	imgRes.SetPixelBytes(1);

	int size=0;
	const RGBQUAD *pQuad=imgOral.GetColorTable(size);
	RGBQUAD *pTmp=new RGBQUAD[size];
	memcpy(pTmp,pQuad,size*sizeof(RGBQUAD));
	imgRes.SetColorTable(pTmp,size);
	delete pTmp;
	pQuad=NULL;

	int j=0;
	BYTE *pB=NULL;
	float meanref=gmean,varref=0.0,meancur=0.0,varcur=0.0;
	for (i=0;i<nSize;i++)
	{
		memset(pBuf,0,width*sizeof(BYTE));
		imgOral.Read(pBuf,i);

		if(!bStainMeans) meanref=pGaussMeans[i];
		varref =pGaussVars[i];

		meancur=pMeans2[i],
		varcur =pVars2[i];

		pB=pBuf;
		for (j=0;j<width;j++)
		{
			*pB=min(max(0,int(meanref+varref/varcur*(*pB-meancur)+0.5)),255);
			pB++;
		}

		GetMeansVars(pBuf,width,pResMeans[i],pResVars[i]);

		imgRes.Write(pBuf,i);
	}

	imgOral.Close();
	imgRes.Close();

//	CString str=strSaveDir+".txt";
//	FILE *fp=fopen(str,"w");
//
//	fprintf(fp,"size\nmeans\tvars\tmeans2\tvars2\tgauss-means\tgauss-vars\tres-means\tres-vars\n");
//	fprintf(fp,"%d\n",nSize);
//	for (i=0;i<nSize;i++)
//	{
//		fprintf(fp,"%5.5f\t%5.5f\t%5.5f\t%5.5f\t%5.5f\t%5.5f\t%5.5f\t%5.5f\n",
//			pMeans[i],pVars[i],pMeans2[i],pVars2[i],pGaussMeans[i],pGaussVars[i],pResMeans[i],pResVars[i]);
//	}
//	fclose(fp);

	delete pMeans;
	delete pVars;
	delete pGaussMeans;
	delete pGaussVars;
	delete pMeans2;
	delete pVars2;
	delete pResMeans;
	delete pResVars;
	delete pBuf;

	return TRUE;
}

bool CAdjustLightColor::ADS40StandEliminate(CString strDir,CString strStdDir,CString strSaveDir)
{
	CSpVZImage imgStd;	//标准影像文件变量
	if (!imgStd.Open(strStdDir)) return FALSE;
	if (imgStd.GetPixelBytes()!=1) 
	{
		imgStd.Close();
		return FALSE;
	}
	
	int nStdSize=imgStd.GetRows(); //标准影像行数
	float *pStdMeans=new float[nStdSize];//标准影像的均值结果
	memset(pStdMeans,0,nStdSize*sizeof(float));
	float *pStdVars=new float[nStdSize]; //标准影像的方差结果
	memset(pStdVars,0,nStdSize*sizeof(float));

	int stdwidth =imgStd.GetCols();
	BYTE *pStdBuf=new BYTE[stdwidth];
	memset(pStdBuf,0,stdwidth*sizeof(BYTE));

	for (int i=0;i<nStdSize;i++)
	{
		memset(pStdBuf,0,stdwidth*sizeof(BYTE));
		imgStd.Read(pStdBuf,i);
		GetMeansVars (pStdBuf,stdwidth,pStdMeans[i],pStdVars[i]);
		GetMeansVars2(pStdBuf,stdwidth,pStdMeans[i],pStdVars[i],pStdMeans[i],pStdVars[i]);
	}
	imgStd.Close();
	delete pStdBuf;

	CSpVZImage imgOral;	//影像文件变量
	if (!imgOral.Open(strDir)) return FALSE;
	if (imgOral.GetPixelBytes()!=1) 
	{
		imgOral.Close();
		return FALSE;
	}

	int nSize=imgOral.GetRows(); //原始影像行数
	float nRatio=float(nStdSize)/float(nSize);
	float *pMeans=new float[nSize];//原始影像的每行的均值
	memset(pMeans,0,nSize*sizeof(float));
	float *pVars=new float[nSize]; //原始影像的每行的方差
	memset(pVars,0,nSize*sizeof(float));

	int width =imgOral.GetCols();
	BYTE *pBuf=new BYTE[width];
	memset(pBuf,0,width*sizeof(BYTE));

	CSpVZImage imgRes;
	if (!imgRes.Open(strSaveDir,CSpVZImage::modeCreate)) 
	{
		AfxMessageBox("Creat File Fail!");
		return FALSE;
	}
	imgRes.SetCols(width);
	imgRes.SetRows(nSize);
	imgRes.SetPixelBytes(1);

	int tbsize=0;
	const RGBQUAD *pQuad=imgOral.GetColorTable(tbsize);
	RGBQUAD *pTmp=new RGBQUAD[tbsize];
	memcpy(pTmp,pQuad,tbsize*sizeof(RGBQUAD));
	imgRes.SetColorTable(pTmp,tbsize);
	delete pTmp;
	pQuad=NULL;

	int j=0,pos=0;
	BYTE *pB=NULL;
	float meanref=0.0,varref=0.0,meancur=0.0,varcur=0.0,dist=0.0;
	for (i=0;i<nSize;i++)
	{
		memset(pBuf,0,width*sizeof(BYTE));
		imgOral.Read(pBuf,i);
		GetMeansVars(pBuf,width,pMeans[i],pVars[i]);
		GetMeansVars2(pBuf,width,pMeans[i],pVars[i],pMeans[i],pVars[i]);

		pos=i*nRatio;
		dist=i*nRatio-pos;

		if (dist<0.0005) 
		{
			meanref=pStdMeans[pos];
			varref =pStdVars [pos];
		}
		else if (dist>0.9995)
		{
			meanref=pStdMeans[pos+1];
			varref =pStdVars [pos+1];
		}
		else 
		{
			meanref=pStdMeans[pos]*(1.0-dist)+pStdMeans[pos+1]*dist;
			varref =pStdVars [pos]*(1.0-dist)+pStdVars [pos+1]*dist;;
		}

		meancur=pMeans[i],
		varcur =pVars [i];

		pB=pBuf;
		for (j=0;j<width;j++)
		{
			*pB=min(max(0,int(meanref+varref/varcur*(*pB-meancur)+0.5)),255);
			pB++;
		}

		imgRes.Write(pBuf,i);
	}

	imgOral.Close();
	imgRes.Close();

//	CString str=strSaveDir+".txt";
//	FILE *fp=fopen(str,"w");
//
//	fprintf(fp,"size\nmeans\tvars\tstdmeans\tstdvars\n");
//	fprintf(fp,"%d\n",nSize);
//	for (i=0;i<nStdSize;i++)
//	{
//		fprintf(fp,"%5.5f\t%5.5f\t%5.5f\t%5.5f\n",
//			pMeans[i],pVars[i],pStdMeans[i],pStdVars[i]);
//	}
//	fclose(fp);

	delete pMeans;
	delete pVars;
	delete pStdMeans;
	delete pStdVars;
	delete pBuf;

	return TRUE;
}

void CAdjustLightColor::GetMeansVars(BYTE *pGray,int gSize,float &means,float &vars)
{
	long pHisto[256];
	memset(pHisto,0,256*sizeof(long));

	BYTE *pG=pGray;
	for(int i=0;i<gSize;i++)
	{
		pHisto[*pG]++;
		pG++;
	}

	vars =0.0;
	means=0.0;
	for(int j=0;j<256;j++)
	{
		means+=float(pHisto[j])/gSize*j;
	}

	for(j=0;j<256;j++)
	{
		vars+=(j-means)*(j-means)*pHisto[j]/gSize;
	}

	vars=float(sqrt(vars));
}

void CAdjustLightColor::GetMeansVars2(BYTE *pGray,int gSize,float &means2,float &vars2,float mean,float var)
{
	long pHisto[256];
	memset(pHisto,0,256*sizeof(long));
	int nUseful=0;
	BYTE *pG=pGray;
	BYTE gmin=min(255,max(0,mean-2*var)); 
	BYTE gmax=min(255,max(0,mean+2*var)); 

	for(int i=0;i<gSize;i++)
	{
		if (*pG>=gmin && *pG<=gmax)
		{
			pHisto[*pG]++;
			nUseful++;
		}
		pG++;
	}

	vars2 =0.0;
	means2=0.0;
	for(int j=0;j<256;j++)
	{
		means2+=float(pHisto[j])/nUseful*j;
	}

	for(j=0;j<256;j++)
	{
		vars2+=(j-means2)*(j-means2)*pHisto[j]/nUseful;
	}

	vars2=float(sqrt(vars2));
}

bool CAdjustLightColor::CombainRGB(	CString strR,CString strG,CString strB,CString strSaveDir,
									int oXR,int oYR,float fR,
									int oXG,int oYG,float fG,
									int oXB,int oYB,float fB,
									BOOL bGrnControl)
{
	CSpVZImage imgR;
	if (!imgR.Open(strR)) 
	{
		AfxMessageBox("Open ADS40 File Fail!");
		return FALSE;
	}

	if (imgR.GetPixelBytes()!=1) 
	{
		AfxMessageBox("Can't Process Colour Image!");
		return FALSE;
	}
	int WidR=imgR.GetCols();
	int HeiR=imgR.GetRows();

	CSpVZImage imgG;
	if (!imgG.Open(strG)) 
	{
		AfxMessageBox("Open ADS40 File Fail!");
		return FALSE;
	}

	if (imgG.GetPixelBytes()!=1) 
	{
		AfxMessageBox("Can't Process Colour Image!");
		return FALSE;
	}
	int WidG=imgG.GetCols();
	int HeiG=imgG.GetRows();

	CSpVZImage imgB;
	if (!imgB.Open(strB)) 
	{
		AfxMessageBox("Open ADS40 File Fail!");
		return FALSE;
	}

	if (imgB.GetPixelBytes()!=1) 
	{
		AfxMessageBox("Can't Process Colour Image!");
		return FALSE;
	}
	int WidB=imgB.GetCols();
	int HeiB=imgB.GetRows();

	if (WidR!=WidG||WidR!=WidB||WidG!=WidB)
	{
		AfxMessageBox("Image Width Error!");
		return FALSE;
	}

	int WidNew=WidR;
	int HeiNew=min(HeiB-oYB,min(HeiR-oYR,HeiG-oYG));

	CSpVZImage imgRes;
	imgRes.Open(strSaveDir,CSpVZImage::modeCreate);
	imgRes.SetCols(WidNew);
	imgRes.SetRows(HeiNew);
	imgRes.SetPixelBytes(3);

	int tsize=256;
	RGBQUAD *pQuad=new RGBQUAD[tsize];
	for (int i=0;i<tsize;i++) 
		pQuad[i].rgbBlue=pQuad[i].rgbGreen=pQuad[i].rgbRed=pQuad[i].rgbReserved=i;
	imgRes.SetColorTable(pQuad,tsize);
	delete pQuad;

	BYTE *pBuf=new BYTE[3*WidNew];
	BYTE *pBufR=new BYTE[WidNew];
	BYTE *pBufG=new BYTE[WidNew];
	BYTE *pBufB=new BYTE[WidNew];

	int j=0;
	BYTE *pR=NULL,*pG=NULL,*pB=NULL,*pA=NULL,*pCur=NULL;
	float meansRed=0.0,meansGrn=0.0,meansBlu=0.0;
	float varsRed=0.0 ,varsGrn=0.0 ,varsBlu=0.0;
	for (i=0;i<HeiNew;i++)
	{
		memset(pBuf,0,3*WidNew*sizeof(BYTE));
		memset(pBufR,0,WidNew*sizeof(BYTE));
		memset(pBufG,0,WidNew*sizeof(BYTE));
		memset(pBufB,0,WidNew*sizeof(BYTE));

		imgR.Read(pBufR,i+oYR);
		imgG.Read(pBufG,i+oYG);
		imgB.Read(pBufB,i+oYB);

		if (bGrnControl)
		{
			GetMeansVars (pBufR,WidNew,meansRed,varsRed);
			GetMeansVars2(pBufR,WidNew,meansRed,varsRed,meansRed,varsRed);

			GetMeansVars (pBufG,WidNew,meansGrn,varsGrn);
			GetMeansVars2(pBufG,WidNew,meansGrn,varsGrn,meansGrn,varsGrn);

			GetMeansVars (pBufB,WidNew,meansBlu,varsBlu);
			GetMeansVars2(pBufB,WidNew,meansBlu,varsBlu,meansBlu,varsBlu);

			pCur=pBufR;
			for (j=0;j<WidNew;j++)
			{
				*pCur=min(max(0,int(meansGrn+varsGrn/varsRed*(*pCur-meansRed)+0.5)),255);
				pCur++;
			}

			pCur=pBufB;
			for (j=0;j<WidNew;j++)
			{
				*pCur=min(max(0,int(meansGrn+varsGrn/varsBlu*(*pCur-meansBlu)+0.5)),255);
				pCur++;
			}
			pCur=NULL;
		}

		pR=pBufR+oXR;
		pG=pBufG+oXG;
		pB=pBufB+oXB;
		pA=pBuf;

		for (j=0;j<WidNew;j++)
		{
			*pA=*pR *fR; pA++; pR++;
			*pA=*pG *fG; pA++; pG++;
			*pA=*pB *fB; pA++; pB++;
 		}

		imgRes.Write(pBuf,i);
	}
	delete pBuf;
	delete pBufR;
	delete pBufG;
	delete pBufB;

	imgR.Close();
	imgG.Close();
	imgB.Close();
	imgRes.Close();

	strSaveDir+=".txt";
	FILE *fp=fopen(strSaveDir,"w");
	if (fp==NULL) return FALSE;

	fprintf(fp,"Band:\tXOffset\tYOffset\tBandFact\n");
	fprintf(fp,"R:\t%d\t%d\t%f\n",oXR,oYR,fR);
	fprintf(fp,"G:\t%d\t%d\t%f\n",oXG,oYG,fG);
	fprintf(fp,"B:\t%d\t%d\t%f",  oXB,oYB,fB);

	fclose(fp);

	return TRUE;
}

float CAdjustLightColor::GetMeans(float *pData,int size)
{
	double edata=0;
	float *pD=pData;
	for (int i=0;i<size;i++)
	{
		edata+=*pD;
		pD++;
	}
	float means=edata/size;

	return means;
}

bool CAdjustLightColor::AdjustBBIColor(CString strBBI,int stdbiBit,float fVarRef[4],float fMeanRef[4],RGBQUAD *pBackGrd,int nBackGrd,BOOL bReWrite)
{
	int bpp=0;
	float fvars[4]={0.0,0.0,0.0,0.0},fmeans[4]={0.0,0.0,0.0,0.0};
	if (!GetBBIStatistic(strBBI,bpp,fvars,fmeans,pBackGrd,nBackGrd)) return false;
	if (bpp!=stdbiBit) return false;

	BYTE lutR[256]={0};
	BYTE lutG[256]={0};
	BYTE lutB[256]={0};
	BYTE lutA[256]={0};
	CalculateLut(bpp,lutR,lutG,lutB,lutA,fVarRef,fMeanRef,fvars,fmeans);

	if (!bReWrite) 
	{
		CString str=strBBI+".ctb";
		FILE *fp=fopen(str,"wt");
		if (fp==NULL) return false;
		int val=0,r=0,g=0,b=0;
		for (int i=0;i<256;i++)
		{
			if (bpp==8) r=g=b=lutA[i];
			else if (bpp==24) 
			{
				r=lutR[i];
				g=lutG[i];
				b=lutB[i];
			}

			if (i==0) {r=g=b=i; fprintf(fp,"%5d\t%5d\t%5d\t%5d\n",r,g,b,i);}
			else if (i==255) {r=g=b=i; fprintf(fp,"%5d\t%5d\t%5d\t%5d\n",r,g,b,i);}
			else fprintf(fp,"%5d\t%5d\t%5d\t%5d\n",r,g,b,i);
		}
		fclose(fp);
		return true;
	}

	CSpBBImage2 bbifile;
	if (!bbifile.Open(strBBI,CSpBBImage::modeReadWrite)) return false;

	int blksize=bbifile.GetBlockSize();
	int blkrows=(bbifile.GetRows()+blksize-1)/blksize;
	int blkcols=(bbifile.GetCols()+blksize-1)/blksize;
	int npb=bbifile.GetPixelBytes();

	int blksize2=blksize*blksize;
	int bsize=blksize2*npb;
	BYTE *pBuf=new BYTE[bsize];
	BYTE *lpPos=NULL,gray=0,r=0,g=0,b=0;
	int rid=0,cid=0,bs=0;
	int iBit=npb*8;
	int iv=0;
	
	for (rid=0;rid<blkrows;rid++)
	{
		for (cid=0;cid<blkcols;cid++)
		{
			memset(pBuf,0,bsize*sizeof(BYTE));
			bbifile.ReadBlock(pBuf,cid,rid);

			lpPos=pBuf;
			for (bs=0;bs<blksize2;bs++)
			{
				if(nBackGrd>0 && pBackGrd!=NULL)
				{
					if(BeBackground(lpPos,iBit,pBackGrd,nBackGrd)) 
					{
						lpPos+=npb;
						continue;
					}
				}

				if(iBit==8)
				{
					*lpPos=lutA[*lpPos];
					lpPos++;
				}
				else
				{
					*lpPos=lutR[*lpPos];
					lpPos++;

					*lpPos=lutG[*lpPos];
					lpPos++;

					*lpPos=lutB[*lpPos];
					lpPos++;
 				}
			}

			bbifile.WriteBlock(pBuf,cid,rid);			
		}
	}
	delete pBuf;

	bbifile.CreatePyramid2();
	bbifile.Close();

	return true;
}

bool CAdjustLightColor::GetBBIStatistic(CString strBBI,int &bpp,float fvars[4],float fmeans[4],RGBQUAD *pBackGrd,int nBackGrd)
{
	CSpBBImage2 bbifile;
	if (!bbifile.Open(strBBI)) return false;

	int blksize=bbifile.GetBlockSize();
	int blkrows=(bbifile.GetRows()+blksize-1)/blksize;
	int blkcols=(bbifile.GetCols()+blksize-1)/blksize;
	int npb=bbifile.GetPixelBytes();

	LONGLONG nUsefulSize=0;
	LONGLONG pHisto[256],pHistor[256],pHistog[256],pHistob[256];
	memset(pHisto,0,256*sizeof(LONGLONG));
	memset(pHistor,0,256*sizeof(LONGLONG));
	memset(pHistog,0,256*sizeof(LONGLONG));
	memset(pHistob,0,256*sizeof(LONGLONG));

	int blksize2=blksize*blksize;
	int bsize=blksize2*npb;
	BYTE *pBuf=new BYTE[bsize];
	BYTE *lpPos=NULL,gray=0,r=0,g=0,b=0;
	int rid=0,cid=0,bs=0;
	int iBit=bpp=npb*8;
	
	for (rid=0;rid<blkrows;rid++)
	{
		for (cid=0;cid<blkcols;cid++)
		{
			memset(pBuf,0,bsize*sizeof(BYTE));
			bbifile.ReadBlock(pBuf,cid,rid);
			lpPos=pBuf;

			for (bs=0;bs<blksize2;bs++)
			{
				nUsefulSize++;

				if(nBackGrd>0 && pBackGrd!=NULL)
				{
					if(BeBackground(lpPos,iBit,pBackGrd,nBackGrd)) 
					{
						nUsefulSize--;
						lpPos+=npb;
						continue;
					}
				}
				
				if(iBit==8)
				{
					gray=*(lpPos);
					pHisto[gray]++;
					lpPos++;
				}
				else
				{
					r=*(lpPos); lpPos++;
					g=*(lpPos); lpPos++;
					b=*(lpPos); lpPos++;

					pHistor[r]++;
					pHistog[g]++;
					pHistob[b]++;
				}
			}
		}
	}

	if(nUsefulSize<=0) return FALSE;
	for(int j=0;j<4;j++)
	{
		fvars[j]=0.0f;
		fmeans[j]=0.0f;
	}

	for(j=0;j<256;j++)
	{
		fmeans[0]+=float(pHistor[j])/nUsefulSize*j;
		fmeans[1]+=float(pHistog[j])/nUsefulSize*j;
		fmeans[2]+=float(pHistob[j])/nUsefulSize*j;
		fmeans[3]+=float(pHisto[j])/nUsefulSize*j;
	}

	for(j=0;j<256;j++)
	{
		fvars[0]+=(j-fmeans[0])*(j-fmeans[0])*pHistor[j]/nUsefulSize;
		fvars[1]+=(j-fmeans[1])*(j-fmeans[1])*pHistog[j]/nUsefulSize;
		fvars[2]+=(j-fmeans[2])*(j-fmeans[2])*pHistob[j]/nUsefulSize;
		fvars[3]+=(j-fmeans[3])*(j-fmeans[3])*pHisto[j]/nUsefulSize;
	}

	for(j=0;j<4;j++)
	{
		fvars[j]=float(sqrt(fvars[j]));
	}

	delete pBuf;

	bbifile.Close();

	return true;
}

bool CAdjustLightColor::AdjustBBIRGB(CString strBBI,int biBit,int rgbs[4],RGBQUAD *pBackGrd,int nBackGrd)
{
	CSpBBImage2 bbifile;
	if (!bbifile.Open(strBBI,CSpBBImage::modeReadWrite)) return false;

	int blksize=bbifile.GetBlockSize();
	int blkrows=(bbifile.GetRows()+blksize-1)/blksize;
	int blkcols=(bbifile.GetCols()+blksize-1)/blksize;
	int npb=bbifile.GetPixelBytes();
	int iBit=npb*8;
	if (iBit!=biBit) 
	{
		bbifile.Close();
		return false;
	}

	int blksize2=blksize*blksize;
	int bsize=blksize2*npb;
	BYTE *pBuf=new BYTE[bsize];
	BYTE *lpPos=NULL,gray=0,r=0,g=0,b=0;
	int rid=0,cid=0,bs=0;
	int iv=0;

	for (rid=0;rid<blkrows;rid++)
	{
		for (cid=0;cid<blkcols;cid++)
		{
			memset(pBuf,0,bsize*sizeof(BYTE));
			bbifile.ReadBlock(pBuf,cid,rid);

			lpPos=pBuf;
			for (bs=0;bs<blksize2;bs++)
			{
				if(nBackGrd>0 && pBackGrd!=NULL)
				{
					if(BeBackground(lpPos,iBit,pBackGrd,nBackGrd)) 
					{
						lpPos+=npb;
						continue;
					}
				}

				if(iBit==8)
				{
					iv=*lpPos;
					iv+=rgbs[3];
					if(iv>255) iv=255;
					if(iv<0) iv=0;
					*lpPos=iv;
					lpPos++;
				}
				else
				{
					iv=*lpPos;
					iv+=rgbs[0];
					if(iv>255) iv=255;
					if(iv<0) iv=0;
					*lpPos=iv;
					lpPos++;

					iv=*lpPos;
					iv+=rgbs[1];
					if(iv>255) iv=255;
					if(iv<0) iv=0;
					*lpPos=iv;
					lpPos++;

					iv=*lpPos;
					iv+=rgbs[2];
					if(iv>255) iv=255;
					if(iv<0) iv=0;
					*lpPos=iv;
					lpPos++;
 				}
			}

			bbifile.WriteBlock(pBuf,cid,rid);			
		}
	}
	delete pBuf;

	bbifile.CreatePyramid2();
	bbifile.Close();

	return true;	
}

bool CAdjustLightColor::GetBigFileStatistic(CString strBigFileDir,int &bpp,float fvars[4],float fmeans[4],RGBQUAD *pBackGrd,int nBackGrd)
{
	CSpVZImage imgOral;
	if (!imgOral.Open(strBigFileDir)) return false;
	int npb=imgOral.GetPixelBytes();
	if (npb!=1&&npb!=3&&npb!=4) 
	{
		imgOral.Close();
		return false;
	}

	LONGLONG nUsefulSize=0;
	LONGLONG pHisto[256],pHistor[256],pHistog[256],pHistob[256];
	memset(pHisto,0,256*sizeof(LONGLONG));
	memset(pHistor,0,256*sizeof(LONGLONG));
	memset(pHistog,0,256*sizeof(LONGLONG));
	memset(pHistob,0,256*sizeof(LONGLONG));

	int cols=imgOral.GetCols();
	int rows=imgOral.GetRows();
	int bsize=cols*npb;
	BYTE *pBuf=new BYTE[bsize];
	BYTE *lpPos=NULL,gray=0,r=0,g=0,b=0;
	int iBit=bpp=npb*8;
	int rid=0,bs=0;

	for (rid=0;rid<rows;rid++)
	{
		memset(pBuf,0,bsize*sizeof(BYTE));
		imgOral.Read(pBuf,rid);
		lpPos=pBuf;

		for (bs=0;bs<cols;bs++)
		{
			nUsefulSize++;

			if(nBackGrd>0 && pBackGrd!=NULL)
			{
				if(BeBackground(lpPos,iBit,pBackGrd,nBackGrd)) 
				{
					nUsefulSize--;
					lpPos+=npb;
					continue;
				}
			}
			
			if(iBit==8)
			{
				gray=*(lpPos);
				pHisto[gray]++;
				lpPos++;
			}
			else if(iBit==24)
			{
				r=*(lpPos); lpPos++;
				g=*(lpPos); lpPos++;
				b=*(lpPos); lpPos++;

				pHistor[r]++;
				pHistog[g]++;
				pHistob[b]++;
			}
			else if (iBit==32)
			{
				r=*(lpPos); lpPos++;
				g=*(lpPos); lpPos++;
				b=*(lpPos); lpPos++;
				gray=*(lpPos); lpPos++;

				pHistor[r]++;
				pHistog[g]++;
				pHistob[b]++;
				pHisto[gray]++;
			}
		}
	}

	if(nUsefulSize<=0) return FALSE;
	for(int j=0;j<4;j++)
	{
		fvars[j]=0.0f;
		fmeans[j]=0.0f;
	}

	for(j=0;j<256;j++)
	{
		fmeans[0]+=float(pHistor[j])/nUsefulSize*j;
		fmeans[1]+=float(pHistog[j])/nUsefulSize*j;
		fmeans[2]+=float(pHistob[j])/nUsefulSize*j;
		fmeans[3]+=float(pHisto[j])/nUsefulSize*j;
	}

	for(j=0;j<256;j++)
	{
		fvars[0]+=(j-fmeans[0])*(j-fmeans[0])*pHistor[j]/nUsefulSize;
		fvars[1]+=(j-fmeans[1])*(j-fmeans[1])*pHistog[j]/nUsefulSize;
		fvars[2]+=(j-fmeans[2])*(j-fmeans[2])*pHistob[j]/nUsefulSize;
		fvars[3]+=(j-fmeans[3])*(j-fmeans[3])*pHisto[j]/nUsefulSize;
	}

	for(j=0;j<4;j++)
	{
		fvars[j]=float(sqrt(fvars[j]));
	}
	delete pBuf;
	imgOral.Close();

	return true;
}

bool CAdjustLightColor::AdjustBigFileColor(CString strBigFileDir,int stdbiBit,float fVarRef[4],float fMeanRef[4],RGBQUAD *pBackGrd,int nBackGrd)
{
	int bpp=0;
	float fvars[4]={0.0,0.0,0.0,0.0},fmeans[4]={0.0,0.0,0.0,0.0};
	if (!GetBigFileStatistic(strBigFileDir,bpp,fvars,fmeans,pBackGrd,nBackGrd)) return false;
	if (bpp!=stdbiBit||(bpp!=8&&bpp!=24&&bpp!=32)) return false;

	CSpVZImage imgOral;	//影像文件变量
	if (!imgOral.Open(strBigFileDir)) return false;
	int npb=imgOral.GetPixelBytes();
	int iBit=npb*8;
	if (iBit!=stdbiBit) 
	{
		imgOral.Close();
		return false;
	}

	int rows=imgOral.GetRows();
	int clos=imgOral.GetCols();

	CString strTmp=strBigFileDir.Left(strBigFileDir.GetLength()-4)+"-d.tif";
	CSpVZImage imgTmp;
	if (!imgTmp.Open(strTmp,CSpVZImage::modeCreate)) 
	{
		imgOral.Close();		
		return false;
	}
	imgTmp.SetRows(rows);
	imgTmp.SetCols(clos);
	if (npb==4) imgTmp.SetPixelBytes(3); //some problem
	else imgTmp.SetPixelBytes(npb);

	RGBQUAD rgbq[256];
	for (int i=0;i<256;i++)
	{
		rgbq[i].rgbBlue=rgbq[i].rgbGreen=rgbq[i].rgbRed=rgbq[i].rgbReserved=i;
	}
	imgTmp.SetColorTable(rgbq,256);

	int bsize=clos*npb;
	BYTE *pBuf=new BYTE[bsize];
	BYTE *lpPos=NULL;
	int rid=0,bs=0;
	int iv=0,g1=0,g2=0;

	int rpb=3;
	if (npb==1) rpb=1;
	int rsize=clos*rpb;
	BYTE *pRes=new BYTE[rsize];
	BYTE *lpRes=NULL;

	for (rid=0;rid<rows;rid++)
	{
		memset(pRes,0,rsize*sizeof(BYTE));
		lpRes=pRes;

		memset(pBuf,0,bsize*sizeof(BYTE));
		imgOral.Read(pBuf,rid);
		lpPos=pBuf;

		for (bs=0;bs<clos;bs++)
		{
			if(nBackGrd>0 && pBackGrd!=NULL)
			{
				if(BeBackground(lpPos,iBit,pBackGrd,nBackGrd)) 
				{
					lpPos+=npb;
					lpRes+=rpb;
					continue;
				}
			}

			if(iBit==8)
			{
				iv=*lpPos;
				iv=int(fMeanRef[3]+fVarRef[3]/fvars[3]*(iv-fmeans[3])+0.5);
				if(iv>255) iv=255;
				if(iv<0) iv=0;
				*lpRes=iv;
				lpPos++;
				lpRes++;
			}
			else if(iBit==24)
			{
				iv=*lpPos;
				iv=int(fMeanRef[0]+fVarRef[0]/fvars[0]*(iv-fmeans[0])+0.5);
				if(iv>255) iv=255;
				if(iv<0) iv=0;
				*lpRes=iv;
				lpPos++;
				lpRes++;

				iv=*lpPos;
				iv=int(fMeanRef[1]+fVarRef[1]/fvars[1]*(iv-fmeans[1])+0.5);
				if(iv>255) iv=255;
				if(iv<0) iv=0;
				*lpRes=iv;
				lpPos++;
				lpRes++;

				iv=*lpPos;
				iv=int(fMeanRef[2]+fVarRef[2]/fvars[2]*(iv-fmeans[2])+0.5);
				if(iv>255) iv=255;
				if(iv<0) iv=0;
				*lpRes=iv;
				lpPos++;
				lpRes++;
 			}
			else if (iBit==32)
			{
				iv=*lpPos;
				iv=int(fMeanRef[0]+fVarRef[0]/fvars[0]*(iv-fmeans[0])+0.5);
				if(iv>255) iv=255;
				if(iv<0) iv=0;
				*lpRes=iv;
				lpPos++;
				lpRes++;

				iv=*lpPos;
				iv=int(fMeanRef[1]+fVarRef[1]/fvars[1]*(iv-fmeans[1])+0.5);
				if(iv>255) iv=255;
				if(iv<0) iv=0;
				*lpRes=g1=iv;
				lpPos++;
				lpRes++;

				iv=*lpPos;
				iv=int(fMeanRef[2]+fVarRef[2]/fvars[2]*(iv-fmeans[2])+0.5);
				if(iv>255) iv=255;
				if(iv<0) iv=0;
				*lpRes=iv;
				lpPos++;
				lpRes++;

				iv=*lpPos;
				iv=int(fMeanRef[3]+fVarRef[3]/fvars[3]*(iv-fmeans[3])+0.5);
				if(iv>255) iv=255;
				if(iv<0) iv=0;
				g2=iv;
				lpPos++;

				g1+=0.08*g2;
				if(g1>255) g1=255;
				if(g1<0) g1=0;
				*(lpRes-2)=g1;
			}
		}
		imgTmp.Write(pRes,rid);	
	}
	delete pBuf;
	delete pRes;

	imgOral.Close();
	imgTmp.Close();

	return true;
}

void CAdjustLightColor::CalCircleSet(int p,int r,double *dxy)
{
	double step = 2*PI/p;
	double tmpX=0.0,tmpY=0.0;
	for (int i=0;i<p;i++)
	{
		tmpX = r * cos(i * step);
		tmpY = r * sin(i * step);

		if (tmpX < 1.0e-5 && tmpX > -1.0e-5) tmpX=0;
		if (tmpY < 1.0e-5 && tmpY > -1.0e-5) tmpY=0;

		*(dxy+2*i)  =tmpX;
		*(dxy+2*i+1)=tmpY;
	}
}

inline BYTE CAdjustLightColor::BilinearInt(BYTE *gray,int w,int h,double xy[2])
{
	if (xy[0]<0||xy[0]>=w || xy[1]<0||xy[1]>=h) return 0;

	int x = xy[0]; double dx=xy[0]-x;
	int y = xy[1]; double dy=xy[1]-y;

	BYTE *pg=gray+y*w+x;
	BYTE res=*pg*(1-dx)*(1-dy)+0.5;
	res+=*(pg+1)*dx*(1-dy)+0.5;
	pg+=w;
	res+=*pg*(1-dx)*dy+0.5;
	res+=*(pg+1)*dx*dy+0.5;

	return res;
}

void CAdjustLightColor::GetLBPLC(BYTE *gray,int p,BYTE *gcent,int &lbp,int &lc)
{
	lbp=0;
	int gdis=0;
	int n1=0,sum1=0,sum2=0;

	for (int i=0;i<p;i++)
	{
		gdis = gray[i] - *gcent;
		lbp |= ( (unsigned int)gdis & 0x80000000 ) >> (31-i);

		if (gdis>=0) 
		{
			sum1 +=gray[i];
			n1++;
		}
		else sum2 +=gray[i];
	}

	if (n1==0||n1==p) lc=0;
	else lc=sum1/n1 - sum2/(p-n1);
}

bool CAdjustLightColor::TextureRecognition(CString strDir,CString strSaveDir,int LBP1,int LBP2,int LC1,int LC2,int nIgnore,int nRadius,int nPixels)
{
	CxImage imgOral;
	if (!imgOral.Load(strDir)) return false;
	if (!imgOral.IsGrayScale()) imgOral.GrayScale();

	int cols=imgOral.GetEffWidth();
	int rows=imgOral.GetHeight();
	int winsize=nRadius*2+1;
	if (cols<winsize||rows<winsize) return false;

	CxImage imgRes;
	if (!imgRes.Create(imgOral.GetWidth(),imgOral.GetHeight(),8)) return false;
	RGBQUAD rgbq[256];
	for (int i=0;i<256;i++)	rgbq[i].rgbBlue=rgbq[i].rgbGreen=rgbq[i].rgbRed=rgbq[i].rgbReserved=i;
	imgRes.SetPalette(rgbq);

	BYTE *pRes = imgRes.GetBits();
	memset(pRes,0,imgRes.GetEffWidth()*rows*sizeof(BYTE));

	double *pDxy=new double[nPixels*2];
	CalCircleSet(nPixels,nRadius,pDxy);

	int j=0,k=0;
	int lbp=0,lc=0;
	double xy[2]={0.0};
	BYTE *gCent=NULL;
	BYTE *pGray=new BYTE[cols*winsize];
	BYTE *pCircle=new BYTE[nPixels];
	for (i=nRadius;i<rows-nRadius;i++)
	{
		pGray=imgOral.GetBits(i-nRadius);
		gCent=pGray+nRadius*cols+nRadius;
		pRes =imgRes.GetBits(i)+nRadius;

		for (j=nRadius;j<cols-nRadius;j++) 
		{
			for (k=0;k<nPixels;k++)
			{
				xy[0]=j+pDxy[2*k];
				xy[1]=nRadius+pDxy[2*k+1];
				pCircle[k]=BilinearInt(pGray,cols,winsize,xy);
			}
			GetLBPLC(pCircle,nPixels,gCent,lbp,lc);
			if (lbp>=LBP1&&lbp<=LBP2 && lc>=LC1&&lc<=LC2) *pRes=255;

			gCent++;
			pRes++;
		}
	}
	imgRes.Threshold(128);
	if (nIgnore>0) imgRes.Erode(nIgnore);
	imgRes.Dilate(nRadius+nIgnore);

	imgRes.Save(strSaveDir,CXIMAGE_FORMAT_TIF);
	imgRes.Destroy();

	delete pDxy;
	delete pGray;
	delete pCircle;
	imgOral.Destroy();

	return true;
}

LONGLONG CAdjustLightColor::GetBBILapStatistic(CString strBBIDir1,float fVars1[4],float fMeans1[4],CString strBBIDir2,float fVars2[4],float fMeans2[4],RGBQUAD *pBackGrd,int nBackGrd)
{
	CSpBBImage2 bbi1;
	if (!bbi1.Open(strBBIDir1)) return 0;
	double minx1=0.0,miny1=0.0,meanz1=0.0,gsd1=0.0;
	int rows1=0.0,cols1=0.0,blksize1=0.0;
	bbi1.GetGeoInfo(minx1,miny1,meanz1,gsd1,rows1,cols1,blksize1);
	int nPbyte1=bbi1.GetPixelBytes();

	CSpBBImage2 bbi2;
	if (!bbi2.Open(strBBIDir2))
	{
		bbi1.Close();
		return 0;
	}
	double minx2=0.0,miny2=0.0,meanz2=0.0,gsd2=0.0;
	int rows2=0.0,cols2=0.0,blksize2=0.0;
	bbi2.GetGeoInfo(minx2,miny2,meanz2,gsd2,rows2,cols2,blksize2);
	int nPbyte2=bbi2.GetPixelBytes();

	if (nPbyte1!=nPbyte2 ||
		minx1>minx2+cols2*gsd2 || minx2>minx1+cols1*gsd1 ||
		miny1>miny2+rows2*gsd2 || miny2>miny1+rows1*gsd1 )
	{
		bbi1.Close();
		bbi2.Close();
		return 0;
	}

	LONGLONG nUsefulSize=0;
	LONGLONG pHisto1[256],pHistor1[256],pHistog1[256],pHistob1[256];
	memset(pHisto1,0,256*sizeof(LONGLONG));
	memset(pHistor1,0,256*sizeof(LONGLONG));
	memset(pHistog1,0,256*sizeof(LONGLONG));
	memset(pHistob1,0,256*sizeof(LONGLONG));

	LONGLONG pHisto2[256],pHistor2[256],pHistog2[256],pHistob2[256];
	memset(pHisto2,0,256*sizeof(LONGLONG));
	memset(pHistor2,0,256*sizeof(LONGLONG));
	memset(pHistog2,0,256*sizeof(LONGLONG));
	memset(pHistob2,0,256*sizeof(LONGLONG));

	double xstart=max(minx1,minx2);
	double ystart=max(miny1,miny2);
	double xend=min(minx1+(cols1-1)*gsd1,minx2+(cols2-1)*gsd2);
	double yend=min(miny1+(rows1-1)*gsd1,miny2+(rows2-1)*gsd2);

	double blkstep=5000*gsd1;
	int blkx=(xend-xstart)/blkstep;
	if ( (int(xend-xstart))%int(blkstep)!=0 ) blkx++;

	int blky=(yend-ystart)/blkstep;
	if ( (int(yend-ystart))%int(blkstep)!=0 ) blky++;

	for (int j=0;j<blky;j++)
	{
		for (int i=0;i<blkx;i++)
		{
			double x1=xstart+i*blkstep;
			double y1=ystart+j*blkstep;

			double x2=x1+blkstep;
			if (x2>xend) x2=xend;

			double y2=y1+blkstep;
			if (y2>yend) y2=yend;

			int sRow1=(y1-miny1)/gsd1+0.5;
			int sCol1=(x1-minx1)/gsd1+0.5;

			int sRow2=(y1-miny2)/gsd2+0.5;
			int sCol2=(x1-minx2)/gsd2+0.5;

			int rows=(y2-y1)/gsd1+1.5;
			int cols=(x2-x1)/gsd1+1.5;

			BYTE *pBuf1=new BYTE[rows*rows*nPbyte1];
			memset(pBuf1,0,rows*rows*nPbyte1*sizeof(BYTE));
			bbi1.Read(pBuf1,nPbyte1,sRow1,sCol1,rows,cols);
			BYTE *lpPos1=pBuf1;

			BYTE *pBuf2=new BYTE[rows*rows*nPbyte2];
			memset(pBuf2,0,rows*rows*nPbyte2*sizeof(BYTE));
			bbi2.Read(pBuf2,nPbyte2,sRow2,sCol2,rows,cols);
			BYTE *lpPos2=pBuf2;

			BYTE gray=0,r=0,g=0,b=0;
			int blksize2=rows*rows,iBit=nPbyte1*8;
			for (int bs=0;bs<blksize2;bs++)
			{
				nUsefulSize++;
				if(nBackGrd>0 && pBackGrd!=NULL)
				{
					if(BeBackground(lpPos1,iBit,pBackGrd,nBackGrd)||BeBackground(lpPos2,iBit,pBackGrd,nBackGrd)) 
					{
						nUsefulSize--;
						lpPos1+=nPbyte1;
						lpPos2+=nPbyte2;
						continue;
					}
				}

				if(iBit==8)
				{
					gray=*(lpPos1);
					pHisto1[gray]++;
					lpPos1++;

					gray=*(lpPos2);
					pHisto2[gray]++;
					lpPos2++;
				}
				else
				{
					r=*(lpPos1); lpPos1++;
					g=*(lpPos1); lpPos1++;
					b=*(lpPos1); lpPos1++;

					pHistor1[r]++;
					pHistog1[g]++;
					pHistob1[b]++;

					r=*(lpPos2); lpPos2++;
					g=*(lpPos2); lpPos2++;
					b=*(lpPos2); lpPos2++;

					pHistor2[r]++;
					pHistog2[g]++;
					pHistob2[b]++;
				}
			}

			delete pBuf1;
			delete pBuf2;
		}
	}
	if(nUsefulSize<=0) return 0;

	memset(fVars1 ,0,4*sizeof(float));
	memset(fMeans1,0,4*sizeof(float));
	memset(fVars2 ,0,4*sizeof(float));
	memset(fMeans2,0,4*sizeof(float));

	for(j=0;j<256;j++)
	{
		fMeans1[0]+=float(pHistor1[j])/nUsefulSize*j;
		fMeans1[1]+=float(pHistog1[j])/nUsefulSize*j;
		fMeans1[2]+=float(pHistob1[j])/nUsefulSize*j;
		fMeans1[3]+=float(pHisto1[j] )/nUsefulSize*j;

		fMeans2[0]+=float(pHistor2[j])/nUsefulSize*j;
		fMeans2[1]+=float(pHistog2[j])/nUsefulSize*j;
		fMeans2[2]+=float(pHistob2[j])/nUsefulSize*j;
		fMeans2[3]+=float(pHisto2[j] )/nUsefulSize*j;
	}

	for(j=0;j<256;j++)
	{
		fVars1[0]+=(j-fMeans1[0])*(j-fMeans1[0])*pHistor1[j]/nUsefulSize;
		fVars1[1]+=(j-fMeans1[1])*(j-fMeans1[1])*pHistog1[j]/nUsefulSize;
		fVars1[2]+=(j-fMeans1[2])*(j-fMeans1[2])*pHistob1[j]/nUsefulSize;
		fVars1[3]+=(j-fMeans1[3])*(j-fMeans1[3])*pHisto1[j]/nUsefulSize;

		fVars2[0]+=(j-fMeans2[0])*(j-fMeans2[0])*pHistor2[j]/nUsefulSize;
		fVars2[1]+=(j-fMeans2[1])*(j-fMeans2[1])*pHistog2[j]/nUsefulSize;
		fVars2[2]+=(j-fMeans2[2])*(j-fMeans2[2])*pHistob2[j]/nUsefulSize;
		fVars2[3]+=(j-fMeans2[3])*(j-fMeans2[3])*pHisto2[j]/nUsefulSize;
	}

	for(j=0;j<4;j++)
	{
		fVars1[j]=float(sqrt(fVars1[j]));
		fVars2[j]=float(sqrt(fVars2[j]));
	}

	bbi1.Close();
	bbi2.Close();

	return nUsefulSize;
}

BOOL CAdjustLightColor::ReadTFWFILE(CString strTifDir,double &left,double &top,double &gsd)
{
	int pos=strTifDir.ReverseFind('.');
	CString strSet=strTifDir.Left(pos);
	CString strtype=strTifDir.Right(strTifDir.GetLength()-pos-1);
	strtype.MakeLower();
	CString strSetType="";
	strSetType.Format(".%c%cw",strtype.GetAt(0),strtype.GetAt(2));
	strSet+=strSetType;

	FILE *fp=fopen(strSet,"r");
	if(fp==NULL) return FALSE;

	double res1=0.0,res2=0.0;
	fscanf(fp,"%lf\n",&res1);
	fscanf(fp,"0\n");
	fscanf(fp,"0\n");
	fscanf(fp,"%lf\n",&res2);
	gsd=res1;
	fscanf(fp,"%lf\n%lf\n",&left,&top);

	fclose(fp);
	return TRUE;
}

long CAdjustLightColor::GetLapStatistic(CString strOrthoDir1,float fVars1[4],float fMeans1[4],CString strOrthoDir2,float fVars2[4],float fMeans2[4],RGBQUAD *pBackGrd,int nBackGrd)
{
	int nPBB1=0,nPBB2=0;
	double left=0.0,top=0.0,nGsd1=0.0,nGsd2=0.0;
	if (!ReadTFWFILE(strOrthoDir1,left,top,nGsd1)) return 0;

	CSpVZImage imgOrtho1;
	if (!imgOrtho1.Open(strOrthoDir1)) return 0;
	RECTBOX nBox1(left,left+(imgOrtho1.GetCols()-1)*nGsd1,top,top-(imgOrtho1.GetRows()-1)*nGsd1);
	nPBB1=imgOrtho1.GetPixelBytes();

	if (!ReadTFWFILE(strOrthoDir2,left,top,nGsd2))
	{
		imgOrtho1.Close();
		return 0;
	}

	CSpVZImage imgOrtho2;
	if (!imgOrtho2.Open(strOrthoDir2)) 
	{
		imgOrtho1.Close();
		return 0;
	}
	RECTBOX nBox2(left,left+(imgOrtho2.GetCols()-1)*nGsd2,top,top-(imgOrtho2.GetRows()-1)*nGsd2);
	nPBB2=imgOrtho2.GetPixelBytes();

	if(nPBB1!=nPBB2||nBox1.r<nBox2.l||nBox2.r<nBox1.l||nBox1.t<nBox2.b||nBox2.t<nBox1.b)
	{
		imgOrtho1.Close();
		imgOrtho2.Close();
		return 0;
	}

	RECTBOX nBoxLap;
	nBoxLap.l=max(nBox1.l,nBox2.l);
	nBoxLap.b=max(nBox1.b,nBox2.b);
	nBoxLap.r=min(nBox1.r,nBox2.r);
	nBoxLap.t=min(nBox1.t,nBox2.t);

	int lapW=nBoxLap.Width()/nGsd1+1.5;
	int lapH=nBoxLap.Height()/nGsd1+1.5;

	BYTE *pBuf1=new BYTE[lapW*lapH*nPBB1];
	memset(pBuf1,0,lapW*lapH*nPBB1*sizeof(BYTE));
	imgOrtho1.Read(pBuf1,nPBB1,(nBoxLap.b-nBox1.b)/nGsd1+0.5,(nBoxLap.l-nBox1.l)/nGsd1+0.5,lapH,lapW);
	imgOrtho1.Close();
	BYTE *lpPos1=pBuf1;

	BYTE *pBuf2=new BYTE[lapW*lapH*nPBB2];
	memset(pBuf2,0,lapW*lapH*nPBB2*sizeof(BYTE));
	imgOrtho2.Read(pBuf2,nPBB2,(nBoxLap.b-nBox2.b)/nGsd2+0.5,(nBoxLap.l-nBox2.l)/nGsd2+0.5,lapH,lapW);
	imgOrtho2.Close();
	BYTE *lpPos2=pBuf2;

	long nUsefulSize=LapStatistic(pBuf1,pBuf2,nPBB1,lapW,lapH,fMeans1,fVars1,fMeans2,fVars2,pBackGrd,nBackGrd);
	delete pBuf1;
	delete pBuf2;

	return nUsefulSize;
}

BOOL CAdjustLightColor::CorrStatisticOrtho(CString strOrtho1,float *pVars1,float *pMeans1,CString strOrtho2,float *pVars2,float *pMeans2,long *pUsefulSize,int nRowBlk,int nColBlk,int nWinSize,int nCorrCut,RGBQUAD *pBackGrd,int nBackGrd)
{
	double left1=0.0,top1=0.0,left2=0.0,top2=0.0,nGsd1=0.0,nGsd2=0.0;
	if (!ReadTFWFILE(strOrtho1,left1,top1,nGsd1)) return FALSE;
	if (!ReadTFWFILE(strOrtho2,left2,top2,nGsd2)) return FALSE;

	CSpVZImage imgOrtho1,imgOrtho2;
	if ( (!imgOrtho1.Open(strOrtho1)) || (!imgOrtho2.Open(strOrtho2)) ) 
	{
		imgOrtho1.Close();
		imgOrtho2.Close();
		return FALSE;
	}

	RECTBOX nBox1(left1,left1+(imgOrtho1.GetCols()-1)*nGsd1,top1,top1-(imgOrtho1.GetRows()-1)*nGsd1);
	int nPBB1=imgOrtho1.GetPixelBytes();
	RECTBOX nBox2(left2,left2+(imgOrtho2.GetCols()-1)*nGsd2,top2,top2-(imgOrtho2.GetRows()-1)*nGsd2);
	int nPBB2=imgOrtho2.GetPixelBytes();

	if(fabs(nGsd1-nGsd2)>0.0001|| nPBB1!=nPBB2||nBox1.r<nBox2.l||nBox2.r<nBox1.l||nBox1.t<nBox2.b||nBox2.t<nBox1.b)
	{
		imgOrtho1.Close();
		imgOrtho2.Close();
		return FALSE;
	}

	RECTBOX nBoxLap;
	nBoxLap.l=max(nBox1.l,nBox2.l);
	nBoxLap.b=max(nBox1.b,nBox2.b);
	nBoxLap.r=min(nBox1.r,nBox2.r);
	nBoxLap.t=min(nBox1.t,nBox2.t);

	double blklapwid=nBoxLap.Width()/nColBlk;
	double blklaphei=nBoxLap.Height()/nRowBlk;

	int blklapcol=blklapwid/nGsd1+0.5;
	int blklaprow=blklaphei/nGsd1+0.5;
	BYTE *pGray1=new BYTE[blklapcol*blklaprow];
	BYTE *pGray2=new BYTE[blklapcol*blklaprow];
	BYTE *lpPos1=NULL,*lpPos2=NULL,*lpPos3=NULL;
	BYTE *pBuf1=NULL,*pBuf2=NULL;
	if (nPBB1!=1)
	{
		pBuf1=new BYTE[blklapcol*blklaprow*nPBB1];
		pBuf2=new BYTE[blklapcol*blklaprow*nPBB1];		
	}
	else
	{
		pBuf1=pGray1;
		pBuf2=pGray2;
	}
	int halfwin=nWinSize/2;
	nWinSize=halfwin*2+1;
	int WinCol=blklapcol/nWinSize;
	int WinRow=blklaprow/nWinSize;

	RGBQUAD *pback=new RGBQUAD[nBackGrd+1];
	memset(pback,0,(nBackGrd+1)*sizeof(RGBQUAD));
	if (nBackGrd) memcpy(pback+1,pBackGrd,nBackGrd*sizeof(RGBQUAD));

	float fmeans1[4]={0.0},fvars1[4]={0.0},fmeans2[4]={0.0},fvars2[4]={0.0};
	float *pResM1=pMeans1,*pResM2=pMeans2,*pResV1=pVars1,*pResV2=pVars2;
	long *pResUsSize=pUsefulSize;

	BYTE cr=0;
	int i=0,j=0,k=0,l=0,m=0,x=0;
	long psize=blklaprow*blklapcol,n=0;
	for (i=0;i<nRowBlk;i++)
	{
		for (j=0;j<nColBlk;j++)
		{
			memset(pGray1,0,blklapcol*blklaprow*sizeof(BYTE));
			memset(pGray2,0,blklapcol*blklaprow*sizeof(BYTE));

			int sRow1=(nBoxLap.b+i*blklaphei-nBox1.b)/nGsd1+0.5;
			int sCol1=(nBoxLap.l+j*blklapwid-nBox1.l)/nGsd1+0.5;
			int sRow2=(nBoxLap.b+i*blklaphei-nBox2.b)/nGsd1+0.5;
			int sCol2=(nBoxLap.l+j*blklapwid-nBox2.l)/nGsd1+0.5;

			imgOrtho1.Read(pGray1,1,sRow1,sCol1,blklaprow,blklapcol);
			imgOrtho2.Read(pGray2,1,sRow2,sCol2,blklaprow,blklapcol);

			for (k=0;k<WinRow;k++)
			{
				lpPos1=pGray1+k*nWinSize*blklapcol;
				lpPos2=pGray2+k*nWinSize*blklapcol;
				for (l=0;l<WinCol;l++)
				{
					x=l*nWinSize+halfwin;
					CorrCoefficient(lpPos1,nWinSize,blklapcol,lpPos2,nWinSize,blklapcol,x,halfwin,x,halfwin,nWinSize,nWinSize,cr);
					if (cr<nCorrCut) 
					{
						lpPos3=lpPos1+l*nWinSize;
						for (m=0;m<nWinSize;m++)
						{
							memset(lpPos3,0,nWinSize*sizeof(BYTE));
							lpPos3+=blklapcol;
						}
					}
				}
			}
			
			if (nPBB1!=1)
			{
				memset(pBuf1,0,blklapcol*blklaprow*nPBB1*sizeof(BYTE));
				memset(pBuf2,0,blklapcol*blklaprow*nPBB1*sizeof(BYTE));
				imgOrtho1.Read(pBuf1,nPBB1,sRow1,sCol1,blklaprow,blklapcol);
				imgOrtho2.Read(pBuf2,nPBB2,sRow2,sCol2,blklaprow,blklapcol);

				lpPos1=pBuf1;
				lpPos2=pGray1;
				for (n=0;n<psize;n++)
				{
					if (*lpPos2==0)
					{
						memset(lpPos1,0,nPBB1*sizeof(BYTE));
					}
					lpPos1+=nPBB1;
					lpPos2++;
				}
			}

			*pResUsSize=LapStatistic(pBuf1,pBuf2,nPBB1,blklapcol,blklaprow,fmeans1,fvars1,fmeans2,fvars2,pback,nBackGrd+1);
			if (nPBB1==1)
			{
				*pResM1=fmeans1[3];	pResM1++;
				*pResV1=fvars1[3];	pResV1++;
				*pResM2=fmeans2[3];	pResM2++;
				*pResV2=fvars2[3];	pResV2++;
			}
			else
			{
				*pResM1=fmeans1[0];	
				*pResV1=fvars1[0];	
				*pResM2=fmeans2[0];	
				*pResV2=fvars2[0];	

				*(pResM1+1)=fmeans1[1];	
				*(pResV1+1)=fvars1[1];	
				*(pResM2+1)=fmeans2[1];	
				*(pResV2+1)=fvars2[1];	

				*(pResM1+2)=fmeans1[2];	
				*(pResV1+2)=fvars1[2];	
				*(pResM2+2)=fmeans2[2];	
				*(pResV2+2)=fvars2[2];	

				pResM1+=nPBB1;
				pResV1+=nPBB1;
				pResM2+=nPBB1;
				pResV2+=nPBB1;
			}
			pResUsSize++;
		}
	}
	delete pGray1;
	delete pGray2;
	delete pback;
	if (nPBB1!=1) 
	{
		delete pBuf1;
		delete pBuf2;
	}
	imgOrtho1.Close();
	imgOrtho2.Close();

	return TRUE;
}

long CAdjustLightColor::LapStatistic(BYTE *pBuf1,BYTE *pBuf2,int nPBB,int wid,int hei,float fMean1[4],float fVars1[4],float fMean2[4],float fVars2[4],RGBQUAD *pBackGrd,int nBackGrd)
{
	BYTE *lpPos1=pBuf1;
	BYTE *lpPos2=pBuf2;

	long nUsefulSize=0;
	long pHisto1[256],pHistor1[256],pHistog1[256],pHistob1[256];
	memset(pHisto1,0,256*sizeof(long));
	memset(pHistor1,0,256*sizeof(long));
	memset(pHistog1,0,256*sizeof(long));
	memset(pHistob1,0,256*sizeof(long));

	long pHisto2[256],pHistor2[256],pHistog2[256],pHistob2[256];
	memset(pHisto2,0,256*sizeof(long));
	memset(pHistor2,0,256*sizeof(long));
	memset(pHistog2,0,256*sizeof(long));
	memset(pHistob2,0,256*sizeof(long));

	int iBit=nPBB*8;
	long psize=wid*hei;
	BYTE gray=0,r=0,g=0,b=0;
	for (long bs=0;bs<psize;bs++)
	{
		nUsefulSize++;
		if(nBackGrd>0 && pBackGrd!=NULL)
		{
			if(BeBackground(lpPos1,iBit,pBackGrd,nBackGrd)||BeBackground(lpPos2,iBit,pBackGrd,nBackGrd)) 
			{
				nUsefulSize--;
				lpPos1+=nPBB;
				lpPos2+=nPBB;
				continue;
			}
		}

		if(iBit==8)
		{
			gray=*(lpPos1);
			pHisto1[gray]++;
			lpPos1++;

			gray=*(lpPos2);
			pHisto2[gray]++;
			lpPos2++;
		}
		else
		{
			r=*(lpPos1); lpPos1++;
			g=*(lpPos1); lpPos1++;
			b=*(lpPos1); lpPos1++;

			pHistor1[r]++;
			pHistog1[g]++;
			pHistob1[b]++;

			r=*(lpPos2); lpPos2++;
			g=*(lpPos2); lpPos2++;
			b=*(lpPos2); lpPos2++;

			pHistor2[r]++;
			pHistog2[g]++;
			pHistob2[b]++;
		}
	}
	lpPos1=NULL;
	lpPos2=NULL;

	memset(fVars1,0,4*sizeof(float));
	memset(fMean1,0,4*sizeof(float));
	memset(fVars2,0,4*sizeof(float));
	memset(fMean2,0,4*sizeof(float));
	if(nUsefulSize<=0) return 0;

	for(int j=0;j<256;j++)
	{
		fMean1[0]+=float(pHistor1[j])/nUsefulSize*j;
		fMean1[1]+=float(pHistog1[j])/nUsefulSize*j;
		fMean1[2]+=float(pHistob1[j])/nUsefulSize*j;
		fMean1[3]+=float(pHisto1[j] )/nUsefulSize*j;

		fMean2[0]+=float(pHistor2[j])/nUsefulSize*j;
		fMean2[1]+=float(pHistog2[j])/nUsefulSize*j;
		fMean2[2]+=float(pHistob2[j])/nUsefulSize*j;
		fMean2[3]+=float(pHisto2[j] )/nUsefulSize*j;
	}

	for(j=0;j<256;j++)
	{
		fVars1[0]+=(j-fMean1[0])*(j-fMean1[0])*pHistor1[j]/nUsefulSize;
		fVars1[1]+=(j-fMean1[1])*(j-fMean1[1])*pHistog1[j]/nUsefulSize;
		fVars1[2]+=(j-fMean1[2])*(j-fMean1[2])*pHistob1[j]/nUsefulSize;
		fVars1[3]+=(j-fMean1[3])*(j-fMean1[3])*pHisto1[j]/nUsefulSize;

		fVars2[0]+=(j-fMean2[0])*(j-fMean2[0])*pHistor2[j]/nUsefulSize;
		fVars2[1]+=(j-fMean2[1])*(j-fMean2[1])*pHistog2[j]/nUsefulSize;
		fVars2[2]+=(j-fMean2[2])*(j-fMean2[2])*pHistob2[j]/nUsefulSize;
		fVars2[3]+=(j-fMean2[3])*(j-fMean2[3])*pHisto2[j]/nUsefulSize;
	}
	for(j=0;j<4;j++)
	{
		fVars1[j]=float(sqrt(fVars1[j]));
		fVars2[j]=float(sqrt(fVars2[j]));		
	}
	return nUsefulSize;
}

void CAdjustLightColor::BlkStatistic(BYTE *pBuf,int nPBB,int wid,int hei,float fMean[4],float fVars[4],RGBQUAD *pBackGrd,int nBackGrd)
{
	BYTE *lpPos=pBuf;
	long nUsefulSize=0;
	long pHisto[256],pHistor[256],pHistog[256],pHistob[256];
	memset(pHisto,0,256*sizeof(long));
	memset(pHistor,0,256*sizeof(long));
	memset(pHistog,0,256*sizeof(long));
	memset(pHistob,0,256*sizeof(long));

	int iBit=nPBB*8;
	long psize=wid*hei;
	BYTE gray=0,r=0,g=0,b=0;
	for (long bs=0;bs<psize;bs++)
	{
		nUsefulSize++;
		if(nBackGrd>0 && pBackGrd!=NULL)
		{
			if(BeBackground(lpPos,iBit,pBackGrd,nBackGrd)) 
			{
				nUsefulSize--;
				lpPos+=nPBB;
				continue;
			}
		}

		if(iBit==8)
		{
			gray=*(lpPos);
			pHisto[gray]++;
			lpPos++;
		}
		else
		{
			r=*(lpPos); lpPos++;
			g=*(lpPos); lpPos++;
			b=*(lpPos); lpPos++;

			pHistor[r]++;
			pHistog[g]++;
			pHistob[b]++;
		}
	}
	lpPos=NULL;

	memset(fVars,0,4*sizeof(float));
	memset(fMean,0,4*sizeof(float));
	if(nUsefulSize<=0) return;

	for(int j=0;j<256;j++)
	{
		fMean[0]+=float(pHistor[j])/nUsefulSize*j;
		fMean[1]+=float(pHistog[j])/nUsefulSize*j;
		fMean[2]+=float(pHistob[j])/nUsefulSize*j;
		fMean[3]+=float(pHisto[j] )/nUsefulSize*j;
	}

	for(j=0;j<256;j++)
	{
		fVars[0]+=(j-fMean[0])*(j-fMean[0])*pHistor[j]/nUsefulSize;
		fVars[1]+=(j-fMean[1])*(j-fMean[1])*pHistog[j]/nUsefulSize;
		fVars[2]+=(j-fMean[2])*(j-fMean[2])*pHistob[j]/nUsefulSize;
		fVars[3]+=(j-fMean[3])*(j-fMean[3])*pHisto[j]/nUsefulSize;
	}
	for(j=0;j<4;j++)
	{
		fVars[j]=float(sqrt(fVars[j]));
	}
}

void CAdjustLightColor::CorrCoefficient(BYTE *img1,int row1,int col1,BYTE *img2,int row2,int col2,int x1,int y1,int x2,int y2,int height,int width,BYTE &cr)
{
	int i,j,n;
	int	tpi,height2,width2;
	float Sx,Sxx,Sy,Syy,Sxy,v1,v2,cv;
	BYTE *image1,*image10,*image2,*image20;
	
	height2=height/2;
	width2=width/2;
	
	n=width*height;
	
	Sx=Sxx=0.0;
	image10=image1=img1+(y1-height2)*col1+(x1-width2);
	for (i=0; i<height; i++) 
	{
		for (j=0; j<width; j++)
		{ 
			Sx += *image1; 
			Sxx+= *image1* *image1++; 
		}
		image10 +=col1;  image1=image10;
	}	
	v1=(Sxx-(Sx*Sx)/n)/n;
	
	Sy=Syy=0.0;
	image10=image1=img2+(y2-height2)*col2+(x2-width2);	
	for (i=0; i<height; i++) 
	{
		for (j=0; j<width; j++)
		{ 
			Sy += *image1; 
			Syy+= *image1* *image1++; 
		}
		image10 +=col2;  image1=image10;

	}
	v2=(Syy-(Sy*Sy)/n)/n;

	Sxy=0;
	image10=image1=img1+(y1-height2)*col1+(x1-width2);	
	image20=image2=img2+(y2-height2)*col2+(x2-width2);	

	for (i=0; i<height; i++)
	{
		for (j=0; j<width; j++)
			Sxy+= *image1++ * *image2++;
		
		image10+=col1;  image1=image10;
		image20+=col2;  image2=image20;
	}
	cv=(Sxy-(Sx*Sy)/n)/n;
	if (v1<1 || v2<1 || cv<=0.0)  tpi= 0;
	else
		tpi=(int)(100.0*cv/sqrt(v1*v2));
	cr=(BYTE)tpi; 
}

bool CAdjustLightColor::BBISubDodging(CString strBBI,CString strRes,float size)
{
	CSpBBImage2 bbiSrc;
	if (!bbiSrc.Open(strBBI,CSpBBImage::modeRead)) return false;

	int nLayerNum=0,pymLayer=2;
	const int *pLayerRatio=bbiSrc.GetPymZmRate(nLayerNum);
	if (pymLayer>=nLayerNum) return false;

	int wid=bbiSrc.GetCols()/pLayerRatio[pymLayer];
	wid=4*int(wid/4);
	int hei=bbiSrc.GetRows()/pLayerRatio[pymLayer];
	int srcbpp=bbiSrc.GetPixelBytes();
	int resbpp=3;
	if (srcbpp==1||srcbpp==2) resbpp=1;

	CxImage img;
	img.Create(wid,hei,resbpp*8);
	memset(img.GetBits(),0,hei*img.GetEffWidth()*sizeof(BYTE));

	BYTE pal[256];
	for (int p=0;p<256;p++) pal[p]=p;
	img.SetPalette(256,pal,pal,pal);		
	BYTE *pBuf=img.GetBits();
	bbiSrc.Read2(pBuf,resbpp,0,0,hei,wid,pymLayer);
	if(resbpp==3) RGB2BGR(pBuf,wid*hei);
	pBuf=NULL;

	cout<<"read paramid over"<<endl;

	long i=0;
	while((1<<i)<wid) i++;
	long w=1<<i;

	i=0;
	while((1<<i)<hei) i++;
	long h=1<<i;

	if(w>16384) w=16384;
	if(h>2048) h=2048;
	if (w!=wid||h!=hei) 
	{
		if (!img.Resample(w,h,0)) return false;	
	}

	cout<<"img resample over"<<endl;

	int bpp=0;
	float fVars[4]={0.0};
	float fMeans[4]={0.0};
	GetColorStatistic(&img,bpp,fVars,fMeans);

	cout<<"color statistic over"<<endl;

	int iBPP=img.GetBpp();
	if (iBPP==24) FilterBigColor(&img,size,5,fMeans);
	else if (iBPP==8) FilterBigGray(&img,size,5,fMeans[3]);
	else return false;

	cout<<"filter img over"<<endl;

	int nRows=bbiSrc.GetRows();
	int nCols=bbiSrc.GetCols();
	int nBlkSize=bbiSrc.GetBlockSize();

	CSpBBImage2 bbiRes;
	if (!bbiRes.Open(strRes,CSpBBImage::modeCreate)) return false;
	bbiRes.SetRows(nRows);
	bbiRes.SetCols(nCols);
	bbiRes.SetPixelBytes(srcbpp);
	bbiRes.SetBlockSize(nBlkSize);
	RGBQUAD rgbq[256];
	for (p=0;p<256;p++)
	{
		rgbq[p].rgbBlue=rgbq[p].rgbGreen=rgbq[p].rgbRed=rgbq[p].rgbReserved=p;
	}
	bbiRes.SetColorTable(rgbq,256);

	double wrat=double(w)/double(nCols);
	double hrat=double(h)/double(nRows);
	double x=0.0,y=0.0;
	RGBQUAD rgb;
	int blkRows=(nRows+nBlkSize-1)/nBlkSize;
	int	blkCols=(nCols+nBlkSize-1)/nBlkSize;
	pBuf=new BYTE[nBlkSize*nBlkSize*srcbpp];
	BYTE *pSrc=NULL;

	for (int r=0;r<blkRows;r++)
	{
		for (int c=0;c<blkCols;c++)
		{
			memset(pBuf,0,nBlkSize*nBlkSize*srcbpp*sizeof(BYTE));
			bbiSrc.ReadBlock(pBuf,c,r);
			pSrc=pBuf;

			for (int ir=0;ir<nBlkSize;ir++)
			{
				for (int ic=0;ic<nBlkSize;ic++)
				{
					x=(c*nBlkSize+ic)*wrat;
					y=(r*nBlkSize+ir)*hrat;
					rgb=img.GetPixelColor(x+0.5,y+0.5);

					if (srcbpp==1) 
					{
						if(*pSrc!=0&&*pSrc!=255 && rgb.rgbGreen!=0&&rgb.rgbGreen!=255) *pSrc=max(0,min(255,(*pSrc)+(fMeans[3]-rgb.rgbGreen)));
						pSrc++;
					}
					else
					{
 						if(*pSrc!=0&&*pSrc!=255 && rgb.rgbRed!=0&&rgb.rgbRed!=255) *pSrc=max(0,min(255,(*pSrc)+(fMeans[0]-rgb.rgbRed)));
 						pSrc++;
 
 						if(*pSrc!=0&&*pSrc!=255 && rgb.rgbGreen!=0&&rgb.rgbGreen!=255) *pSrc=max(0,min(255,(*pSrc)+(fMeans[1]-rgb.rgbGreen)));
 						pSrc++;
 
 						if(*pSrc!=0&&*pSrc!=255 && rgb.rgbBlue!=0&&rgb.rgbBlue!=255) *pSrc=max(0,min(255,(*pSrc)+(fMeans[2]-rgb.rgbBlue)));
 						pSrc++;	
					}
				}
			}
			bbiRes.WriteBlock(pBuf,c,r);
		}
	}
	delete pBuf;

	cout<<"sub dodging over"<<endl;

	img.Destroy();
	bbiSrc.Close();
	bbiRes.Close();

	cout<<"close bbi over"<<endl;

	return true;
}

bool CAdjustLightColor::BBIDivideDodging(CString strBBI,CString strRes,float size)
{
	CSpBBImage2 bbiSrc;
	if (!bbiSrc.Open(strBBI,CSpBBImage::modeRead)) return false;

	int nLayerNum=0,pymLayer=2;
	const int *pLayerRatio=bbiSrc.GetPymZmRate(nLayerNum);
	if (pymLayer>=nLayerNum) return false;

	int wid=bbiSrc.GetCols()/pLayerRatio[pymLayer];
	wid=4*int(wid/4);
	int hei=bbiSrc.GetRows()/pLayerRatio[pymLayer];
	int srcbpp=bbiSrc.GetPixelBytes();
	int resbpp=3;
	if (srcbpp==1||srcbpp==2) resbpp=1;

	CxImage img;
	img.Create(wid,hei,resbpp*8);
	memset(img.GetBits(),0,hei*img.GetEffWidth()*sizeof(BYTE));

	BYTE pal[256];
	for (int p=0;p<256;p++) pal[p]=p;
	img.SetPalette(256,pal,pal,pal);		
	BYTE *pBuf=img.GetBits();
	bbiSrc.Read2(pBuf,resbpp,0,0,hei,wid,pymLayer);
	if(resbpp==3) RGB2BGR(pBuf,wid*hei);
	pBuf=NULL;

	long i=0;
	while((1<<i)<wid) i++;
	long w=1<<i;

	i=0;
	while((1<<i)<hei) i++;
	long h=1<<i;

	if(w>16384) w=16384;
	if(h>2048) h=2048;
	if (w!=wid||h!=hei) 
	{
		if (!img.Resample(w,h,0)) return false;	
	}

	int bpp=0;
	float fVars[4]={0.0};
	float fMeans[4]={0.0};
	GetColorStatistic(&img,bpp,fVars,fMeans);

	int iBPP=img.GetBpp();
	if (iBPP==24) FilterBigColor(&img,size,5,fMeans);
	else if (iBPP==8) FilterBigGray(&img,size,5,fMeans[3]);
	else return false;

	int nRows=bbiSrc.GetRows();
	int nCols=bbiSrc.GetCols();
	int nBlkSize=bbiSrc.GetBlockSize();

	CSpBBImage2 bbiRes;
	if (!bbiRes.Open(strRes,CSpBBImage::modeCreate)) return false;
	bbiRes.SetRows(nRows);
	bbiRes.SetCols(nCols);
	bbiRes.SetPixelBytes(srcbpp);
	bbiRes.SetBlockSize(nBlkSize);
	RGBQUAD rgbq[256];
	for (p=0;p<256;p++)
	{
		rgbq[p].rgbBlue=rgbq[p].rgbGreen=rgbq[p].rgbRed=rgbq[p].rgbReserved=p;
	}
	bbiRes.SetColorTable(rgbq,256);

	double wrat=w/nCols;
	double hrat=h/nRows;
	double x=0.0,y=0.0;
	RGBQUAD rgb;
	int blkRows=(nRows+nBlkSize-1)/nBlkSize;
	int	blkCols=(nCols+nBlkSize-1)/nBlkSize;
	pBuf=new BYTE[nBlkSize*nBlkSize*srcbpp];
	BYTE *pSrc=NULL;
	for (int r=0;r<blkRows;r++)
	{
		for (int c=0;c<blkCols;c++)
		{
			memset(pBuf,0,nBlkSize*nBlkSize*srcbpp*sizeof(BYTE));
			bbiSrc.ReadBlock(pBuf,c,r);
			pSrc=pBuf;

			for (int ir=0;ir<nBlkSize;ir++)
			{
				for (int ic=0;ic<nBlkSize;ic++)
				{
					x=(c*nBlkSize+ic)*wrat;
					y=(r*nBlkSize+ir)*hrat;
					rgb=img.GetfPixelColor(x,y);

					if (srcbpp==1) 
					{
						if(*pSrc!=0&&*pSrc!=255) if(rgb.rgbGreen) *pSrc=max(0,min(255,*pSrc*fMeans[3]/rgb.rgbGreen));
						pSrc++;
					}
					else
					{
						if(*pSrc!=0&&*pSrc!=255) if(rgb.rgbBlue) *pSrc=max(0,min(255,*pSrc*fMeans[0]/rgb.rgbRed));
						pSrc++;

						if(*pSrc!=0&&*pSrc!=255) if(rgb.rgbGreen) *pSrc=max(0,min(255,*pSrc*fMeans[1]/rgb.rgbGreen));
						pSrc++;

						if(*pSrc!=0&&*pSrc!=255) if(rgb.rgbRed) *pSrc=max(0,min(255,*pSrc*fMeans[2]/rgb.rgbBlue));
						pSrc++;	
					}
				}
			}
			bbiRes.WriteBlock(pBuf,c,r);
		}
	}
	delete pBuf;

	img.Destroy();
	bbiSrc.Close();
	bbiRes.Close();

	return true;
}

# Color balance for TIFF images 
-------------------------------
__Color balance based on Wallis filter__

## Illustration
Original Image            |  Template Image |  Color Balanced Image
:-------------------------:|:-------------------------:|:-------------------------:
![ori](/uploads/b9b207c959395d21cc4df84f8d152728/ori.png)  |  ![temp](/uploads/9b5157cff1d3b2b289ff8930745def36/temp.png)  |  ![trans](/uploads/d0e066fec799cdd85a851ab5e92c397c/trans.png)

## Structure of directories
### sub directories
```
color_balance
├── image
│   ├── ori_image
│   └── tmpl_image
├── src
│   ├── bin
│   ├── mtl
│   └── src
├── result
├── utils
│   ├── gdalcopyproj.py
│   ├── geo_project.py
│   ├── make_run_bash.py
│   └── mode_select.py
├── main.sh
...
```
#### directories
* `./image/ori_image`: original images
* `./image/tmpl_image`: template image
* `./src/bin` : source code for color balance
* `./src/mtl` : references 
* `./src/src` : source code for color balance
* `./utils`: utilities for api

#### scripts
* `main.sh`: main bash
* `gdalcopyproj.py`: use GDAL command line to copy projections from ori\_image.tif to trans\_image.tif
* `geo_project.py`: make bash file saved in temporary file `geo_project.sh` for batch geo projection
* `make_run_bash.py`: make bash file saved in temporary file `trans.sh` for calling source exe file saved in `./src/bin`
* `mode_select.py`: select color transform mode and make txt files saved in temporary dir `./trans` for further transform

## Get Started
### Reference
The original idea is from:
```
@phdthesis{孙明伟2009正射影像全自动快速制作关键技术研究,
  title={正射影像全自动快速制作关键技术研究},
  author={孙明伟},
  year={2009},
  school={武汉: 武汉大学}
}

```
### Requirements
* [Python 3.5.2+](https://www.python.org/)
* [wine 3.0.3+](https://www.winehq.org/news/2018091301)
* [GDAL 2.1.0+](https://trac.osgeo.org/gdal/wiki/DownloadSource)

### Data
```
mkdir ./image/ori_image ./image/tmpl_image ./result
```
original images in `./image/ori_image`  
template image(only one piece) in `./image/tmpl_image`  
result images in `./result`

### Run
```
$ bash ./main.sh
```
or 
```
$ python ./utils/mode_select.py
$ python ./utils/make_run_bash.py 
$ bash trans.sh
$ python utils/geo_project.py
$ bash geo_project.sh
$ rm -r trans
$ rm trans.sh
$ rm geo_project.sh
```
### Result
Result images are saved in `./result`

### Others
More color balance methods can be chosen in scrip `mode_select.py`

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 22 11:38:41 2018

@author: kaku

Make color temparary temporary bash file
"""
import os
Utils_DIR = os.path.dirname(os.path.abspath(__file__))
DIR = os.path.dirname(Utils_DIR)

soft_path = os.path.join(DIR, 'src/bin/JQDodging.exe')
trans_dir = os.path.join(DIR, 'trans/')
trans_bash_name = 'trans.sh'

trans_names = os.listdir(trans_dir)

script=open(os.path.join(DIR, trans_bash_name),'a')
script.write("echo 'Running Scripts for making transformation bash file ...'"+'\n')
script.close()

for trans_name in trans_names:
    script=open(os.path.join(DIR, trans_bash_name),'a')
    script.write('wine '+ soft_path + ' ' + os.path.join(trans_dir, trans_name) + '\n')
    script.close()



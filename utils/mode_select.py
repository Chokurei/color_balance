#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 22 11:21:23 2018

@author: kaku

Transformation mode and file selection
"""

import os

Utils_DIR = os.path.dirname(os.path.abspath(__file__))
DIR = os.path.dirname(Utils_DIR)
# template image dir
tmpl_img_dir = 'image/tmpl_image/'
# original image dir
img_dir = 'image/ori_image/'
# transformed image dir
result_dir = 'result/'

trans_path = os.path.join(DIR,'trans/')

if not os.path.exists(trans_path):
    os.mkdir(trans_path)

img_names = os.listdir(os.path.join(DIR, img_dir))
tmpl_img_name = os.listdir(os.path.join(DIR, tmpl_img_dir))[0]

for idx in range(0, len(img_names)):
    script=open(os.path.join(trans_path, 'Dodge_'+str(idx)+'.txt'),'a')
    # mode
    script.write('1'+'\n')
    # original image files
    script.write(os.path.join(os.path.join(DIR, img_dir)+img_names[idx]+'\n'))
    # result image files
    script.write(os.path.join(os.path.join(DIR, result_dir)+img_names[idx]+'\n'))
    # templete image files
    script.write(os.path.join(os.path.join(DIR, tmpl_img_dir)+tmpl_img_name+'\n'))
    # mode
    script.write('7'+'\n')
    # mode
    script.write('0'+'\n')
    script.close()
  
    
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 22 16:51:53 2018

@author: kaku
"""
import os

Utils_DIR = os.path.dirname(os.path.abspath(__file__))
DIR = os.path.dirname(Utils_DIR)

tool_path = os.path.join(Utils_DIR, 'gdalcopyproj.py')
img_dir = 'image/ori_image/'
result_dir = 'result/'

geo_proj_path = os.path.join(DIR, 'geo_project.sh')

script=open(geo_proj_path,'a')
script.write("echo 'Making geo projection bash file ...'"+'\n')
script.close()

img_names = sorted(os.listdir(os.path.join(DIR, img_dir)))
for img_name in img_names:
    img_path = os.path.join(DIR, img_dir, img_name)
    result_path = os.path.join(DIR, result_dir, img_name)
    script=open(geo_proj_path,'a')
    script.write('python '+ tool_path + " '" + img_path + "'" + ' ' + "'" + result_path + "'" + '\n')
    script.close()


    
    